# iwithlong
I with you long and long

## 开发说明

## 路径规范

为了方便session的控制及以后权限的管理，所有的URI都必须遵循如下规则，因为当有需要整合`Spring Security`、`Shiro`这些跟MVC无关的安全框架时，`Controller`等类上加注解这种依赖于MVC框架本身的方式将会无法使用。

目前平台的URI分为以下几种：

- `/a/**` 表示/admin/** 需要session，且需要用户类型为admin
- `/f/**` 表示/front/** 前端的页面，一般公网部分，不需要session
- `/user/**` 用户登录退出等，不需要session

iwithlong的URI规则：

- `/a/**` 跟平台管理功能类似，需要权限等控制，保持一致
- `/b/**` 表示/business/** 业务功能部分，需要session不需要权限控制
- `/m/**` 表示/mobile/** 给手机等移动端使用接口，需要session不需要权限控制
- 其它 不需要session也不需要权限控制

## 页面开发说明

本项目属于后台管理系统，没有公网类似于blog等部分，平台已经有现成搭好的layout，一个典型页面如下：

    <#import "../layout/admin-assembly.ftl" as adminAssembly>
    <@adminAssembly.layout>
    <section class="content container-fluid">
        <form role="form" class="form-horizontal" action="add" method="post">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <h3 class="box-title">
                        <i class="<@spring.messageText code="admin.list.btn.add.ico" text="fa fa-plus"/>"
                           aria-hidden="true"></i>
                        <@spring.messageText code="admin.model.add.header.setting" text="添加参数"/></h3>
                </div>
                <#include "setting-form-share.ftl">
            </div>
        </form>
    </section>
    </@adminAssembly.layout>

可以看到用很少的代码就能实现整个页面，面包屑等会自动处理，可以参考插件jar包中的ftl文件。

## 插件页面文字修改

从上面的示例页面可以看出，整个页面没有硬编码的文字和图标内容，插件内所有的页面上的文字和图标均可以修改。

插件中的页面采用了默认值的方式，如要修改可以按如下方法：

在 resources(classpath) 目录下建立 i18n 文件夹，新建 messages.properties 文件，找到文字或图标内容的code并替换。

例如：

    admin.model.add.header.setting=add setting
    
再次运行项目，上面原来显示`添加参数`的地方将显示为`add setting`。

当然也可以建立`messages_zh_CN.properties`、`messages_en_US.properties`等实现国际化，此处不在赘述。

### 增、删、改按钮封装说明

平台默认对增、删、改等常用按钮做了封装，可以快速实现相应功能。

列表`checkbox`要求：

顶部全选或全不选 checkbox，拥有class `header-chk-all`：

    <input type="checkbox" class="header-chk-all">

记录前面 checkbox，拥有class `row-chk`：

    <input name="settingId" type="checkbox" class="row-chk" value="${item.settingId}">

按钮代码：

    <a href="add" class="btn btn-sm bg-olive btn-add">
        <i class="<@spring.messageText code="admin.list.btn.add.ico" text="fa fa-plus"/>"aria-hidden="true"></i>
        <@spring.messageText code="admin.list.btn.add.text" text="添加"/>
    </a>
    <a href="update" class="btn btn-sm bg-orange btn-update">
        <i class="<@spring.messageText code="admin.list.btn.update.ico" text="fa fa-pencil"/>"aria-hidden="true"></i> 
        <@spring.messageText code="admin.list.btn.update.text" text="修改"/>
    </a>
    <a href="delete" class="btn btn-sm bg-danger btn-confirm"
       msg="<@spring.messageText code="admin.list.btn.delete.confirm.msg" text="删除操作不可恢复，确认删除？"/>">
        <i class="<@spring.messageText code="admin.list.btn.delete.ico" text="fa fa-trash"/>"aria-hidden="true"></i> 
        <@spring.messageText code="admin.list.btn.delete.text" text="删除"/>
    </a>
    
只要 class 中有`btn-update`、`btn-confirm`就会绑定相应的功能事件，表单提交的action为a标签的`href`。

`btn-add`是页面跳转，不需要功能事件绑定。

其中`btn-confirm`不限于删除，会弹出窗口让用户确认，配合`msg`属性显示具体的提示信息。

### 密码加密

平台目前使用的密码加密方式为当前公认最安全的`BCrypt`，为了保持一致，更为了适配后面整合`Spring Security`等安全框架时的统一认证，在使用到用户登录密码的地方统一使用`BCrypt`加密。

`BCrypt`加密的方式跟`MD5`相比在使用上有一定区别。

可以不需要指定`salt`，默认会有一个`salt`和加密强度，且同一个密码每次加密出来的结果都不相同，长度为60。

平台封装示例代码：

    //这里加密出来的encodePassword每次都不相同
    String encodePassword = EncryptUtils.encodeBCrypt("123456");
    
    //由于加密出来的结果每次都不相同，所以不能像md5一样简单equals判断，需要专用方法 返回true表示正确
    boolean isMatch = EncryptUtils.matchesBCrypt("123456", encodePassword);

### 数据字典使用

**后台使用**

通过工具类`DictionaryKit`实现。不用担心缓存问题，当有更新时会自动更新缓存。

主要方法：

- Dictionary getDictionary(String dicKey) 获取字典对象本身
- List<Dictionary> getChildren(String dicKey) 获取字典对象子项列表
- List<Dictionary> getChildren(String[] dicKeys) 获取有多个层级的子项列表

  例如有以下结构字典数据：
  - menu_type
    - admin_menu 管理员菜单
      - manage_menu 管理菜单
      - set_menu 设置菜单
    - user_menu 用户菜单
      - nav_menu 导航菜单
      - msg_menu 消息菜单
      
  getChildren(new String[]{"menu_type"}) 等同于 getChildren("menu_type") 返回：
  - admin_menu 管理员菜单
  - user_menu 用户菜单
  
  getChildren(new String[]{"menu_type","user_menu"}) 返回：
  - nav_menu 导航菜单
  - msg_menu 消息菜单
  
**前端使用**

可以使用函数`dickit`来实现。以获取菜单类型为例：

    <select name="menuType" id="menuType" class="form-control">
    <@dickit dicKey="menu_type">
        <#list dictionaries as dic>
            <option value="${dic.dicKey}" <#if (menu.menuType)! == dic.dicKey>selected</#if>>${(dic.dicValue)!}</option>
        </#list>
    </@dickit>
    </select>
    
多层级获取使用英文逗号分隔：

    <@dickit dicKey="menu_type,user_menu">
        <#list dictionaries as dic>
        ...
        </#list>
    </@dickit>
    
**显示具体的值**

同样支持多层级，`specKey`指定具体要显示子项内容的key。示例：

    <@dickit dicKey="user_type" specKey="1">
        ${value!}
    </@dickit>
    
### 系统设置/参数使用

**后台使用**


通过工具类`SettingKit`实现。不用担心缓存问题，当有更新时会自动更新缓存。

主要方法：

- Setting getSetting(String name) 获取设置/参数对象本身
- String getValue(String name) 获取设置/参数对象的值

**前端使用**

可以使用函数`settingkit`来实现。示例：

    <@settingkit name="websiteTitle">${value!}</@settingkit>