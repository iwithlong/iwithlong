package com.iwithlong.listener;

import com.iwithlong.event.MemberEvent;
import com.iwithlong.schedule.UpdateIndexScanTask;
import com.iwithlong.service.MemberGrowthService;
import com.iwithlong.service.SearchKeyService;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * Write class comments here
 * <p>
 * User: TJM
 * Date: 2017/8/29 15:05
 * version $Id: UserListener.java, v 0.1  15:05 Exp $
 */
@Component
public class MemberListener {


	@Resource
	private MemberGrowthService memberGrowthService;

	@Resource
	private SearchKeyService searchKeyService;

	private String key;

	/**
	 * 注册事件
	 * @param register
	 */
	@EventListener
	public void registerListener(MemberEvent.Register register){

		Long memberId = (Long)register.getSource ();

		memberGrowthService.registerMemberGrowth (memberId);

	}


	/**
	 * 会员信息补充事件
	 * @param informationSupplement
	 */
	@EventListener
	public void informationSupplement(MemberEvent.InformationSupplement informationSupplement){

	}


	/**
	 * 会员点兴趣事件
	 * @param clickInterest
	 */
	@EventListener
	public void clickInterest(MemberEvent.ClickInterest clickInterest){




	}

	/**
	 * 会员查看活动事件
	 * @param viewActivitie
	 */
	@EventListener
	public void viewActivitie(MemberEvent.ViewActivitie viewActivitie){



	}

	/**
	 * 查询后添加到热门搜索 事件
	 * @param addToSearchKey
	 */
	@EventListener
	public void addToSearchKey(MemberEvent.AddToSearchKey addToSearchKey){

		this.key=(String) addToSearchKey.getSource ();
		UpdateIndexScanTask.SCHEDULE_POOL.execute (new Runnable () {
			@Override
			public void run () {

				searchKeyService.strAddToSearchKey (key);

			}
		});

	}



}
