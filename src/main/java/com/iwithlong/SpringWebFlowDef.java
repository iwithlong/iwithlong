//package com.iwithlong;
//
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.context.annotation.ImportResource;
//import org.springframework.webflow.config.AbstractFlowConfiguration;
//import org.springframework.webflow.definition.registry.FlowDefinitionRegistry;
//import org.springframework.webflow.executor.FlowExecutor;
//import org.springframework.webflow.mvc.servlet.FlowHandlerAdapter;
//import org.springframework.webflow.mvc.servlet.FlowHandlerMapping;
//
///**
// * Created by xiexiaohan on 2018/1/23.
// * 配置SpringWebFlow
// */
//@Configuration
////@ImportResource("classpath:webflow-config.xml")
//public class SpringWebFlowDef extends AbstractFlowConfiguration {
//    @Bean
//    public FlowDefinitionRegistry flowRegistry() {
//        return getFlowDefinitionRegistryBuilder()
//                .setBasePath("/WEB-INF/flow/")
//                .addFlowLocationPattern("/**/*-flow.xml")
//                .build();
//    }
//
//    @Bean
//    public FlowExecutor flowExecutor(FlowDefinitionRegistry flowRegistry) {
//        return getFlowExecutorBuilder(flowRegistry()).build();
//    }
//
//    @Bean
//    public FlowHandlerAdapter flowHandlerAdapter(FlowExecutor flowExecutor) {
//        FlowHandlerAdapter flowHandlerAdapter =
//                new FlowHandlerAdapter();
//        flowHandlerAdapter.setFlowExecutor(flowExecutor);
//        return flowHandlerAdapter;
//    }
//
//    @Bean
//    public FlowHandlerMapping flowHandlerMapping(FlowDefinitionRegistry flowRegistry) {
//        FlowHandlerMapping flowHandlerMapping =
//                new FlowHandlerMapping();
//        flowHandlerMapping.setFlowRegistry(flowRegistry);
//        flowHandlerMapping.setOrder(0);
//        return flowHandlerMapping;
//    }
//}
