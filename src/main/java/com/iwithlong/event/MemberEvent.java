package com.iwithlong.event;

import org.springframework.context.ApplicationEvent;

/**
 * Created by liyd on 17/8/28.
 */
public class MemberEvent {

    public static class Register extends ApplicationEvent {
        public Register(Object source) {
            super(source);
        }
    }

    public static class InformationSupplement extends ApplicationEvent {
        public InformationSupplement(Object source) {
            super(source);
        }
    }

    public static class ClickInterest extends ApplicationEvent {
        public ClickInterest(Object source) {
            super(source);
        }
    }


    public static class ViewActivitie extends ApplicationEvent {
        public ViewActivitie(Object source){
            super(source);
        }
    }


    public static class AddToSearchKey extends ApplicationEvent {
        public AddToSearchKey(Object source){
            super(source);
        }
    }


}
