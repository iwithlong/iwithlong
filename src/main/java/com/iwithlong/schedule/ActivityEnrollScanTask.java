package com.iwithlong.schedule;

import com.iwithlong.service.ActivityEnrollService;
import lombok.extern.log4j.Log4j;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by mhy on 2018/5/16.
 */
@Component
@Log4j
@EnableScheduling
public class ActivityEnrollScanTask {
    private static final ExecutorService SCHEDULE_POOL= Executors.newFixedThreadPool(5);

    @Resource
    private ActivityEnrollService activityEnrollService;

    @Scheduled(cron = "0/5 * * * * ?")
    public void updateActivityEnrollStatus(){
        SCHEDULE_POOL.execute (new Runnable () {
            @Override
            public void run () {
//                log.info("开始检测启动的报名单是否到截止日期");
                int count = activityEnrollService.updateActivityEnrollStatus();
//                log.info("检测结束," + count + "条报名单已更新");
            }
        });
    }

}
