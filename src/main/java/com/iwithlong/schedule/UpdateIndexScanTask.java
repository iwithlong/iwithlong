package com.iwithlong.schedule;

import com.iwithlong.service.ActivityAreaService;
import com.iwithlong.service.ActivityService;
import com.iwithlong.service.IndexService;
import com.iwithlong.service.LabelService;
import com.iwithlong.vo.ActivityAreaVo;
import com.iwithlong.vo.ActivityVo;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * 定时任务
 * <p>
 * User: TJM
 * Date: 2017/10/26 20:05
 * version $Id: UpdateIndexScanTask.java, v 0.1  20:05 Exp $
 */
@Component
@EnableScheduling
public class UpdateIndexScanTask {


	public static final ExecutorService SCHEDULE_POOL= Executors.newFixedThreadPool  (5);

	@Resource
	private ActivityAreaService activityAreaService;

	@Resource
	private ActivityService activityService;

	@Resource
	private IndexService indexService;


	@Resource
	private LabelService labelService;


 	//每天凌晨3点执行
	@Scheduled(cron = " 0 0 3 * * ?")
	public void updateIndex(){

		SCHEDULE_POOL.execute (new Runnable () {
			@Override
			public void run () {

				List<ActivityAreaVo> allEnable = activityAreaService.findAllEnable ();

				List<ActivityVo> allEnable1 = activityService.findAllEnable ();

				indexService.reconstructorIndex (allEnable1,allEnable);

			}
		});
	}


	//每天凌晨2点执行
	@Scheduled(cron = " 0 0 2 * * ?")
	public void updateHotKeywords(){

		SCHEDULE_POOL.execute (new Runnable () {
			@Override
			public void run () {

				labelService.updateHotKeywords ();

			}
		});
	}



}
