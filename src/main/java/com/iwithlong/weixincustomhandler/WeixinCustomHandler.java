package com.iwithlong.weixincustomhandler;

import java.util.Map;

/**
 * Created by xiexiaohan on 2018/2/1.
 */
public interface WeixinCustomHandler {
    Map<String,Object> dealWithJsonRequest(Map<String,Object> map);
}
