package com.iwithlong.filter.http;

import org.apache.http.Header;
import org.apache.http.cookie.Cookie;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.List;

/**
 * Created by liyd on 16/5/10.
 */
public class ResponseInfo implements Serializable {

    private static final long serialVersionUID = 5348866703929067415L;

    private int statusCode;

    private String contentType;

    private byte[] body;

    private Header[] headers;

    private List<Cookie> cookies;

    public String getStringBody(String encode) {
        try {
            return new String(this.body, encode);
        } catch (UnsupportedEncodingException e) {
            return new String(this.body);
        }
    }

    public String getStringBody() {
        return new String(this.body);
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public String getContentType() {
        return contentType;
    }

    public void setContentType(String contentType) {
        this.contentType = contentType;
    }

    public byte[] getBody() {
        return body;
    }

    public void setBody(byte[] body) {
        this.body = body;
    }

    public Header[] getHeaders() {
        return headers;
    }

    public void setHeaders(Header[] headers) {
        this.headers = headers;
    }

    public List<Cookie> getCookies() {
        return cookies;
    }

    public void setCookies(List<Cookie> cookies) {
        this.cookies = cookies;
    }
}
