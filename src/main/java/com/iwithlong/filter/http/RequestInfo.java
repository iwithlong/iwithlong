package com.iwithlong.filter.http;


import org.apache.commons.lang3.StringUtils;

import java.util.Map;

/**
 * Created by liyd on 16/5/10.
 */
public class RequestInfo {

    /**
     * 协议名称(http或https)
     */
    private String scheme;

    private String requestURI;

    private String requestUrl;

    private String servletPath;

    private String remoteIP;

    private String method;

    private String queryString;

    private String encodeQueryString;

    private Map<String, String> params;

    private Map<String, String> headers;

    private Map<String, String> cookies;

    private String proxyServer;

    private boolean isEncrypt;
    /**
     * 获取完整的请求url
     *
     * @return
     */
    public String getRequestFullUrl() {
        StringBuilder sb = new StringBuilder(this.requestUrl);
        if (StringUtils.isNotBlank(queryString)) {
            sb.append("?").append(queryString);
        }
        return sb.toString();
    }

    /**
     * 获取完整的请求url，queryString中的value经过url编码
     *
     * @return
     */
    public String getRequestEncodeValueFullUrl() {
        StringBuilder sb = new StringBuilder(this.requestUrl);
        if (StringUtils.isNotBlank(encodeQueryString)) {
            sb.append("?").append(encodeQueryString);
        }
        return sb.toString();
    }

    /**
     * 获取参数
     *
     * @param name
     * @return
     */
    public String getParam(String name) {
        return this.params == null ? "" : this.params.get(name);
    }

    public String getScheme() {
        return scheme;
    }

    public String getRequestURI() {
        return requestURI;
    }

    public String getRemoteIP() {
        return remoteIP;
    }

    public String getMethod() {
        return method;
    }

    public String getRequestUrl() {
        return requestUrl;
    }

    public void setRequestUrl(String requestUrl) {
        this.requestUrl = requestUrl;
    }

    public Map<String, String> getParams() {
        return params;
    }

    public Map<String, String> getHeaders() {
        return headers;
    }

    public Map<String, String> getCookies() {
        return cookies;
    }

    public void setScheme(String scheme) {
        this.scheme = scheme;
    }

    public void setRequestURI(String requestURI) {
        this.requestURI = requestURI;
    }

    public void setRemoteIP(String remoteIP) {
        this.remoteIP = remoteIP;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public void setParams(Map<String, String> params) {
        this.params = params;
    }

    public void setHeaders(Map<String, String> headers) {
        this.headers = headers;
    }

    public void setCookies(Map<String, String> cookies) {
        this.cookies = cookies;
    }

    public String getServletPath() {
        return servletPath;
    }

    public void setServletPath(String servletPath) {
        this.servletPath = servletPath;
    }

    public String getQueryString() {
        return queryString;
    }

    public void setQueryString(String queryString) {
        this.queryString = queryString;
    }

    public String getEncodeQueryString() {
        return encodeQueryString;
    }

    public void setEncodeQueryString(String encodeQueryString) {
        this.encodeQueryString = encodeQueryString;
    }

    public String getProxyServer() {
        return proxyServer;
    }

    public void setProxyServer(String proxyServer) {
        this.proxyServer = proxyServer;
    }

    public boolean isEncrypt () {
        return isEncrypt;
    }

    public void setEncrypt ( boolean encrypt ) {
        isEncrypt = encrypt;
    }
}
