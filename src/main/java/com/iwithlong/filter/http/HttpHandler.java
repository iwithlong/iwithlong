package com.iwithlong.filter.http;

import java.io.InputStream;

/**
 * Created by liyd on 16/5/10.
 */
public interface HttpHandler {

    /**
     * post文件
     *
     * @param postUrl         the post url
     * @param requestInfo     the request info
     * @param fileName        the file name
     * @param fileInputStream the file input stream
     * @return response info
     */
    ResponseInfo postMultipart(String postUrl, RequestInfo requestInfo, String fileName, InputStream fileInputStream);

    /**
     * post请求
     *
     * @param postUrl     the post url
     * @param requestInfo the request info
     * @return response info
     */
    ResponseInfo post(String postUrl, RequestInfo requestInfo);

    /**
     * get请求
     *
     * @param getUrl      the get url
     * @param requestInfo the request info
     * @return response info
     */
    ResponseInfo get(String getUrl, RequestInfo requestInfo);

}
