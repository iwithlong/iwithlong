package com.iwithlong.filter.utils;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.lang3.StringUtils;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * 加密工具类
 * <p/>
 * User: shenyongsheng
 * Date: 2016/5/23 11:09
 * version $Id: EncryptionUtils.java, v 0.1  11:09 Exp $
 */
//与hoop-utils包里面重复
@Slf4j
public class EncryptionUtils {
    private static String initKey(String key) {
        return key;
    }

    /**
     * des加密
     *
     * @param key
     * @param content
     * @return
     */
    public static String encryptDes(String key, String content) {
        byte[] bytes = encryptDes(key, content.getBytes());
        return StringUtils.trim(new String(bytes));
    }

    /**
     * des加密
     *
     * @param key
     * @param bytes
     * @return
     */
    public static byte[] encryptDes ( String key, byte[] bytes ) {
        try {
            key = initKey(key);
            DESKeySpec desKey = new DESKeySpec(key.getBytes());
            SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
            SecretKey secureKey = keyFactory.generateSecret(desKey);
            Cipher cipher = Cipher.getInstance("DES");
            cipher.init(Cipher.ENCRYPT_MODE, secureKey);
            byte[] results = cipher.doFinal(bytes);
            return new Base64().encodeBase64(results);
        } catch (Exception e) {
            throw new RuntimeException("对内容进行des加密失败", e);
        }
    }

    /**
     * des解密
     *
     * @param key
     * @param content
     * @return
     */
    public static String decryptDes(String key, String content) {
        byte[] bytes = decryptDes(key, content.getBytes());
        return StringUtils.trim(new String(bytes));
    }

    /**
     * des解密
     *
     * @param key
     * @param bytes
     * @return
     */
    private static byte[] decryptDes(String key, byte[] bytes) {
        try {
            key = initKey(key);
            DESKeySpec desKey = new DESKeySpec(key.getBytes());
            SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");
            SecretKey secureKey = keyFactory.generateSecret(desKey);
            Cipher cipher = Cipher.getInstance("DES/ECB/NoPadding");
            cipher.init(Cipher.DECRYPT_MODE, secureKey);
            byte[] result = cipher.doFinal(Base64.decodeBase64(bytes));
            return result;
        } catch (Exception e) {
            throw new RuntimeException("对内容进行des解密密失败", e);
        }
    }

    /**
     * 使用sha-1算法加密
     *
     * @param decript
     * @return
     */
    public static String SHA1(String decript) {
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-1");
            digest.update(decript.getBytes());
            byte[] messageDigest = digest.digest();
            return byteToHexString(messageDigest);
        } catch (NoSuchAlgorithmException e) {
            log.warn("SHA1 加密算法失败", e);
        }
        return "";
    }

    /**
     * 使用sha算法加密数据
     *
     * @param decript
     * @return
     */
    public static String SHA(String decript) {
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA");
            digest.update(decript.getBytes());
            byte[] messageDigest = digest.digest();
            return byteToHexString(messageDigest);
        } catch (NoSuchAlgorithmException e) {
            log.warn("SHA 加密算法失败", e);
        }
        return "";
    }

    /**
     * 使用md5算法加密文本
     *
     * @param decirpt
     * @return
     */
    public static String MD5(String decirpt) {
        try {
            //获取MD5摘要算法的messageDigest对象
            MessageDigest mdInst = MessageDigest.getInstance("MD5");
            //使用指的字节更新信息
            mdInst.update(decirpt.getBytes());
            //获得密文
            byte[] md = mdInst.digest();
            //把密文转换为16进制数
            return byteToHexString(md);
        } catch (NoSuchAlgorithmException e) {
            log.warn("MD5 加密算法失败", e);
        }
        return "";
    }

    private static String byteToHexString(byte[] messageDigest){
        StringBuffer hexString = new StringBuffer();
        for (int i = 0; i < messageDigest.length; i++) {
            String shaHex = Integer.toHexString(messageDigest[i] & 0xFF);
            if (shaHex.length() < 2) {
                hexString.append(0);
            }
            hexString.append(shaHex);
        }
        return hexString.toString();
    }

}
