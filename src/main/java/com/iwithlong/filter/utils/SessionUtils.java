package com.iwithlong.filter.utils;


import com.ktanx.platform.web.LoginUser;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * Created by liyingdan on 2016/6/6.
 */
public final class SessionUtils {

    /**
     * 放入登录用户信息到session
     *
     * @param request
     * @param loginUser
     */
    public static void setSessionUser(HttpServletRequest request, LoginUser loginUser) {

        setSessionUser(request.getSession(),loginUser);
    }

    /**
     * 放入登录用户信息到session
     *
     * @param session
     * @param loginUser
     */
    public static void setSessionUser(HttpSession session, LoginUser loginUser) {
        session.setAttribute("LOGIN_USER", loginUser);
    }

    /**
     * 获取session中的登录用户信息
     *
     * @param request
     * @return
     */
    public static LoginUser getSessionUser(HttpServletRequest request) {
        return getSessionUser(request.getSession());
    }


    /**
     * 获取session中的登录用户信息
     *
     * @param session
     * @return
     */
    public static LoginUser getSessionUser(HttpSession session) {
        return (LoginUser) session.getAttribute("LOGIN_USER");
    }


}
