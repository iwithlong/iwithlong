package com.iwithlong.filter.utils;

import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.impl.client.CloseableHttpClient;

import java.io.IOException;

/**
 * Created by liyd on 16/5/10.
 */
public class AgentHttpUtils {

    public static void closeResponse(CloseableHttpResponse response) {

        try {
            response.close();
        } catch (IOException e) {
            //ignore
        }
    }

    public static void closeHttpClient(CloseableHttpClient httpClient) {

        try {
            httpClient.close();
        } catch (IOException e) {
            //ignore
        }
    }

}
