package com.iwithlong.filter.handler;



import com.iwithlong.filter.http.RequestInfo;
import com.iwithlong.filter.http.ResponseInfo;
import com.ktanx.platform.web.LoginUser;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by liyingdan on 2017/1/5.
 */
public interface ForwardHandler {

    /**
     * 是否是有效的
     *
     * @param requestURI
     * @param request
     * @return
     */
    boolean isValid (String requestURI, HttpServletRequest request);

    /**
     * request处理
     *
     * @param request
     * @return
     */
    RequestInfo buildForwardRequestInfo (HttpServletRequest request);

    /**
     * 请求目标服务器
     *
     * @param requestInfo
     * @param forwardUrl
     * @param request
     * @return
     */
    ResponseInfo requestTargetServer (RequestInfo requestInfo, String forwardUrl, HttpServletRequest request);

    /**
     * 写返回信息
     *
     * @param responseInfo
     * @param request
     * @param response
     * @return
     */
    boolean writeResponse (ResponseInfo responseInfo, HttpServletRequest request, HttpServletResponse response);
    /**
     * 写返回信息
     *
     * @param requestInfo
     * @return
     */
    boolean isFilter (RequestInfo requestInfo, HttpServletRequest request);
}
