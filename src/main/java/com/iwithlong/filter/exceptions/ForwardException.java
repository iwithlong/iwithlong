package com.iwithlong.filter.exceptions;

import com.imesne.assistant.common.enums.IEnum;
import com.imesne.assistant.common.exception.ImesneException;

/**
 * Created by liyingdan on 2017/1/4.
 */
public class ForwardException extends ImesneException {


    public ForwardException(IEnum e) {
        super(e);
    }

    public ForwardException(String message, Throwable e) {
        super(message, e);
    }

    public ForwardException(IEnum iEnum, Throwable e) {
        super(iEnum, e);
    }

    public ForwardException(Throwable e) {
        super(e);
    }

    public ForwardException(String message) {
        super(message);
    }

    public ForwardException(String code, String message) {
        super(code, message);
    }

    public ForwardException(String code, String message, Throwable e) {
        super(code, message, e);
    }
}
