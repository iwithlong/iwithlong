package com.iwithlong.log;

import com.iwithlong.entity.OperateLog;
import com.iwithlong.service.OperateLogService;
import com.ktanx.platform.utils.HttpUtils;
import com.ktanx.platform.utils.SpringUtils;
import com.ktanx.platform.web.LoginUser;
import org.apache.commons.lang3.StringUtils;

import javax.servlet.http.HttpServletRequest;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * User : liulu
 * Date : 17-8-29 下午4:27
 * version $Id: LogUtil.java, v 0.1 Exp $
 */
public class LogUtil {

    private static final ExecutorService POOL = Executors.newFixedThreadPool(20);

    private static OperateLogService logService;

    public static void log(final OperateLog log) {

        if (logService == null) {
            logService = SpringUtils.getBean(OperateLogService.class);
        }

        POOL.execute(new Runnable() {
            @Override
            public void run() {
                logService.log(log);
            }
        });
    }

    public static void setSessionOperator(OperateLog operateLog) {

        HttpServletRequest request = HttpUtils.getRequest();
        // 会员登录
//        MemberVo member = SessionUtil.getSessionMember(request.getSession());
        // 管理员登录
        LoginUser loginUser = HttpUtils.getLoginUser();

        /*if (member != null) {
            operateLog.setOperator(member.getMobile());
            operateLog.setOperatorName(member.getMemberName());
        } else*/
        if (loginUser != null) {
            operateLog.setOperator(loginUser.getUsername());
            operateLog.setOperatorName(loginUser.getRealName());
        } else {
            operateLog.setOperator("anonymous");
            operateLog.setOperatorName("匿名用户");
        }
    }

    public static void setLogContent(LogOperatorType operatorType, String logContent) {
        setLogContent(LogLevel.MAJOR, operatorType, logContent);
    }

    public static void setLogContent(LogLevel logLevel, LogOperatorType operatorType, String logContent) {
        OperateLog operateLog = ThreadLocalStore.getOperateLog();
        operateLog.setLogLevel(logLevel.getCode());
        operateLog.setOperatorType(operatorType.getCode());
        operateLog.setLogContent(logContent);
    }

    /**
     * 脱敏
     *
     * @param source
     * @param desensitizeEnum 不能为　null
     * @return
     */
    public static String desensitize(String source, DesensitizeEnum desensitizeEnum) {
        String card = "*****************************************************";

        switch (desensitizeEnum) {
            case MOBILE:
                return StringUtils.substring(source, 0, 3) + "****" + StringUtils.substring(source, source.length() - 4, source.length());
            case BANK_CARD:
                int length = source.length();

                String start = StringUtils.substring(source, 0, 4);
                String middle = StringUtils.substring(card, 0, length - 8);
                String end = StringUtils.substring(source, length - 4, length);

                return start + middle + end;
            default:
                return source;
        }

    }

    public enum DesensitizeEnum {

        MOBILE,
        BANK_CARD,;

    }

}
