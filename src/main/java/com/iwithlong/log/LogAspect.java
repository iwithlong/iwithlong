package com.iwithlong.log;

import com.iwithlong.entity.OperateLog;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

/**
 * User : liulu
 * Date : 17-8-28 上午11:00
 * version $Id: LogAspect.java, v 0.1 Exp $
 */
@Aspect
@Component
@Slf4j
public class LogAspect {


    @AfterReturning(value = "@annotation(logable)", returning = "retValue")
    public void afterReturning(JoinPoint joinPoint, Logable logable, Object retValue) {

        OperateLog operateLog = ThreadLocalStore.getOperateLog();

        LogUtil.setSessionOperator(operateLog);

        if (StringUtils.isEmpty(operateLog.getLogContent()) && StringUtils.isEmpty(operateLog.getOperatorType())) {
            operateLog.setLogLevel(logable.logLeVel().getCode());
            operateLog.setOperatorType(logable.operatorType().getCode());
            operateLog.setLogContent(logable.logContent());
        }

        ThreadLocalStore.removeOprateLog();
        LogUtil.log(operateLog);
    }


}
