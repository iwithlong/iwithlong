package com.iwithlong.log;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * User : liulu
 * Date : 17-8-28 下午2:03
 * version $Id: Logable.java, v 0.1 Exp $
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface Logable {

    LogLevel logLeVel() default LogLevel.MINOR;

    LogOperatorType operatorType() default LogOperatorType.OP_WATER;

    String logContent() default "";


}
