package com.iwithlong.log;


import com.iwithlong.entity.OperateLog;

/**
 * User : liulu
 * Date : 17-8-28 上午11:22
 * version $Id: ThreadLocalStore.java, v 0.1 Exp $
 */
public class ThreadLocalStore {

    private static ThreadLocal<OperateLog> operateLog = new ThreadLocal<>();


    public static void setOperateLog(OperateLog log) {
        operateLog.set(log);
    }

    public static OperateLog getOperateLog() {
        return operateLog.get();
    }

    public static void removeOprateLog() {
        operateLog.remove();
    }


}
