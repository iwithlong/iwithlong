package com.iwithlong.log;

import com.imesne.assistant.common.enums.IEnum;
import lombok.Getter;

/**
 * User : liulu
 * Date : 17-8-29 下午5:03
 * version $Id: LogOperatorType.java, v 0.1 Exp $
 */
@Getter
public enum LogOperatorType implements IEnum {

    MEMBER("member", "会员操作"),
    PAY("pay", "支付操作"),
    CARD("bank_card", "银行卡操作"),
    SYSTEM("system_config", "系统配置"),
    OP_WATER("op_water", "操作流水"),;

    private String code;
    private String desc;

    LogOperatorType(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

}
