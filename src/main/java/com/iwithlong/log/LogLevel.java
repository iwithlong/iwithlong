package com.iwithlong.log;

import com.imesne.assistant.common.enums.IEnum;
import lombok.Getter;

/**
 * User : liulu
 * Date : 17-8-28 下午3:05
 * version $Id: LogLevel.java, v 0.1 Exp $
 */
@Getter
public enum LogLevel implements IEnum {

    TRIVIAL("trivial", "微小"),
    MINOR("minor", "轻微"),
    MAJOR("major", "重要"),
    CRITICAL("critical", "严重"),
    BLOCKER("blocker", "危险"),
    ERROR("error", "错误"),;


    private String code;
    private String desc;

    LogLevel(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }
}
