//package com.iwithlong;
//
//import com.iwithlong.interceptor.MerchantInterceptor;
//import com.iwithlong.log.LogInterceptor;
//import com.ktanx.platform.web.interceptor.SessionInterceptor;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
//import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
//
///**
// * Created by mhy on 2018/6/7.
// */
//@Configuration
//public class IwlWebConfig extends WebMvcConfigurerAdapter {
//    @Override
//    public void addInterceptors(InterceptorRegistry registry) {
//        registry.addInterceptor(new MerchantInterceptor()).addPathPatterns("/**");
//        registry.addInterceptor(new SessionInterceptor()).addPathPatterns("/**");
//    }
//}
