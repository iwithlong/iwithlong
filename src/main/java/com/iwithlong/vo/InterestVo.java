package com.iwithlong.vo;

import com.imesne.assistant.common.page.Pageable;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * Created by xiexiaohan on 2017/10/26.
 */
@Getter
@Setter
public class InterestVo extends Pageable{
    /**
     * 兴趣ID
     */
    private long interestId;

    /**
     * 活动ID
     */
    private long activityId;

    /**
     * 拥有者
     */
    private String owner;

    /**
     * 创建时间
     */
    private Date gmtCreate;
}
