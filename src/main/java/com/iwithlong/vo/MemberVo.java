package com.iwithlong.vo;

import com.imesne.assistant.common.page.Pageable;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * User : liulu
 * Date : 2017/6/6 18:49
 * version $Id: MemberVo.java, v 0.1 Exp $
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class MemberVo extends Pageable {

    private static final long serialVersionUID = -674164008671511047L;

    /**
     * 会员用户ID
     */
    private Long memberId;


    /**
     * 手机号
     */
    private String mobile;

    /**
     * 帐号
     */
    private String memberCode;

    /**
     * 密码
     */
    private String memberPassword;

    /**
     * 姓名
     */
    private String memberName;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 性别
     */
    private Boolean sex;

    /**
     * 出生日期
     */
    private Date birthday;



    /**
     * 会员等级
     */
    private String rank;

    /**
     * 用户状态：
     01,启用
     02,禁用
     */
    private String status;


    /**
     * 会员成长值
     */
    private Integer balance;


    /**
     *微信帐号
     */
    private String weixin;

    /**
     * 头像  URL
     */
    private String head;

    /**
     * 昵称
     */
    private String nickName;

    /**
     * 查询关键字
     */
    private String keywords;

    /**
     * 会员类型
     */
    private String memberType;

    public Integer getBalance () {

        if(balance ==null){
            return 0;
        }

        return balance;
    }

    public String getSexStr () {
        if (sex == null) {
            return "";
        }
        return sex?"男":"女";
    }


}
