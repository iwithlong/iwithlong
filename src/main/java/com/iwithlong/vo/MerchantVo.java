package com.iwithlong.vo;

import com.imesne.assistant.common.page.Pageable;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by mhy on 2018/4/25.
 */
@Getter
@Setter
public class MerchantVo extends Pageable {
    /**
     * 会员用户ID
     */
    private Long memberId;

    /**
     * 帐号
     */
    private String memberCode;

    /**
     * 密码
     */
    private String memberPassword;

    /**
     * 姓名
     */
    private String memberName;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 用户状态：
     01,启用
     02,禁用
     */
    private String status;

    /**
     * 头像  URL
     */
    private String head;

    /**
     * 昵称
     */
    private String nickName;

    /**
     * 商户电话
     */
    private String serviceTel;

    /**
     * 账户类型
     */
    private String accountType;

    /**
     * 账户号
     */
    private String accountCode;

    /**
     * 账户名称
     */
    private String accountName;

    /**
     * 商户简介
     */
    private String intro;

    /**
     * 会员类型
     */
    private String memberType;

    /**
     * 是否启用
     */
    private boolean enable;
}
