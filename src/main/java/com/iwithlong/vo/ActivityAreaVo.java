package com.iwithlong.vo;

import com.imesne.assistant.common.page.Pageable;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by xiexiaohan on 2017/10/10.
 */
@Getter
@Setter
public class ActivityAreaVo extends Pageable{
    /**
     * 活动点ID
     */
    private Long activityAreaId;

    /**
     * 活动点名称
     */
    private String areaName;

    /**
     * 简述
     */
    private String brief;

    /**
     * 活动点介绍
     */
    private String areaIntroduction;

    /**
     * 省份
     */
    private String province;

    /**
     * 城市
     */
    private String city;

    /**
     * 区、镇
     */
    private String district;

    /**
     * 纬度值
     */
    private Double latitude;

    /**
     * 经度值
     */
    private Double longitude;

    /**
     * 地址
     */
    private String address;

    /**
     * 创建人
     */
    private String creator;

    /**
     * 创建时间
     */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date gmtCreate;

    /**
     * 修改人
     */
    private String modifier;

    /**
     * 修改时间
     */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date gmtModify;

    /**
     * 状态
     */
    private String status;

    /**
     * 退回理由
     */
    private String reason;

    /**
     * 审核人
     */
    private String auditor;

    /**
     * 审核时间
     */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date gmtAudit;

    /**
     * 发布人
     */
    private String issuer;

    /**
     * 发布时间
     */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date gmtIssue;

    /**
     * 是否启用
     */
    private boolean enable;

    /**
     * 文件集合
     */
    private List<UploadFileVo> uploadFileVos = new ArrayList<>();

    /**
     * 活动集合
     */
    private List<ActivityVo> activityVos = new ArrayList<>();

    /**
     * 左上角坐标
     */
    private String leftCorner;

    /**
     * 右下角坐标
     */
    private String rightCorner;

    /**
     *首张图片路径
     */
    private String firstPicUrl;


    public void setActivityVo(ActivityVo vo){

        if (activityVos==null){
            activityVos=new ArrayList<> ();
        }

        activityVos.add (vo);

    }

    /**
     * 移动端所需id
     */
    private long id;

    /**
     * 移动端所需title
     */
    private String title;

    /**
     * 查询用启用状态
     */
    private String enableSearch;

    /**
     * 关键词
     */
    private String keyword;

}
