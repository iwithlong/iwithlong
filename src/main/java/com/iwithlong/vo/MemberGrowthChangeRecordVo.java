package com.iwithlong.vo;

import com.imesne.assistant.common.page.Pageable;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;

/**
 * 会员成长变动记录
 * <p>
 * User: TJM
 * Date: 2017/8/25 10:56
 * version $Id: MemberGrowthChangeRecord.java, v 0.1  10:56 Exp $
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class MemberGrowthChangeRecordVo extends Pageable {

	private static final long serialVersionUID = -8590629093242200070L;
	/**
	 * 会员成长变动记录ID
	 */
	private Long memberGrowthChangeRecordId;

	/**
	 * 会员成长值ID
	 */
	private Long memberGrowthAccountId;

	/**
	 * 会员ID
	 */
	private Long memberId;


	/**
	 * 变动值
	 */
	private Integer changeValue;

	/**
	 * 摘要
	 */
	private String memo;

	/**
	 * 记录时间
	 */
	private Date gmtRecord;


}
