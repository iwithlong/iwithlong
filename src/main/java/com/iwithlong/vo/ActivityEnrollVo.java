package com.iwithlong.vo;

import com.imesne.assistant.common.page.Pageable;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;
import java.util.List;

/**
 * Created by mhy on 2018/5/1.
 */
@Getter
@Setter
public class ActivityEnrollVo extends Pageable {
    /**
     * 报名单ID
     */
    private Long activityEnrollId;

    /**
     * 活动
     */
    private ActivityVo activityVo;

    private Long activityId;

    /**
     * 商家
     */
    private MerchantVo merchantVo;

    private Long memberId;

    private List<EnrollProductVo> EnrollProductVos;

    /**
     * 标题
     */
    private String title;

    /**
     * 地址
     */
    private String address;

    /**
     * 开始时间
     */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm")
    private Date gmtBegin;

    /**
     * 结束时间
     */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm")
    private Date gmtEnd;

    private String gmtEndStr;

    /**
     * 备注
     */
    private String memo;

    /**
     * 状态
     */
    private String status;

    /**
     * 创建时间
     */
    private Date gmtCreate;

    /**
     * 创建人
     */
    private String creator;

    @DateTimeFormat(pattern="yyyy-MM-dd")
    private Date beginTime;

    @DateTimeFormat(pattern="yyyy-MM-dd")
    private Date endTime;

    /**
     * 活动开始时间
     */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm")
    private Date gmtActivityStart;

    /**
     * 活动结束时间
     */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm")
    private Date gmtActivityEnd;
}
