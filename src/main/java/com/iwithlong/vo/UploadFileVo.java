package com.iwithlong.vo;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * Created by xiexiaohan on 2017/10/11.
 */
@Getter
@Setter
public class UploadFileVo {
    /**
     * 上传文件ID
     */
    private Long uploadFileId;

    /**
     * 业务文件ID
     */
    private Long bizUploadFileId;

    /**
     * 文件名
     */
    private String fileName;

    /**
     * 文件路径：采用相对路径
     */
    private String fileUrl;

    /**
     * 上传时间
     */
    private Date gmtUpload;

    /**
     * 序顺号
     */
    private Long ord;
}
