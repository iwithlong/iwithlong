package com.iwithlong.vo;

import com.imesne.assistant.common.page.Pageable;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by xiexiaohan on 2017/10/10.
 */
@Getter
@Setter
public class ActivityVo extends Pageable {

    /**
     * 活动ID
     */
    private Long activityId;

    /**
     * 活动点ID
     */
    private Long activityAreaId;

    /**
     * 报名单信息
     */
    private List<ActivityEnrollVo> activityEnrollVos;

    /**
     * 名称
     */
    private String name;

    /**
     * 简述
     */
    private String brief;

    /**
     * 详情
     */
    private String content;

    /**
     * 阅数量
     */
    private Long numberOfClick;

    /**
     * 兴趣量
     */
    private Long numberOfInterest;

    /**
     * 收费情况
     */
    private String charging;

    /**
     * 推荐度：取值0~5
     */
    private Long recommended;

    /**
     * 联系电话
     */
    private String telephone;

    /**
     * 开始时间
     */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm")
    private Date beginDate;

    /**
     * 结束时间
     */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm")
    private Date endDate;

    /**
     * 创建人
     */
    private String creator;

    /**
     * 创建时间
     */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date gmtCreate;

    /**
     * 状态
     */
    private String status;

    /**
     * 修改人
     */
    private String modifier;

    /**
     * 修改时间
     */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date gmtModify;

    /**
     * 退回理由
     */
    private String reason;

    /**
     * 审核人
     */
    private String auditor;

    /**
     * 审核时间
     */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date gmtAudit;

    /**
     * 发布人
     */
    private String issuer;

    /**
     * 发布时间
     */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm:ss")
    private Date gmtIssue;

    /**
     * 标签
     */
    private String activityLabel;

    /**
     * 关键字
     */
    private String keyword;

    /**
     * 权重
     */
    private double weight;

    /**
     * 是否启用
     */
    private boolean enable;

    /**
     * 图片集合
     */
    private List<UploadFileVo> uploadFileVos = new ArrayList<>();

    /**
     * 是否感兴趣
     */
    private boolean isInterest;

    /**
     * 所属活动点
     */
    private ActivityAreaVo activityAreaVo;

    /**
     * 首张图片url
     */
    private String firstPicUrl;

    /**
     * 是否禁用 搜索用
     */
    private String enableSearch;


    /**
     * 活动点 状态
     */
    private String activityAreaStatus;


    /**
     * 活动点 是否启用
     */
    private boolean activityAreaEnable;



    /**
     * 活动点的
     * 纬度值
     */
    private Double latitude;

    /**
     * 活动点的
     * 经度值
     */
    private Double longitude;


    /**
     *  该活动下的用户
     */
    private String weixinStrs;

    /**
     * 推荐值查询开始值
     */
    private Long recommendedStart;

    /**
     * 推荐值查询结束值
     */
    private Long recommendedEnd;

    /**
     * 开始时间查询开始值
     */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm")
    private Date beginDateStart;

    /**
     * 开始时间查询结束值
     */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm")
    private Date beginDateEnd;

    /**
     * 结束时间查询开始值
     */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm")
    private Date endDateStart;

    /**
     * 结束时间查询开始值
     */
    @DateTimeFormat(pattern="yyyy-MM-dd HH:mm")
    private Date endDateEnd;

    private String beginDateStr;

    private String endDateStr;

    /**
     * 排序方式
     */
    private String orderBy;

    /**
     * 排序类型
     */
    private String orderByType;

    /**
     * 购票地址
     */
    private String ticketUrl;

}
