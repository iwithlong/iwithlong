package com.iwithlong.vo;

import com.imesne.assistant.common.page.Pageable;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.StringUtils;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * Write class comments here
 * <p>
 * User: TJM
 * Date: 2017/10/25 19:37
 * version $Id: SearchVo.java, v 0.1  19:37 Exp $
 */
@Getter
@Setter
public class SearchVo extends Pageable {



	/**
	 * 左上角坐标  逗号分割，如：200.22,300.33
	 */
	private String leftCorner;

	/**
	 * 右下角坐标
	 */
	private String rightCorner;

	/**
	 * 当前坐标点 经纬度
	 */
	private String nowCorner;


	private String keyword;

	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Date beginTime;

	@DateTimeFormat(pattern="yyyy-MM-dd")
	private Date endTime;

	private String activityLabel;

	/**
	 * 排序类型
	 */
	private String sortType;

	/**
	 *  该活动下的用户
	 */
	private String weixin;

	/**
	 * 是否查找感兴趣，是：传weixin
	 */
	private Boolean searchInterest;

	/**
	 * 活动点ID
	 */
	private Long activityAreaId;

	/**
	 * X经  Y纬
	 */
	private Double xleft;
	private Double yleft;


	private Double xright;
	private Double yright;

	private Double xnow;
	private Double ynow;



	public Double getXleft () {
		if (StringUtils.isNotEmpty (leftCorner)) {
			String[] leftSplit= leftCorner.split (",");
			xleft=Double.valueOf (leftSplit[0]);
		}else{
			xleft=0D;
		}
		return xleft;
	}

	public Double getYleft () {
		if (StringUtils.isNotEmpty (leftCorner)) {
			String[] leftSplit= leftCorner.split (",");
			yleft=Double.valueOf (leftSplit[1]);
		}else{
			yleft=0D;
		}
		return yleft;
	}

	public Double getXright () {

		if (StringUtils.isNotEmpty (rightCorner)) {
			String[] rightSplit= rightCorner.split (",");
			xright=Double.valueOf (rightSplit[0]);
		}else{
			xright=0D;
		}
		return xright;
	}

	public Double getYright () {
		if (StringUtils.isNotEmpty (rightCorner)) {
			String[] rightSplit= rightCorner.split (",");
			yright=Double.valueOf (rightSplit[1]);
		}else{
			yright=0D;
		}
		return yright;
	}


	public Double getXnow () {

		if (StringUtils.isNotEmpty (nowCorner)) {
			String[] nowSplit= nowCorner.split (",");
			xnow=Double.valueOf (nowSplit[0]);
		}else{
			xnow=0D;
		}
		return xnow;
	}

	public Double getYnow () {
		if (StringUtils.isNotEmpty (nowCorner)) {
			String[] nowSplit= nowCorner.split (",");
			ynow=Double.valueOf (nowSplit[1]);
		}else{
			ynow=0D;
		}
		return ynow;
	}


}
