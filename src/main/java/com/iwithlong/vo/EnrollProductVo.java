package com.iwithlong.vo;

import com.imesne.assistant.common.page.Pageable;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by mhy on 2018/5/11.
 */
@Getter
@Setter
public class EnrollProductVo extends Pageable{
    private Long enrollProductId;

    private Long activityEnrollId;

    /**
     * 名称
     */
    private String name;

    /**
     * 数量
     */
    private int quantity;

    /**
     * 剩余数量
     */
    private int left;

    /**
     * 摘要
     */
    private String memo;

    /**
     * 已报名数量
     */
    private int enrolledQuantity;


    /**
     * 顺序号
     */
    private int ord;
}
