package com.iwithlong.vo;

import lombok.Getter;
import lombok.Setter;

/**
 * Created by xiexiaohan on 2017/10/11.
 */
@Getter
@Setter
public class BizUploadFIleVo {

    /**
     * 业务文件ID
     */
    private Long bizUploadFileId;

    /**
     * 业务类型
     */
    private String bizType;

    /**
     * 业务值
     */
    private Long bizId;
}
