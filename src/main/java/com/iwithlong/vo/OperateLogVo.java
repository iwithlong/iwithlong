package com.iwithlong.vo;

import com.imesne.assistant.common.page.Pageable;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * User : liulu
 * Date : 17-9-9 下午6:04
 * version $Id: OperateLogVo.java, v 0.1 Exp $
 */
@Getter
@Setter
public class OperateLogVo extends Pageable {

    private static final long serialVersionUID = -4978459932992459607L;

    /**
     * 操作人
     */
    private String operator;

    /**
     * 操作类型
     */
    private String operatorType;

    private Date startTime;

    private Date endTime;

}
