package com.iwithlong.vo;

import com.imesne.assistant.common.page.Pageable;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.List;

/**
 * Created by mhy on 2018/5/31.
 */
@Getter
@Setter
public class ProductSmsVo extends Pageable{
    /**
     * 产品短信ID
     */
    private Long productSmsId;

    /**
     * 报名产品ID
     */
    private Long activityEnrollId;

    /**
     * 标题
     */
    private String title;

    /**
     * 详情
     */
    private String content;

    /**
     * 数量
     */
    private int quantity;

    /**
     * 账号
     */
    private String memberCode;

    private String activityEnrollName;

    /**
     * 状态
     */
    private String status;

    /**
     * 创建时间
     */
    private Date gmtCreate;

    /**
     * 审核时间
     */
    private Date gmtAudit;

    /**
     * 审核人
     */
    private String auditor;

    /**
     * 发送时间
     */
    private Date gmtSend;

    /**
     * 驳回原因
     */
    private String reason;

    /**
     * 报名名单id汇总
     */
    private String enrollNamelistIds;

    /**
     * 短信参数
     */
    private List<String> params;

    private String memberCodeEqual;
}
