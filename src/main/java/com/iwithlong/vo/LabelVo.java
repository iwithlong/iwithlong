package com.iwithlong.vo;

import com.imesne.assistant.common.page.Pageable;
import lombok.Getter;
import lombok.Setter;

/**
 * 标签Vo
 * <p>
 * User: TJM
 * Date: 2017/9/25 15:21
 * version $Id: LabelVo.java, v 0.1  15:21 Exp $
 */
@Getter
@Setter
public class LabelVo extends Pageable {


	private static final long serialVersionUID = -5188197805690273986L;
	/**
	 * 标签ID
	 */
	private Long labelId;

	/**
	 * 标签名
	 */
	private String labelName;

	/**
	 * 标签类型
	 */
	private String labelType;

	/**
	 * 序顺号
	 */
	private Integer ord;

	/**
	 * 关键字
	 */
	private String keyword;

}
