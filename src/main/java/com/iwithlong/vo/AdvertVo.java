package com.iwithlong.vo;

import com.imesne.assistant.common.page.Pageable;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * Created by mhy on 2018/5/19.
 */
@Getter
@Setter
public class AdvertVo extends Pageable {

    private Long advertId;

    private Long bizId;

    private String bizType;

    private String bizName;

    private String advertType;

    private String title;

    private String status;

    private String image;

    private Integer orderNo;

    private String creator;

    private Date gmtCreate;

    private String fmtGmtCreate;

    private String description;

    private ActivityVo activity;

    private String issuer;

    private Date gmtIssue;


}
