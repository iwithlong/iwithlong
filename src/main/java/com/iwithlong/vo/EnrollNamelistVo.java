package com.iwithlong.vo;

import com.imesne.assistant.common.page.Pageable;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.List;

/**
 * Created by mhy on 2018/5/11.
 */
@Getter
@Setter
public class EnrollNamelistVo extends Pageable{
    private Long activityEnrollId;
    private Long enrollNamelistId;
    private Long enrollProductId;
    private String enrollProductName;

    private EnrollProductVo enrollProductVo;

    private ActivityEnrollVo activityEnrollVo;

    private ActivityVo activityVo;

    private List<ProductSmsVo> productSmsVos;

    /**
     * 出勤率
     */
    private Double attendance;

    /**
     * 名称
     */
    private String name;

    /**
     * 联系电话
     */
    private String mobile;

    /**
     * 备注
     */
    private String memo;

    /**
     * 状态
     */
    private String status;

    /**
     * 人数
     */
    private int quantity;

    /**
     * 报名时间
     */
    private Date gmtEnroll;

    /**
     * 微信
     */
    private String weixin;
}
