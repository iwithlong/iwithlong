package com.iwithlong.utils;

import com.iwithlong.context.Constants;
import com.iwithlong.vo.MemberVo;

import javax.servlet.http.HttpSession;

/**
 * Write class comments here
 * <p>
 * User: TJM
 * Date: 2017/10/24 18:35
 * version $Id: SessionUtil.java, v 0.1  18:35 Exp $
 */
public class SessionUtil {

	public static MemberVo getSessionMember(HttpSession session) {
		return  (MemberVo) session.getAttribute(Constants.LOGIN_MEMBER);
	}

}
