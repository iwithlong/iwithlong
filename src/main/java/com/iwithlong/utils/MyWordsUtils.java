package com.iwithlong.utils;

import java.io.*;
import java.util.List;

/**
 * 我的词库 工具
 * <p>
 * User: TJM
 * Date: 2018/1/22 8:46
 * version $Id: MyWordsUtils.java, v 0.1  8:46 Exp $
 */
public class MyWordsUtils {


	private static String fileName=MyWordsUtils.class.getClassLoader ().getResource ("data/words-my.dic").getPath ();


	public static void clearMyWords(){

		File file=new File (fileName);


		try {
			if(!file.exists()) {
				file.createNewFile();
			}
			FileWriter fileWriter =new FileWriter(file);
			fileWriter.write("");
			fileWriter.flush();
			fileWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}


	public static void infoMyWords(List<String > myWords){


		File file =new File(fileName);

		try {

			if(!file.exists()){
				file.createNewFile();
			}

			FileOutputStream writerStream = new FileOutputStream(file);
			BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(writerStream, "UTF-8"));

			for (String myWord : myWords) {
				writer.write (myWord+System.getProperty("line.separator"));
			}

			writer.flush();
			writer.close();

		} catch (IOException e) {

			e.printStackTrace();

		}

	}



}
