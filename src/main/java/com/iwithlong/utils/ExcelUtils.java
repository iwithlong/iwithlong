package com.iwithlong.utils;

import ch.qos.logback.core.util.FileUtil;
import com.iwithlong.enums.EnrollNamelistStatus;
import com.iwithlong.vo.EnrollNamelistVo;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.HSSFColor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Iterator;
import java.util.List;

/**
 * Created by mhy on 2018/5/17.
 */
public class ExcelUtils {
//    public static String createExcelWorkbood(String uploadFilePath,String mould,String fileName) throws IOException{
//        String temp;
//        switch (mould){
//            case "enrollNamelist":temp="/excel/enrollNamelist.xls";break;
//            default: temp="";break;
//        }
//        InputStream inputStream = FileUtil.class.getResourceAsStream(temp);
//        HSSFWorkbook workBook = new HSSFWorkbook(inputStream);
//        HSSFSheet sheet = workBook.getSheetAt(0);
////        //修改标题
////        HSSFRow row0 = sheet.getRow(0);
////        setCellValue(row0,0,title);
//////        String filePath = uploadFilePath+mould+"/"+fileName+".xls";
////        File file = new File(fileName+".xls");
//////        if(!file.getParentFile().exists()){
//////            file.getParentFile().mkdir();
//////        }
//////        if(!file.exists()){
//////            file.createNewFile();
//////        }
////        FileOutputStream out = new FileOutputStream(file);
//
//    }

    public static void createExcelWorkbood(String uploadPath,HttpServletResponse response ,List<EnrollNamelistVo> enrollNamelistVos,String fileName) throws IOException{
        String temp="/excel/enrollNamelist.xls";
        InputStream inputStream = FileUtil.class.getResourceAsStream(temp);
        HSSFWorkbook workBook = new HSSFWorkbook(inputStream);
        HSSFCellStyle cellStyle = getCellStyle(workBook);
        HSSFSheet sheet = workBook.getSheetAt(0);
        int rownNum = sheet.getLastRowNum()+1;
        Iterator<EnrollNamelistVo> iterator = enrollNamelistVos.iterator();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy年MM月dd日 hh:mm:ss");
        while (iterator.hasNext()){
            EnrollNamelistVo enrollNamelistVo = iterator.next();
            HSSFRow row = sheet.createRow(rownNum);
            createNewCell(cellStyle,row,0,String.valueOf(rownNum-1));
            createNewCell(cellStyle,row,1,enrollNamelistVo.getName());
            createNewCell(cellStyle,row,2,enrollNamelistVo.getMobile());
            createNewCell(cellStyle,row,3,String.valueOf(enrollNamelistVo.getAttendance()*100)+"%");
            createNewCell(cellStyle,row,4,formatter.format(enrollNamelistVo.getGmtEnroll()));
            createNewCell(cellStyle,row,5,getEnrollNamelistStatusValueByCode(enrollNamelistVo.getStatus()));
            createNewCell(cellStyle,row,6,enrollNamelistVo.getEnrollProductName());
            createNewCell(cellStyle,row,7,enrollNamelistVo.getMemo());
            rownNum++;
        }
        fileName = fileName+".xls";
        String filePath = uploadPath+fileName;
        File file = new File(filePath);
        if(!file.getParentFile().exists()){
            file.getParentFile().mkdir();
        }
        if(!file.exists()){
            file.createNewFile();
        }
        OutputStream fileOutPutStream = new FileOutputStream(file);
        workBook.write(fileOutPutStream);
        fileOutPutStream.close();
        inputStream.close();

        response.setContentType("application/x-download");
        response.setContentLength((int) FileUtils.sizeOf(file));
        String downloadName = new String(fileName.getBytes("utf-8"),"iso-8859-1");
        response.setHeader("Content-Disposition","attachment;filename=\""+downloadName+"\"");
        OutputStream out = response.getOutputStream();
        out.write(FileUtils.readFileToByteArray(file));
        out.flush();
        out.close();
    }

    private static void setCellValue(HSSFRow row,int cellNum,String cellValue){
        HSSFCell cell = row.getCell(cellNum);
        cell.setCellValue(cellValue);
    }

//    public static void exportEnrollNamelist(List<EnrollNamelistVo> enrollNamelistVos,String filePath,HttpServletRequest request){
//        HSSFWorkbook workbook = new HSSFWorkbook();
//        HSSFCellStyle cellStyle = getCellStyle(workbook);
//        HSSFSheet sheet = workbook.getSheetAt(0);
//        //填充数据
//        int rownNum = sheet.getLastRowNum()+1;
//        Iterator<EnrollNamelistVo> iterator = enrollNamelistVos.iterator();
//        SimpleDateFormat formatter = new SimpleDateFormat("yyyy年MM月dd日 hh:mm:ss");
//        while (iterator.hasNext()){
//            EnrollNamelistVo enrollNamelistVo = iterator.next();
//            HSSFRow row = sheet.createRow(rownNum);
//            createNewCell(cellStyle,row,0,enrollNamelistVo.getName());
//            createNewCell(cellStyle,row,1,enrollNamelistVo.getMobile());
//            createNewCell(cellStyle,row,2,formatter.format(enrollNamelistVo.getGmtEnroll()));
//            createNewCell(cellStyle,row,3,getEnrollNamelistStatusValueByCode(enrollNamelistVo.getStatus()));
//            createNewCell(cellStyle,row,4,enrollNamelistVo.getEnrollProductName());
//            createNewCell(cellStyle,row,5,enrollNamelistVo.getMemo());
//            rownNum++;
//        }
//        FileOutputStream out = new FileOutputStream();
//
//    }

    private static String getEnrollNamelistStatusValueByCode(String code){
        for (EnrollNamelistStatus enrollNamelistStatus:EnrollNamelistStatus.values()){
            if(StringUtils.equals(enrollNamelistStatus.getCode(), code)){
                return enrollNamelistStatus.getDesc();
            }
        }
        return "";
    }

    private static void createNewCell(HSSFCellStyle cellStyle, HSSFRow row, int cellNum, String cellValue) {
        HSSFCell cell = row.createCell(cellNum);
        cell.setCellStyle(cellStyle);
        cell.setCellValue(cellValue);
    }

    private static HSSFCellStyle getCellStyle(HSSFWorkbook workbook){
        //字体
        HSSFFont font = workbook.createFont();
        font.setFontName("宋体");
        font.setFontHeightInPoints((short) 9);
        font.setBoldweight(HSSFFont.BOLDWEIGHT_NORMAL);
        HSSFCellStyle style = workbook.createCellStyle();
        //设置颜色
        style.setAlignment(HSSFCellStyle.ALIGN_CENTER);
        style.setVerticalAlignment(HSSFCellStyle.ALIGN_LEFT);
        style.setFillForegroundColor(HSSFColor.WHITE.index);
        style.setFillPattern(HSSFCellStyle.SOLID_FOREGROUND);
        //边框填充
        style.setBorderBottom(HSSFCellStyle.BORDER_THIN);
        style.setBorderLeft(HSSFCellStyle.BORDER_THIN);
        style.setBorderRight(HSSFCellStyle.BORDER_THIN);
        style.setBorderTop(HSSFCellStyle.BORDER_THIN);
        style.setFont(font);
        style.setWrapText(true);
        return style;
    }


}
