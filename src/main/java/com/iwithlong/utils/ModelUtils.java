package com.iwithlong.utils;

import com.imesne.assistant.common.runtime.RunBinder;
import com.iwithlong.context.ResultConst;
import org.springframework.ui.Model;

/**
 * Created by sys53 on 2017/6/27.
 */
public class ModelUtils {
    public static void processModel(Model model, String successMsg) {
        if (RunBinder.hasErrors()) {
            model.addAttribute(ResultConst.SUCCESS, false);
            model.addAttribute(ResultConst.MESSAGE, RunBinder.getStrErrors());
        } else {
            model.addAttribute(ResultConst.SUCCESS, true);
            model.addAttribute(ResultConst.MESSAGE, successMsg);
        }
    }

    public static void processModel(Model model, boolean success, String successMsg) {
        model.addAttribute(ResultConst.SUCCESS, success);
        model.addAttribute(ResultConst.MESSAGE, successMsg);
    }
}
