package com.iwithlong.utils;

import com.imesne.assistant.common.enums.IEnum;
import org.apache.commons.lang3.StringUtils;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by sys53 on 2017/7/1.
 */
public class EnumUtils {

    public String getDesc(IEnum[] enums, String code) {
        for (IEnum ie : enums) {
            if (StringUtils.equals(code, ie.getCode())) {
                return ie.getDesc();
            }
        }
        return code;
    }

    public static <E extends IEnum> Map<String, E> getEnumMap(final Class<E> enumClass) {
        final Map<String, E> map = new LinkedHashMap<String, E>();
        for (final E e : enumClass.getEnumConstants()) {
            map.put(e.getCode(), e);
        }
        return map;
    }

    public static <E extends IEnum> Map<String, String> getEnumStringMap(final Class<E> enumClass) {
        final Map<String, String> map = new LinkedHashMap<String, String>();
        for (final E e : enumClass.getEnumConstants()) {
            map.put(e.getCode(), e.getDesc());
        }
        return map;
    }
}
