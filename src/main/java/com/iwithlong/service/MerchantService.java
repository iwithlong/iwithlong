package com.iwithlong.service;

import com.imesne.assistant.common.page.PageList;
import com.iwithlong.vo.MerchantVo;

import java.util.List;

/**
 * Created by mhy on 2018/4/25.
 */
public interface MerchantService {
    /**
     * 列表
     * @param merchantVo
     * @return
     */
    PageList<MerchantVo> pageList(MerchantVo merchantVo);

    /**
     * 新增
     * @param merchantVo
     */
    void add(MerchantVo merchantVo);

    boolean existMerchantName(MerchantVo merchantVo);

    MerchantVo findById(Long memberId);

    void update(MerchantVo merchantVo);

    void deleteByIds(Long[] memberIds);

    boolean enable(Long memberId);

    List<MerchantVo> nameFilter(MerchantVo merchantVo);

    List<MerchantVo> nickNameFilter(MerchantVo merchantVo);


    /**
     * 商户登陆
     * @param memberCode
     * @param memberPassword
     * @return
     */
    MerchantVo logon(String memberCode, String memberPassword);

    void modifyPwd(String memberCode, String oldPassword, String newPassword);
}
