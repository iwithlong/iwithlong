package com.iwithlong.service;

import com.iwithlong.vo.ActivityAreaVo;
import com.iwithlong.vo.ActivityVo;

import java.util.List;

/**
 * Write class comments here
 * <p>
 * User: TJM
 * Date: 2017/10/13 14:17
 * version $Id: IndexService.java, v 0.1  14:17 Exp $
 */
public interface IndexService {

	/**
	 * 添加 活动 索引
	 * @param activityVo
	 */
	void addActivityIndex(ActivityVo activityVo);

	/**
	 * 添加 活动点 索引
	 * @param activityAreaVo
	 */
	void addActivityAreaIndex(ActivityAreaVo activityAreaVo);

	/**
	 * 更新 活动 索引
	 * @param activityVo
	 */
	void updateActivityIndex(ActivityVo activityVo);

	/**
	 * 更新 活动点 索引
	 * @param activityAreaVo
	 */
	void updateActivityAreaIndex(ActivityAreaVo activityAreaVo);

	/**
	 * 删除索引
	 * @param id
	 */
	void deleteIndex(String id,String indexType);

	/**
	 * 提交索引
	 */
	void commitIndex();

	/**
	 * 重构 索引
	 */
	void reconstructorIndex(List<ActivityVo> activityVoList,List<ActivityAreaVo> activityAreaVoList);


	ActivityAreaVo findActivityAreaById(Long id);


	ActivityVo     findActivityById(Long id);

}
