package com.iwithlong.service;

import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.List;

/**
 * User : liulu
 * Date : 2017/6/20 16:00
 * version $Id: SmsService.java, v 0.1 Exp $
 */
public interface SmsService {

    void validateCode(HttpSession session, String code);

    String getSessionCode(HttpSession session);

    String  sendVerificationCode ( String receiver);

    /**
     * 发送活动报名单短信
     * @param phoneNumbers
     * @param productSmsId
     */
    void sendActivityEnrollMessages(ArrayList<String> phoneNumbers, Long productSmsId);
}
