package com.iwithlong.service;

import com.iwithlong.entity.Member;
import com.iwithlong.vo.MemberVo;

import java.util.List;

/**
 * Write class comments here
 * <p>
 * User: TJM
 * Date: 2017/10/10 19:01
 * version $Id: MemberService.java, v 0.1  19:01 Exp $
 */
public interface MemberService {

	/**
	 * 删除会员
	 * @param memberId
	 */
	void delete(Long memberId);

	/**
	 * 启用会员
	 * @param memberId
	 */
	void enable(Long memberId);

	/**
	 * 禁用会员
	 *
	 * @param memberId
	 */
	void disable(Long memberId);


	/**
	 * 获取详情
	 *
	 * @param memberId
	 * @return
	 */
	Member get(Long memberId);

	/**
	 * 会员列表
	 *
	 * @param vo
	 * @return
	 */
	List<MemberVo> pageList(MemberVo vo);

	/**
	 * 修改密码
	 *
	 * @param loginMember 当前登录用户
	 * @param newPassword 新密码
	 * @param oldPassword 旧密码
	 */
	void modifyPassword(MemberVo loginMember, String newPassword, String oldPassword);

	/**
	 * 验证用户密码
	 *
	 * @param password
	 * @param loginMember
	 */
	void validatePassword(String password, MemberVo loginMember);

	/**
	 * 更新会员信息
	 * @param memberVo
	 */
	void updateMember(MemberVo memberVo);


	/**
	 * 会员注册
	 *
	 * @param vo
	 */
	MemberVo register(MemberVo vo);

	/**
	 * 登录
	 * @param weixin
	 * @return
	 */
	MemberVo login(String weixin);


	/**
	 * 获取用户信息
	 * @param weixin
	 * @return
	 */
	MemberVo findByWeixin(String weixin);
}
