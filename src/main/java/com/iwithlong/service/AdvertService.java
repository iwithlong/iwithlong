package com.iwithlong.service;

import com.imesne.assistant.common.page.PageList;
import com.iwithlong.entity.Advert;
import com.iwithlong.vo.AdvertVo;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * 广告service
 */
public interface AdvertService {

    /**
     * 查询广告列表
     *
     * @param advert
     * @return
     */
    PageList<AdvertVo> queryPageList(Advert advert);

    /**
     * 添加
     *
     * @param advert the advert
     * @param file   the file
     * @return long
     */
    Long add(Advert advert, MultipartFile file);


    PageList<AdvertVo> activityPageList(AdvertVo advertVo);

    /**
     * 发布
     * @param advertIds
     * @param status
     */
    void changeStatus(Long[] advertIds,String status);

    /**
     * 删除
     * @param advertIds
     */
    void delete(Long[] advertIds);

    /**
     * 更新
     * @param advertVo
     * @param file
     */
    void update(AdvertVo advertVo,MultipartFile file);

    AdvertVo findById(Long advertId);

    /**
     * 上移
     * @param advertId
     */
    void up(Long advertId);

    /**
     * 下移
     * @param advertId
     */
    void down(Long advertId);

    /**
     * 置顶
     * @param advertId
     */
    void top(Long advertId);

    /**
     * 置底
     * @param advertId
     */
    void bottom(Long advertId);
}
