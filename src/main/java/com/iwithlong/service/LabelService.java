package com.iwithlong.service;

import com.iwithlong.entity.Label;
import com.iwithlong.vo.LabelVo;

import java.util.List;

/**
 * 标签服务
 * <p>
 * User: TJM
 * Date: 2017/9/25 15:17
 * version $Id: LabelService.java, v 0.1  15:17 Exp $
 */
public interface LabelService {

	/**
	 * 标签列表
	 * @param labelVo
	 * @return
	 */
	List<Label> pageList(LabelVo labelVo);

	/**
	 * 删除标签
	 * @param labelId
	 */
	void deleteLabel(Long labelId);

	/**
	 * 添加标签
	 * @param label
	 */
	void addLabel(Label label);


	/**
	 * 编辑标签
	 * @param label
	 */
	void modifyLabel(Label label);

	/**
	 * 获取标签
	 * @param labelId
	 * @return
	 */
	Label getLabel(Long labelId);

	/**
	 * 更新热门标签
	 */
	void updateHotKeywords ();

	/**
	 * 更新 我的词库
	 */
	void updateMyWords ();

	/**
	 * 获取热门搜索
	 * @return
	 */
	List<Label> getHotKeywords();

	/**
	 * 根据类型获取标签列表
	 * @param type
	 * @return
	 */
	List<Label> getLabelByType(String type);

	List<Label> getActivityLabels();
}
