package com.iwithlong.service;


import com.imesne.assistant.common.page.PageList;
import com.iwithlong.entity.MemberGrowthAccount;
import com.iwithlong.entity.MemberGrowthChangeRecord;
import com.iwithlong.vo.MemberGrowthChangeRecordVo;

/**
 * 会员成长值服务层
 * <p>
 * User: TJM
 * Date: 2017/8/25 14:38
 * version $Id: MemberGrowthService.java, v 0.1  14:38 Exp $
 */
public interface MemberGrowthService {
	/**
	 * 获取当前会员等级与成长值
	 */
	MemberGrowthAccount getMemberGrowth (Long memberId);

	/**
	 * 获取当前会员的历史成长记录
	 */
	PageList<MemberGrowthChangeRecord> getMemberGrowthChangeList (MemberGrowthChangeRecordVo memberGrowthChangeRecordVo);


	/**
	 * 注册会员添加成长值
	 * @param memberId
	 */
	void registerMemberGrowth (Long memberId);

	/**
	 * 添加用户成长值
	 * @param memberId
	 * @param changeValue
	 * @param memo
	 */
	void addMemberGrowth (Long memberId, Integer changeValue, String memo);

}
