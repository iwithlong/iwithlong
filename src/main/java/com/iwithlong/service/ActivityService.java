package com.iwithlong.service;

import com.imesne.assistant.common.page.PageList;
import com.iwithlong.vo.ActivityVo;
import com.iwithlong.vo.SearchVo;

import java.util.List;
import java.util.Map;

/**
 * Created by xiexiaohan on 2017/10/10.
 */
public interface ActivityService {
    /**
     * 查询活动列表
     * @param activityVo
     * @return
     */
    PageList<ActivityVo> pageList(ActivityVo activityVo);

    /**
     * 活动详情
     * @param activityId
     * @return
     */
    ActivityVo findById(Long activityId);

    /**
     * 收藏/取消收藏
     * @param activityId
     * @param weixin
     * @param isInterest
     */
    Map<String,Object> interest(Long activityId, String weixin, boolean isInterest);

    /**
     * 新增活动
     * @param activityVo
     * @param uploadFileIds
     */
    void add(ActivityVo activityVo, String uploadFileIds);

    /**
     * 根据ID删除活动
     * @param activityId
     */
    void deleteByIds(Long[] activityId);

    /**
     * 启用
     * @param activityId
     */
    void enable(Long activityId);

    /**
     * 更新活动
     * @param activityVo
     */
    void update(ActivityVo activityVo,String uploadFileIds);

    void doAudit(ActivityVo activityVo);

    String findActivityFirstPicUrl(Long activityId);

    /**
     * 活动查询接口
     * @param searchVo
     * @return
     */
	List<ActivityVo> searchActivity (SearchVo searchVo);



    /**
     * 查找索引里的所有活动
     * @return
     */
	List<ActivityVo> findAllActivityInIndex();


//    /**
//     * 感兴趣列表
//     * @param searchVo
//     * @return
//     */
//    List<ActivityVo> interestList(SearchVo searchVo);

    /**
     * 查找所有已发布
     * @return
     */
    List<ActivityVo> findAllEnable();


    void setActivityUploadFiles(ActivityVo activityVo);

    ActivityVo mobileFindById(Long activityId, String weixin);

    boolean existActivityName(String name);

    /**
     * 给活动添加是否感兴趣
     * @param weixin
     */
    void dealActivityInterest(List<ActivityVo> activityVos,String weixin);

    void IssuerActivityArea(ActivityVo activityVo);

    List<ActivityVo> nameFilter(ActivityVo activityVo);
}
