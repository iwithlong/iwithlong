package com.iwithlong.service;

import com.iwithlong.entity.Attendance;

import java.util.List;

/**
 * Created by mhy on 2018/5/31.
 */
public interface AttendanceService {

    Attendance findByWeixin(String weixin);

    Double getAtendencePer(String weixin);
}
