package com.iwithlong.service.impl;

import com.imesne.assistant.common.bean.BeanKit;
import com.imesne.assistant.common.exception.ImesneException;
import com.imesne.assistant.common.utils.UUIDUtils;
import com.imesne.assistant.jdbc.command.entity.Select;
import com.imesne.assistant.jdbc.persist.JdbcDao;
import com.iwithlong.entity.UploadFile;
import com.iwithlong.service.UploadFileService;
import com.iwithlong.vo.UploadFileVo;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by xiexiaohan on 2017/10/12.
 */
@Service
public class UploadFileServiceImpl implements UploadFileService {
    @Value("${uploadFilePath:null}")
    private String uploadFilePath;

    @Resource
    JdbcDao jdbcDao;

    @Override
    public List<UploadFileVo> uploadFile(MultipartFile[] multipartFile) {
        List<UploadFileVo> result = new ArrayList<>();
        for (MultipartFile file : multipartFile) {
            UploadFile uploadFile = new UploadFile();
            uploadFile.setFileName(file.getOriginalFilename());
            uploadFile.setGmtUpload(new Date());
            uploadFile.setFileUrl(uploadAndGetUrl(file,true));
            Long uploadFileId = (Long) jdbcDao.insert(uploadFile);
            uploadFile.setUploadFileId(uploadFileId);
            result.add(BeanKit.convert(new UploadFileVo(),uploadFile));
        }

        return result;
    }

    @Override
    public void deleteById(String uploadFileId) {
        Select<UploadFile> uploadFileSelect = jdbcDao.createSelect(UploadFile.class).where("1",1);
        uploadFileSelect.and("uploadFileId",uploadFileId);
        UploadFile uploadFile = uploadFileSelect.singleResult();
        if(uploadFile==null){
            return;
        }else {
            jdbcDao.delete(uploadFile);
        }
    }

    @Override
    public void uEditorUploadFile(MultipartFile[] multipartFiles) {
        for (MultipartFile file : multipartFiles) {
            uploadAndGetUrl(file,false);
        }
    }

    private String uploadAndGetUrl(MultipartFile multipartFile,boolean needRenameFile) {
        if(StringUtils.equals(uploadFilePath,"null")){
            throw new ImesneException("文件上传路径未指定");
        }
        String fileName = multipartFile.getOriginalFilename();
        String finalPath;
        String picPath="";
        if(needRenameFile){
            String suffix = StringUtils.substring(fileName,StringUtils.lastIndexOf(fileName,'.'));
            picPath =  UUIDUtils.getUUID8()+suffix;
            finalPath = uploadFilePath + picPath;
        }else {
            finalPath = uploadFilePath +fileName;
        }

        try {
            FileUtils.copyInputStreamToFile(multipartFile.getInputStream(),new File(finalPath));
        }catch (IOException e){
            throw new ImesneException("文件保存出错");
        }
        return picPath;
    }


}
