package com.iwithlong.service.impl;


import com.imesne.assistant.common.page.PageList;
import com.imesne.assistant.common.validation.Verifier;
import com.imesne.assistant.jdbc.persist.JdbcDao;
import com.iwithlong.entity.Member;
import com.iwithlong.entity.MemberGrowthAccount;
import com.iwithlong.entity.MemberGrowthChangeRecord;
import com.iwithlong.enums.Rank;
import com.iwithlong.enums.RankGrowth;
import com.iwithlong.service.MemberGrowthService;
import com.iwithlong.utils.EnumUtils;
import com.iwithlong.vo.MemberGrowthChangeRecordVo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.Map;

/**
 * Write class comments here
 * <p>
 * User: TJM
 * Date: 2017/8/25 14:39
 * version $Id: MemberGrowthServiceImpl.java, v 0.1  14:39 Exp $
 */
@Service
public class MemberGrowthServiceImpl implements MemberGrowthService {


	@Resource
	private JdbcDao jdbcDao;

	@Override
	public MemberGrowthAccount getMemberGrowth (Long memberId) {

		Verifier.init().notNull(memberId, "id").validate();

		return jdbcDao.createSelect (MemberGrowthAccount.class)
				.where ("memberId", new Object[]{memberId}).singleResult ();
	}

	@Override
	public PageList<MemberGrowthChangeRecord> getMemberGrowthChangeList (MemberGrowthChangeRecordVo memberGrowthChangeRecordVo) {

		Verifier.init().notNull(memberGrowthChangeRecordVo.getMemberId (), "id").validate();

		//通过会员id查找成长值
		MemberGrowthAccount memberGrowthAccount = jdbcDao.createSelect (MemberGrowthAccount.class)
				.where ("memberId", new Object[]{memberGrowthChangeRecordVo.getMemberId ()}).singleResult ();

		//通过成长值ID找成长值记录
		return jdbcDao.createSelect (MemberGrowthChangeRecord.class)
				.where ("memberGrowthAccountId", new Object[]{memberGrowthAccount.getMemberGrowthAccountId ()})
				.pageList (memberGrowthChangeRecordVo);
	}

	@Override
	public void registerMemberGrowth (Long memberId) {

		MemberGrowthAccount memberGrowthAccount=new MemberGrowthAccount ();
		memberGrowthAccount.setMemberId (memberId);
		memberGrowthAccount.setBalance (0);
		jdbcDao.insert (memberGrowthAccount);

	}

	@Override
	public void addMemberGrowth (Long memberId, Integer changeValue,String memo) {

		//获取用户
		Member member = jdbcDao.createSelect (Member.class)
				.where ("memberId", new Object[]{memberId}).singleResult ();


		//获取用户成长值
		MemberGrowthAccount memberGrowthAccount = jdbcDao.createSelect (MemberGrowthAccount.class)
				.where ("memberId", new Object[]{memberId}).singleResult ();


		//计算积分的等级变化
		Integer afterChange=memberGrowthAccount.getBalance () + changeValue;
		String rank = getRank (afterChange,member.getRank ());


		if(!StringUtils.equals (rank,member.getRank ())){
			member.setRank (rank);
			jdbcDao.update (member);
		}

		//保存 改变后的用户成长值
		memberGrowthAccount.setBalance (afterChange);
		jdbcDao.update (memberGrowthAccount);


		//添加成长值变动记录
		MemberGrowthChangeRecord changeRecord=new MemberGrowthChangeRecord ();
		changeRecord.setMemberGrowthAccountId (memberGrowthAccount.getMemberGrowthAccountId ());
		changeRecord.setChangeValue (changeValue);
		changeRecord.setMemo (memo);
		changeRecord.setGmtRecord (new Date ());
		jdbcDao.insert (changeRecord);

	}




	private String getRank(Integer growth,String nowRank){


		Map<String, String> rankMap = EnumUtils.getEnumStringMap (RankGrowth.class);



		if(StringUtils.equals (nowRank, Rank.V1.getCode ())){
			Integer rankGrowthValue = Integer.valueOf (rankMap.get (Rank.V1.getCode ()));
			if(growth>rankGrowthValue){
				nowRank=Rank.V2.getCode ();
			}
		}

		if(StringUtils.equals (nowRank, Rank.V2.getCode ())){
			Integer rankGrowthValue =  Integer.valueOf(rankMap.get (Rank.V2.getCode ()));
			if(growth>rankGrowthValue){
				nowRank=Rank.V3.getCode ();
			}
		}


		if(StringUtils.equals (nowRank, Rank.V3.getCode ())){
			Integer rankGrowthValue = Integer.valueOf(rankMap.get (Rank.V3.getCode ()));
			if(growth>rankGrowthValue){
				nowRank=Rank.V4.getCode ();
			}
		}

		if(StringUtils.equals (nowRank, Rank.V4.getCode ())){
			Integer rankGrowthValue =  Integer.valueOf(rankMap.get (Rank.V4.getCode ()));
			if(growth>rankGrowthValue){
				nowRank=Rank.V5.getCode ();
			}
		}

		if(StringUtils.equals (nowRank, Rank.V5.getCode ())){
			nowRank=Rank.V5.getCode ();
		}

		return nowRank;
	}
}
