package com.iwithlong.service.impl;

import com.imesne.assistant.jdbc.command.entity.Select;
import com.imesne.assistant.jdbc.persist.JdbcDao;
import com.iwithlong.entity.Attendance;
import com.iwithlong.service.AttendanceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by mhy on 2018/5/31.
 */
@Service
@Transactional
public class AttendanceServiceImpl implements AttendanceService {
    @Autowired
    private JdbcDao jdbcDao;


    @Override
    public Attendance findByWeixin(String weixin) {
        Select<Attendance> attendanceSelect = jdbcDao.createSelect(Attendance.class).where("1",1);
        attendanceSelect.and("weixin",weixin);
        return attendanceSelect.singleResult();
    }

    @Override
    public Double getAtendencePer(String weixin) {
        Attendance attendance = findByWeixin(weixin);
        if(attendance==null){
            return 1.00;
        }
        return attendance.getAttendance();
    }
}
