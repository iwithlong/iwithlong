package com.iwithlong.service.impl;

import com.imesne.assistant.jdbc.persist.JdbcDao;
import com.iwithlong.entity.SearchKey;
import com.iwithlong.service.SearchKeyService;
import com.iwithlong.utils.IndexUtils;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * Write class comments here
 * <p>
 * User: TJM
 * Date: 2017/10/19 15:34
 * version $Id: SearchKeyServiceImpl.java, v 0.1  15:34 Exp $
 */
@Service
public class SearchKeyServiceImpl implements SearchKeyService {

	@Resource
	private JdbcDao jdbcDao;

	@Override
	public void strAddToSearchKey (String string) {
		List<String> words = IndexUtils.getWords (string);

		Date date=new Date ();
		for (String word : words) {
			SearchKey searchKey=new SearchKey ();
			searchKey.setKeyword (word);
			searchKey.setGmtSearch (date);
			jdbcDao.insert (searchKey);
		}
	}




}
