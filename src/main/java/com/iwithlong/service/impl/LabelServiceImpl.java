package com.iwithlong.service.impl;

import com.imesne.assistant.common.validation.Verifier;
import com.imesne.assistant.jdbc.command.entity.Select;
import com.imesne.assistant.jdbc.persist.JdbcDao;
import com.iwithlong.context.Constants;
import com.iwithlong.entity.Label;
import com.iwithlong.service.LabelService;
import com.iwithlong.utils.LuceneContext;
import com.iwithlong.utils.MyWordsUtils;
import com.iwithlong.vo.LabelVo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 标签服务实现
 * <p>
 * User: TJM
 * Date: 2017/9/25 15:17
 * version $Id: LabelServiceImpl.java, v 0.1  15:17 Exp $
 */
@Service
public class LabelServiceImpl implements LabelService {

	@Resource
	private JdbcDao jdbcDao;

	@Resource
	private JdbcTemplate jdbcTemplate;

	@Override
	public List<Label> pageList (LabelVo labelVo) {

		Select<Label> select = jdbcDao.createSelect(Label.class).where("1", 1);

		if (StringUtils.isNotEmpty (labelVo.getLabelType ())) {
			select.and ("labelType", new Object[]{labelVo.getLabelType ()});
		}

		if (StringUtils.isNotEmpty (labelVo.getKeyword ())){
			select.and ("labelName","like",new Object[]{labelVo.getKeyword ()});
		}

		select.orderBy("ord").asc ();
		return select.pageList(labelVo);
	}

	@Override
	public void deleteLabel (Long labelId) {

		Label label=new Label ();
		label.setLabelId (labelId);
		jdbcDao.delete (label);

	}

	@Override
	public void addLabel (Label label) {
		Verifier.init()
				.notBlank(label.getLabelName (), "标签名")
				.notNull(label.getLabelType (),"标签类型")
				.validate();

		jdbcDao.insert (label);


	}

	@Override
	public void modifyLabel (Label label) {
		Verifier.init()
				.notBlank(label.getLabelName (), "标签名")
				.notNull(label.getLabelType (),"标签类型")
				.notNull (label.getLabelId (),"标签ID")
				.validate();

		jdbcDao.update (label);


	}

	@Override
	public Label getLabel (Long labelId) {
		Verifier.init().notNull(labelId, "id").validate();
		return jdbcDao.get(Label.class, labelId);
	}

	@Override
	public void updateHotKeywords () {


		jdbcDao.createDelete (Label.class).where("labelType", new Object[]{Constants.HOT_KEYWORDS}).execute();

		List<Map<String, Object>> maps = jdbcTemplate.queryForList (
				"SELECT keyword, COUNT(keyword) as num  " +
						"FROM iwl_search_key GROUP BY keyword ORDER BY num desc limit 10");


		for (int i = 0; i < maps.size (); i++) {
			Label label=new Label ();

			label.setLabelName ((String) maps.get (i).get ("keyword"));
			label.setLabelType (Constants.HOT_KEYWORDS);
			label.setOrd (i+1);

			addLabel (label);

		}


	}

	@Override
	public void updateMyWords () {
		List<Label> list = jdbcDao.createSelect (Label.class).where ("1", 1).list ();

		List<String > myWords=new ArrayList<> ();


		for (Label label : list) {
			myWords.add (label.getLabelName ());
		}

		MyWordsUtils.clearMyWords ();
		MyWordsUtils.infoMyWords (myWords);
		LuceneContext.reloadWords ();

	}

	@Override
	public List<Label> getHotKeywords () {

		Select<Label> select = jdbcDao.createSelect(Label.class).where("1", 1);

		select.and ("labelType", new Object[]{Constants.HOT_KEYWORDS});
		select.orderBy("ord").asc ();

		return select.list ();
	}

	@Override
	public List<Label> getLabelByType (String labelType) {
		Select<Label> select = jdbcDao.createSelect(Label.class).where("1", 1);
		select.and ("labelType", new Object[]{labelType});
		select.orderBy("ord").asc ();

		return select.list ();
	}

	@Override
	public List<Label> getActivityLabels() {
		List<Label> result = jdbcTemplate.query(
				"SELECT LABEL_ID,LABEL_NAME ,LABEL_TYPE,ORD FROM iwl_label WHERE LABEL_TYPE = 'activity_type' or LABEL_TYPE = 'fast_keywords' ORDER BY LABEL_TYPE,ORD ASC",new BeanPropertyRowMapper<>(Label.class));
		return result;
	}
}
