package com.iwithlong.service;

import com.alibaba.fastjson.JSON;
import org.json.JSONObject;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by xiexiaohan on 2017/12/12.
 */
public interface WeixinService {

    Object requestAccessToken(String grant_type, String appid, int offset,int count);

    String requestWeixinPublic(String url);

    Object requestWeixinJsonObject(String url);

    Object requestMapData(String appid, String js_code, String grant_type);

    void dealWithAppRequest(HttpServletRequest request, HttpServletResponse response);
}
