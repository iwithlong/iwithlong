package com.iwithlong.service;


import com.iwithlong.entity.OperateLog;
import com.iwithlong.vo.OperateLogVo;

import java.util.List;

/**
 * User : liulu
 * Date : 17-9-9 下午5:57
 * version $Id: OperateLogService.java, v 0.1 Exp $
 */
public interface OperateLogService {

    /**
     * 日志列表
     *
     * @param logVo
     * @return
     */
    List<OperateLog> pageList(OperateLogVo logVo);

    /**
     * 获取日志详情
     *
     * @param operateLogId
     * @return
     */
    OperateLog get(Long operateLogId);

    /**
     * 记录日志
     *
     * @param log
     */
    void log(OperateLog log);
}
