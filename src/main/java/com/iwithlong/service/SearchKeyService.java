package com.iwithlong.service;

/**
 * 查询关键字
 * <p>
 * User: TJM
 * Date: 2017/10/19 15:33
 * version $Id: SearchKeyService.java, v 0.1  15:33 Exp $
 */
public interface SearchKeyService {

	/**
	 * 搜索内容添加到 查询关键字中（会自动拆分）
	 * @param string
	 */
	void strAddToSearchKey(String string);

}
