package com.iwithlong.service;

import com.imesne.assistant.common.page.PageList;
import com.iwithlong.vo.ActivityAreaVo;
import com.iwithlong.vo.SearchVo;

import java.util.List;

/**
 * Created by xiexiaohan on 2017/10/10.
 */
public interface ActivityAreaService {
    /**
     * 活动点列表
     * @param activityAreaVo
     * @return
     */
    PageList<ActivityAreaVo> pageList(ActivityAreaVo activityAreaVo);

    /**
     * 按id删除活动点
     * @param activityAreaIds
     */
    void deleteByIds(Long[] activityAreaIds);

    /**
     * 更新活动点
     * @param activityArea
     * @param uploadFileIds 图片地址
     */
    void update(ActivityAreaVo activityArea,String uploadFileIds);

    /**
     * 新增活动点
     * @param activityArea
     * @param uploadFileIds 图片地址
     */
    void add(ActivityAreaVo activityArea,String uploadFileIds);

    /**
     * 根据id获取活动点
     * @param activityAreaId
     * @return
     */
    ActivityAreaVo findById(long activityAreaId);

    /**
     * 移动端查询列表
     * @param searchVo
     * @return
     */
    List<ActivityAreaVo> mobilePageList(SearchVo searchVo);

    /**
     * 移动端根据id查询
     * @param activityAreaId
     * @return
     */
    ActivityAreaVo mobileFindById(Long activityAreaId,String weixin);

    /**
     * 启用／禁用活动点
     * @param activityAreaId
     */
    void enableActivityArea(long activityAreaId);

    void audit(ActivityAreaVo activityAreaVo);


    /**
     * 通过坐标点查询活动点
     * @param searchVo
     * @return
     */
    List<ActivityAreaVo> findActivityAreaByCorner(SearchVo searchVo);

    /**
     * 通过坐标点查询活动点,包含活动列表
     * @param searchVo
     * @return
     */
    List<ActivityAreaVo> findActivityAreaAndActivityByCorner(SearchVo searchVo);

    /**
     * 查找所有已发布活动点
     * @return
     */
    List<ActivityAreaVo> findAllEnable();

    /**
     * 过滤活动点
     * @param activityAreaVo
     * @return
     */
    List<ActivityAreaVo> filter(ActivityAreaVo activityAreaVo);

    /**
     * 验证活动点名称是否存在
     * @param areaName
     * @return
     */
    boolean existAreaName(String areaName);

    /**
     * 发布活动点
     * @param activityAreaVo
     */
    void IssuerActivityArea(ActivityAreaVo activityAreaVo);
}
