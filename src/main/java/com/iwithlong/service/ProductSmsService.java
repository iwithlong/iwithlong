package com.iwithlong.service;

import com.imesne.assistant.common.page.PageList;
import com.iwithlong.vo.ProductSmsVo;

import java.util.Map;

/**
 * Created by mhy on 2018/5/31.
 */
public interface ProductSmsService {
    /**
     * 按产品id发送短信
     * @param productId
     * @return
     */
    ProductSmsVo sendMsgByProduct(Long productId);

    /**
     * 新增短信
     * @param productSmsVo
     */
    void addProductSms(ProductSmsVo productSmsVo);

    /**
     * 根据报名单用户发送短信
     * @param enrollNamelistId
     * @return
     */
    ProductSmsVo sendMsgByEnrollNamelistIds(Long[] enrollNamelistId);

    /**
     * 分页查询
     * @param productSmsVo
     * @return
     */
    PageList<ProductSmsVo> pageList(ProductSmsVo productSmsVo);

    /**
     * 根据id查询
     * @param productSmsId
     * @return
     */
    ProductSmsVo findById(Long productSmsId);

    void pass(ProductSmsVo productSmsVo);

    void notPass(ProductSmsVo productSmsVo);
}
