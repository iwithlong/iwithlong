package com.iwithlong.service;

import com.iwithlong.vo.UploadFileVo;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * Created by xiexiaohan on 2017/10/12.
 */
public interface UploadFileService {
    List<UploadFileVo> uploadFile(MultipartFile[] multipartFile);

    void deleteById(String uploadFileId);

    void uEditorUploadFile(MultipartFile[] multipartFiles);
}
