package com.iwithlong.service;

import com.imesne.assistant.common.page.PageList;
import com.iwithlong.vo.ActivityEnrollVo;
import com.iwithlong.vo.ActivityVo;
import com.iwithlong.vo.EnrollNamelistVo;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * Created by mhy on 2018/5/1.
 */
public interface ActivityEnrollService {
    /**
     * 查询列表
     * @param activityEnrollVo
     * @return
     */
    PageList<ActivityEnrollVo> pageList(ActivityEnrollVo activityEnrollVo);

    /**
     * 新增
     * @param activityEnrollVo
     */
    void add(ActivityEnrollVo activityEnrollVo);

    /**
     * 判断商家是否存在
     * @param activityEnrollVo
     * @return
     */
    boolean validateMember(ActivityEnrollVo activityEnrollVo);

    boolean validateActivity(ActivityEnrollVo activityEnrollVo);

    void update(ActivityEnrollVo activityEnrollVo);

    ActivityEnrollVo findById(Long activityEnrollId);

    List<ActivityEnrollVo> findByActivityId(Long activityId);

    /**
     * 启动
     * @param activityEnrollId
     */
    void enable(Long activityEnrollId);

    void deleteByIds(Long[] activityEnrollIds);

    /**
     * 新增分组vo返回页面
     * @param activityEnrollVo
     */
    void addEnrollProductVo(ActivityEnrollVo activityEnrollVo);

    /**
     * 查询报名名单列表
     * @param enrollNamelistVo
     * @param activityEnrollVo
     * @return
     */
    PageList<EnrollNamelistVo> namePageList(EnrollNamelistVo enrollNamelistVo, ActivityEnrollVo activityEnrollVo);

    /**
     * 报名
     * @param enrollNamelistVo
     */
    void enroll(EnrollNamelistVo enrollNamelistVo);

    /**
     * 我的报名单列表
     * @param enrollNamelistVo
     * @return
     */
    PageList<EnrollNamelistVo> myEnrollPageList(EnrollNamelistVo enrollNamelistVo);

    /**
     * 更新活动报名单状态
     */
    int updateActivityEnrollStatus();

    /**
     * 报名签到
     * @param enrollNamelistIds
     */
    void sign(Long[] enrollNamelistIds);

    /**
     * 报名取消并退款
     * @param enrollNamelistIds
     */
    void cancel(Long[] enrollNamelistIds);

    /**
     * 报名名单导出
     * @param enrollNamelistVo
     * @param response
     */
    void exportExcel(List<EnrollNamelistVo> enrollNamelistVo, HttpServletResponse response);

    /**
     * 完成报名单
     * @param activityEnrollIds
     */
    void complete(Long[] activityEnrollIds);


    EnrollNamelistVo getEnrollNamelistDetailById(Long enrollNamelistId);

    /**
     * 手机端活动报名列表接口
     * @param activityEnrollVo
     * @return
     */
    PageList<ActivityVo> mobilePageListActivityVo(ActivityEnrollVo activityEnrollVo);

    /**
     * 名称过滤
     * @param activityEnrollVo
     * @return
     */
    PageList<ActivityEnrollVo> nameFilter(ActivityEnrollVo activityEnrollVo);
}
