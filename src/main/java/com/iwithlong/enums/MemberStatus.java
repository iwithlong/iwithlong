package com.iwithlong.enums;

import com.imesne.assistant.common.enums.IEnum;
import lombok.Getter;

/**
 * 会员状态
 */
@Getter
public enum MemberStatus implements IEnum {


    ENABLED("01", "启用"),

    DISABLED("02", "禁用"),;

    MemberStatus (String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    private String code;

    private String desc;


}
