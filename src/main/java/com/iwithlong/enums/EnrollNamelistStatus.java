package com.iwithlong.enums;

import lombok.Getter;

/**
 * Created by mhy on 2018/5/15.
 */
@Getter
public enum EnrollNamelistStatus {
    ENROLLED("enrolled", "已报名"),
    SIGNED("signed","已签到"),
    UNCOMMITTED("uncommitted","未参加"),
    CANCELLED("cancelled","已取消");



    EnrollNamelistStatus (String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    private String code;

    private String desc;
}
