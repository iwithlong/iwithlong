package com.iwithlong.enums;

import lombok.Getter;

/**
 * Created by mhy on 2018/4/28.
 */
@Getter
public enum  MemberType {
    MEMBER("member","普通会员"),
    MERCHANT("merchant","商户");



    MemberType (String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    private String code;

    private String desc;
}
