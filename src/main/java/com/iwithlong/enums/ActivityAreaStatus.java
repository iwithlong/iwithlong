package com.iwithlong.enums;

import lombok.Getter;

/**
 * Created by xiexiaohan on 2017/10/11.
 */
@Getter
public enum ActivityAreaStatus {

    SAVED("1", "草稿"),

    NEEDREVIEW("2", "待审核"),

    REVIEWREFUSED("3","审核未通过"),

    REVIEWED("4","审核通过"),

    ISSUERED("5","已发布");



    ActivityAreaStatus (String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    private String code;

    private String desc;
}
