package com.iwithlong.enums;

import com.imesne.assistant.common.enums.IEnum;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by xiexiaohan on 2017/10/10.
 * 业务上传文件类型
 */
@Getter
public enum BizUploadFileType implements IEnum {
    ACTIVITYAREA("ActivityArea","活动点"),
    ACTIVITY("Activity","活动");

    private String code;
    private String desc;

    BizUploadFileType(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }
}
