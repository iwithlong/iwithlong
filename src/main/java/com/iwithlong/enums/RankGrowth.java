package com.iwithlong.enums;

import com.imesne.assistant.common.enums.IEnum;
import lombok.Getter;

/**
 * 等级所需成长值枚举
 * <p>
 * User: TJM
 * Date: 2017/10/10 18:35
 * version $Id: RankGrowth.java, v 0.1  18:35 Exp $
 */
@Getter
public enum RankGrowth implements IEnum {
	V1("01","0"),
	V2("02","1000"),
	V3("03","2000"),
	V4("04","5000"),
	V5("05","10000")
	;

	RankGrowth (String code, String desc) {
		this.code = code;
		this.desc = desc;
	}

	private String code;

	private String desc;
}
