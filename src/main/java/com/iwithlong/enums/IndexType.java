package com.iwithlong.enums;

import com.imesne.assistant.common.enums.IEnum;
import lombok.Getter;

/**
 * Write class comments here
 * <p>
 * User: TJM
 * Date: 2017/10/16 18:52
 * version $Id: IndexType.java, v 0.1  18:52 Exp $
 */
@Getter
public enum IndexType  implements IEnum {
	ACTIVITY("Activity","活动"),
	ACTIVITY_AREA("ActivityArea","活动点"),
	;

	IndexType (String code, String desc) {
		this.code = code;
		this.desc = desc;
	}

	private String code;

	private String desc;
}
