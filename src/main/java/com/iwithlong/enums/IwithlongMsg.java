package com.iwithlong.enums;

import com.imesne.assistant.common.enums.IEnum;
import lombok.Getter;

/**
 * Write class comments here
 * <p>
 * User: TJM
 * Date: 2017/10/24 19:11
 * version $Id: IwithlongMsg.java, v 0.1  19:11 Exp $
 */
@Getter
public enum IwithlongMsg implements IEnum {


	VERIFICATION_CODE_ERROR("VERIFICATION_CODE_ERROR", "短信验证码不正确"),
	;

	IwithlongMsg (String code, String desc) {
		this.code = code;
		this.desc = desc;
	}

	private String code;

	private String desc;

}
