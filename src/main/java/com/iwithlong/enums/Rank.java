package com.iwithlong.enums;

import com.imesne.assistant.common.enums.IEnum;
import lombok.Getter;

/**
 * 等级枚举
 */
@Getter
public enum Rank implements IEnum {
    V1("01","等级1"),
	V2("02","等级2"),
	V3("03","等级3"),
	V4("04","等级4"),
	V5("05","等级5")
    ;

    Rank (String code, String desc) {
		this.code = code;
		this.desc = desc;
    }

    private String code;

    private String desc;
}
