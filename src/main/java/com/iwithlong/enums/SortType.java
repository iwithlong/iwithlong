package com.iwithlong.enums;

import com.imesne.assistant.common.enums.IEnum;
import lombok.Getter;

/**
 * Write class comments here
 * <p>
 * User: TJM
 * Date: 2017/10/26 15:20
 * version $Id: SortType.java, v 0.1  15:20 Exp $
 */
@Getter
public enum SortType implements IEnum {

	WEIGHT_SORT("weightSort","权重排序"),
	INTEREST_SORT("interestSort","人气优先"),
	DISTANCE_SORT("distanceSort","距离优先"),
	;

	SortType (String code, String desc) {
		this.code = code;
		this.desc = desc;
	}

	private String code;

	private String desc;
}

