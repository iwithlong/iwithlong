package com.iwithlong.enums;

import com.imesne.assistant.common.enums.IEnum;
import lombok.Getter;

/**
 * Created by mhy on 2018/5/7.
 */
@Getter
public enum ActivityEnrollStatus implements IEnum {
    UNABLE("unable","未启用"),
    ENROLLMENT("enrollment","报名中"),
    STOPPED("stopped","已截止"),
    COMPLETE("complete","已结束");

    private String code;
    private String desc;

    ActivityEnrollStatus(String code, String desc) {
        this.code = code;
        this.desc = desc;
    }
}
