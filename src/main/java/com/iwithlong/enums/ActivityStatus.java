package com.iwithlong.enums;

import lombok.Getter;

/**
 * Created by xiexiaohan on 2017/10/11.
 */
@Getter
public enum ActivityStatus {
    SAVED("1", "草稿"),

    NEEDREVIEW("2", "待审核"),

    REVIEWREFUSED("3","审核未通过"),

    REVIEWED("4","审核通过／发布"),

    ISSUERED("5","已发布");



    ActivityStatus (String code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    private String code;

    private String desc;
}
