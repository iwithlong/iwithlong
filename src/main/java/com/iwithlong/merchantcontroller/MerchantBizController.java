package com.iwithlong.merchantcontroller;

import com.iwithlong.annotation.MerchantSession;
import com.iwithlong.context.Constants;
import com.iwithlong.context.ResultConst;
import com.iwithlong.service.ActivityEnrollService;
import com.iwithlong.service.ProductSmsService;
import com.iwithlong.vo.ActivityEnrollVo;
import com.iwithlong.vo.MerchantVo;
import com.iwithlong.vo.ProductSmsVo;
import com.ktanx.platform.admin.controller.AdminBaseController;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;

/**
 * Created by mhy on 2018/6/11.
 */
@Controller
@RequestMapping("/merchant")
@MerchantSession
public class MerchantBizController extends AdminBaseController {
    @Resource
    private ProductSmsService productSmsService;

    @Resource
    private ActivityEnrollService activityEnrollService;

    /**
     * 短信记录
     * @param productSmsVo
     * @param request
     * @param modelMap
     * @return
     */
    @RequestMapping("sms/record")
    public String smsRecord(ProductSmsVo productSmsVo,HttpServletRequest request,ModelMap modelMap){
        productSmsVo.setMemberCodeEqual(getSessionMerchantVo(request).getMemberCode());
        modelMap.addAttribute("productSmsVo", productSmsVo);
        modelMap.addAttribute(ResultConst.RESULT,productSmsService.pageList(productSmsVo));
        return "merchantH5/sms-record";
    }

    /**
     * 所有报名单
     * @param activityEnrollVo
     * @param request
     * @param modelMap
     * @return
     */
    @RequestMapping("activityenroll/all")
    public String allActivityEnroll(ActivityEnrollVo activityEnrollVo,HttpServletRequest request,ModelMap modelMap){
        modelMap.addAttribute("activityEnrollVo",activityEnrollVo);
        activityEnrollVo.setMemberId(getSessionMerchantVo(request).getMemberId());
        modelMap.addAttribute(ResultConst.RESULT,activityEnrollService.pageList(activityEnrollVo));
        return "merchantH5/activityenroll-all";
    }


    private MerchantVo getSessionMerchantVo(HttpServletRequest request){
        return (MerchantVo)request.getSession().getAttribute(Constants.LOGIN_MERCHANT);
    }
}
