//package com.iwithlong.interceptor;
//
//import com.iwithlong.annotation.MerchantSession;
//import com.iwithlong.context.Constants;
//import com.iwithlong.vo.MerchantVo;
//import org.apache.commons.lang3.StringUtils;
//import org.springframework.web.method.HandlerMethod;
//import org.springframework.web.servlet.HandlerInterceptor;
//import org.springframework.web.servlet.ModelAndView;
//
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//
///**
// * Created by mhy on 2018/6/7.
// */
//
//public class MerchantInterceptor implements HandlerInterceptor {
//    @Override
//    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
//        if (!(handler instanceof HandlerMethod)) {
//            return true;
//        }
//
//        HandlerMethod method = (HandlerMethod) handler;
//        Class<?> handlerClass = method.getBean().getClass();
//
//        MerchantSession memberSession = handlerClass.getAnnotation(MerchantSession.class);
//        if (memberSession == null) {
//            memberSession = method.getMethodAnnotation(MerchantSession.class);
//        }
//
//        if (memberSession == null || !memberSession.value()) {
//            return true;
//        }
//
//        MerchantVo member = (MerchantVo) request.getSession().getAttribute(Constants.LOGIN_MERCHANT);
//        if (member == null) {
//            String    requestURI  = request.getRequestURI ();
//            String    contextPath = request.getContextPath ();
//            if ( StringUtils.length(contextPath) > 0 ) {
//                requestURI = StringUtils.substring ( requestURI, contextPath.length () );
//            }
//            if(StringUtils.endsWith ( requestURI, ".json" )){
//                response.getWriter().println("{\"success\":false,\"message\":\"未登陆或登陆超时!\"}");
//            }else {
//                response.sendRedirect(request.getContextPath()+"/merchant/login");
//            }
//            return false;
//        }
//
//        return true;
//    }
//
//    @Override
//    public void postHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, ModelAndView modelAndView) throws Exception {
//
//    }
//
//    @Override
//    public void afterCompletion(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o, Exception e) throws Exception {
//
//    }
//}
