package com.iwithlong.controller;

import com.imesne.assistant.common.runtime.RunBinder;
import com.iwithlong.service.ActivityAreaService;
import com.iwithlong.service.ActivityService;
import com.iwithlong.service.IndexService;
import com.iwithlong.vo.ActivityAreaVo;
import com.iwithlong.vo.ActivityVo;
import com.ktanx.platform.admin.controller.AdminBaseController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributesModelMap;

import javax.annotation.Resource;
import java.util.List;

/**
 * Created by xiexiaohan on 2017/10/31.
 */
@Controller
public class InitIndexController extends AdminBaseController {
    @Resource
    IndexService indexService;

    @Resource
    ActivityAreaService activityAreaService;

    @Resource
    ActivityService activityService;

    @RequestMapping("a/init/index")
    public String initIndex(RedirectAttributesModelMap redirectAttributesModelMap){
        List<ActivityAreaVo> activityAreaVos = activityAreaService.findAllEnable();
        List<ActivityVo> activityVos = activityService.findAllEnable();
        indexService.reconstructorIndex(activityVos,activityAreaVos);
        if(RunBinder.hasErrors()){
            this.addErrorMsg(redirectAttributesModelMap,RunBinder.getStrErrors());
        }else {
            this.addSuccessMsg(redirectAttributesModelMap,"初始化成功");
        }
        return "redirect:/b/activityarea/list";
    }
}
