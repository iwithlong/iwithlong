package com.iwithlong.controller;

import com.imesne.assistant.common.page.PageList;
import com.imesne.assistant.common.runtime.RunBinder;
import com.iwithlong.context.ResultConst;
import com.iwithlong.service.MerchantService;
import com.iwithlong.vo.MerchantVo;
import com.ktanx.platform.admin.controller.AdminBaseController;
import com.ktanx.platform.web.Page;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributesModelMap;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

/**
 * Created by mhy on 2018/4/25.
 * 商户控制层
 */
@Controller
public class MerchantController extends AdminBaseController {
    @Resource
    private MerchantService merchantService;

    /**
     * 商户列表
     *
     * @param merchantVo
     * @param model
     * @return
     */
    @RequestMapping({"b/merchant/list"})
    public String merchantList(MerchantVo merchantVo,boolean getSession,HttpServletRequest request, Model model) {
        HttpSession session = request.getSession();
        if(getSession){
            merchantVo = (MerchantVo)session.getAttribute("memberSearchVo");
        }else {
            session.setAttribute("memberSearchVo",merchantVo);
        }
        model.addAttribute("merchantVo", merchantVo);
        PageList<MerchantVo> result = merchantService.pageList(merchantVo);
        model.addAttribute(ResultConst.RESULT, result);
        model.addAttribute("page", Page.wrap(result));
        return "merchant/merchant-list";
    }

    /**
     * 商户新增页面
     *
     * @return
     */
    @RequestMapping({"b/merchant/add"})
    public String addView() {
        return "merchant/merchant-add";
    }

    /**
     * 商户新增后台
     * @param merchantVo
     * @param modelMap
     * @param redirectAttributesModelMap
     * @return
     */
    @RequestMapping(value = {"b/merchant/add"}, method = {RequestMethod.POST})
    public String merchantAdd(MerchantVo merchantVo, ModelMap modelMap,RedirectAttributesModelMap redirectAttributesModelMap) {
        merchantService.add(merchantVo);
        if(RunBinder.hasErrors()){
            this.addErrorMsg(modelMap,RunBinder.getStrErrors());
            return "merchant/merchant-add";
        }else {
            this.addSuccessMsg(redirectAttributesModelMap,"新增商户成功");
            redirectAttributesModelMap.addAttribute("getSession",true);
            return "redirect:list";
        }
    }

    /**
     * 验证商户名重复
     * @param merchantVo
     * @param model
     * @return
     */
    @RequestMapping(value = {"b/merchant/existmerchantname.json"})
    public String existMerchantName(MerchantVo merchantVo,Model model){
        model.addAttribute("valid", !merchantService.existMerchantName(merchantVo));
        return "";
    }

    /**
     * 更新商家页面
     * @param memberId
     * @param model
     * @return
     */
    @RequestMapping("b/merchant/update")
    public String update(Long memberId,Model model){
        model.addAttribute("merchantVo",merchantService.findById(memberId));
        return "merchant/merchant-update";
    }

    /**
     * 更新活动
     * @param merchantVo
     * @param modelMap
     * @param redirectAttributesModelMap
     * @return
     */
    @RequestMapping(value = {"b/merchant/update"},method = {RequestMethod.POST})
    public String update(MerchantVo merchantVo,ModelMap modelMap,RedirectAttributesModelMap redirectAttributesModelMap){
        merchantService.update(merchantVo);
        if(RunBinder.hasErrors()){
            this.addErrorMsg(modelMap,RunBinder.getStrErrors());
            return "merchant/merchant-update";
        }else {
            this.addSuccessMsg(redirectAttributesModelMap,"修改成功");
            redirectAttributesModelMap.addAttribute("getSession",true);
            return "redirect:list";
        }
    }

    /**
     * 删除活动
     * @param memberId
     * @param redirectAttributesModelMap
     * @return
     */
    @RequestMapping("b/merchant/delete")
    public String delete(Long[] memberId,RedirectAttributesModelMap redirectAttributesModelMap){
        merchantService.deleteByIds(memberId);
        if(RunBinder.hasErrors()){
            this.addErrorMsg(redirectAttributesModelMap, RunBinder.getStrErrors());
        }else {
            this.addSuccessMsg(redirectAttributesModelMap, "删除成功");
        }
        redirectAttributesModelMap.addAttribute("getSession",true);
        return "redirect:list";
    }

    /**
     *启用／禁用商家
     * @param memberId
     * @param redirectAttributesModelMap
     * @return
     */
    @RequestMapping("b/merchant/enable")
    public String enable(Long memberId,RedirectAttributesModelMap redirectAttributesModelMap){
        boolean action = merchantService.enable(memberId);
        if(RunBinder.hasErrors()){
            this.addErrorMsg(redirectAttributesModelMap, RunBinder.getStrErrors());
        }else {
            this.addSuccessMsg(redirectAttributesModelMap, (action?"启用":"禁用")+"成功");
        }
        redirectAttributesModelMap.addAttribute("getSession",true);
        return "redirect:list";
    }

    /**
     * 商家详情
     * @param memberId
     * @param model
     * @return
     */
    @RequestMapping("b/merchant/detail")
    public String detail(Long memberId,Model model){
        MerchantVo merchantVo = merchantService.findById(memberId);
        model.addAttribute("merchantVo",merchantVo);
        return "merchant/merchant-detail";
    }

    @RequestMapping("b/merchant/namefilter.json")
    public String memberNameFilter(MerchantVo merchantVo,Model model){
        List<MerchantVo> merchantVos = merchantService.nameFilter(merchantVo);
        model.addAttribute(ResultConst.RESULT,merchantVos);
        return "";
    }

    @RequestMapping("b/merchant/nicknamefilter.json")
    public String nickNameFilter(MerchantVo merchantVo,Model model){
        List<MerchantVo> merchantVos = merchantService.nickNameFilter(merchantVo);
        model.addAttribute(ResultConst.RESULT,merchantVos);
        return "";
    }


}
