package com.iwithlong.controller;

import com.imesne.assistant.common.page.PageList;
import com.imesne.assistant.common.runtime.RunBinder;
import com.iwithlong.context.ResultConst;
import com.iwithlong.enums.ActivityAreaStatus;
import com.iwithlong.service.ActivityAreaService;
import com.iwithlong.utils.ReadAreaUtils;
import com.iwithlong.vo.ActivityAreaVo;
import com.iwithlong.vo.SearchVo;
import com.ktanx.platform.admin.controller.AdminBaseController;
import com.ktanx.platform.setting.kit.SettingKit;
import com.ktanx.platform.utils.HttpUtils;
import com.ktanx.platform.web.LoginUser;
import com.ktanx.platform.web.Page;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributesModelMap;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * Created by xiexiaohan on 2017/10/10.
 * 活动点控制层
 */
@Controller
public class ActivityAreaController extends AdminBaseController {

    @Resource
    ActivityAreaService activityAreaService;

    /**
     * 活动点列表
     * @param activityAreaVo
     * @param model
     * @return
     */
    @RequestMapping({"b/activityarea/list"})
    public String list(ActivityAreaVo activityAreaVo, Model model) {
        PageList<ActivityAreaVo> activityAreaVos = activityAreaService.pageList(activityAreaVo);
        model.addAttribute(ResultConst.RESULT,activityAreaVos);
        model.addAttribute("page", Page.wrap(activityAreaVos));
        return "activityarea/activity-area-list";
    }

    /**
     * 活动点过滤
     * @param activityAreaVo
     * @param model
     * @return
     */
    @RequestMapping("b/activityarea/filter.json")
    public String filterActivityArea(ActivityAreaVo activityAreaVo,Model model){
        List<ActivityAreaVo> activityAreaVos = activityAreaService.filter(activityAreaVo);
        model.addAttribute(ResultConst.RESULT,activityAreaVos);
        return "";
    }

    /**
     * 活动点审核列表
     * @param activityAreaVo
     * @param model
     * @return
     */
    @RequestMapping({"a/activityarea/audit/list"})
    public String auditList(ActivityAreaVo activityAreaVo,Model model){
        activityAreaVo.setStatus(ActivityAreaStatus.NEEDREVIEW.getCode());
        PageList<ActivityAreaVo> activityAreaVos = activityAreaService.pageList(activityAreaVo);
        model.addAttribute(ResultConst.RESULT,activityAreaVos);
        model.addAttribute("page", Page.wrap(activityAreaVos));
        return "activityarea/activity-area-audit-list";
    }

    /**
     * 活动点审核详情
     * @param activityAreaId
     * @param model
     * @return
     */
    @RequestMapping({"a/activityarea/audit/detail"})
    public String auditDetail(Long activityAreaId,Model model){
        model.addAttribute("activityAreaVo",activityAreaService.findById(activityAreaId));
        model.addAttribute("areaMap", ReadAreaUtils.getAreaData());
        model.addAttribute("needAudit",StringUtils.equals(SettingKit.getValue("activityAreaAudit"),"true")?true:false);
        return "activityarea/activity-area-audit-detail";
    }

    /**
     * 审核
     * @return
     */
    @RequestMapping({"a/activityarea/audit/doaudit"})
    public String doAudit(ActivityAreaVo activityAreaVo,ModelMap modelMap,RedirectAttributesModelMap redirectAttributesModelMap){
        activityAreaService.audit(activityAreaVo);
        if(RunBinder.hasErrors()){
            this.addErrorMsg(modelMap,RunBinder.getStrErrors());
            modelMap.addAttribute("activityAreaVo",activityAreaService.findById(activityAreaVo.getActivityAreaId()));
            modelMap.addAttribute("areaMap", ReadAreaUtils.getAreaData());
            modelMap.addAttribute("needAudit",StringUtils.equals(SettingKit.getValue("activityAreaAudit"),"true")?true:false);
            return "activityarea/activity-area-audit-detail";
        }else {
            this.addSuccessMsg(redirectAttributesModelMap,"审核成功");
            return "redirect:list";
        }
    }

    /**
     * 删除活动点
     * @param activityAreaId
     * @param redirectModelMap
     * @return
     */
    @RequestMapping({"b/activityarea/delete"})
    public String delete(Long[] activityAreaId,RedirectAttributesModelMap redirectModelMap){
        activityAreaService.deleteByIds(activityAreaId);
        if(RunBinder.hasErrors()){
            this.addErrorMsg(redirectModelMap,RunBinder.getStrErrors());
        }else {
            this.addSuccessMsg(redirectModelMap,"删除成功");
        }
        return "redirect:list";
    }

    /**
     * 更新界面
     * @param activityAreaId
     * @param model
     * @return
     */
    @RequestMapping(value = {"b/activityarea/update"})
    public String updateView(Long activityAreaId,Model model){
        model.addAttribute("activityAreaVo",activityAreaService.findById(activityAreaId));
        model.addAttribute("areaMap", ReadAreaUtils.getAreaData());
        model.addAttribute("needAudit",StringUtils.equals(SettingKit.getValue("activityAreaAudit"),"true")?true:false);
        return "activityarea/activity-area-update";
    }

    /**
     * 更新活动点
     * @param activityAreaVo
     * @param modelMap
     * @param uploadFileIds
     * @return
     */
    @RequestMapping(value = "b/activityarea/update",method = {RequestMethod.POST})
    public String update(ActivityAreaVo activityAreaVo, ModelMap modelMap,String uploadFileIds,RedirectAttributesModelMap redirectModelMap){
        LoginUser loginUser = HttpUtils.getLoginUser();
        activityAreaVo.setModifier(loginUser.getUsername());
        activityAreaService.update(activityAreaVo,uploadFileIds);
        if(RunBinder.hasErrors()) {
            this.addErrorMsg(modelMap,RunBinder.getStrErrors());
            modelMap.addAttribute("areaMap",ReadAreaUtils.getAreaData());
            modelMap.addAttribute("needAudit",StringUtils.equals(SettingKit.getValue("activityAreaAudit"),"true")?true:false);
            return "activityarea/activity-area-update";
        } else {
            this.addSuccessMsg(redirectModelMap, "修改成功");
            return "redirect:list";
        }
    }

    /**
     * 新增活动点页面
     * @return
     */
    @RequestMapping({"b/activityarea/add"})
    public String addView(ModelMap modelMap){
        modelMap.addAttribute("areaMap", ReadAreaUtils.getAreaData());
        modelMap.addAttribute("needAudit", StringUtils.equals(SettingKit.getValue("activityAudit"),"true")?true:false);
        return "activityarea/activity-area-add";
    }

    /**
     * 验证活动名称是否重复
     * @param areaName
     * @param model
     * @return
     */
    @RequestMapping({"b/activityarea/existactivityareaname.json"})
    public String existActivityAreaName(String areaName,Model model){
        model.addAttribute("valid",!activityAreaService.existAreaName(areaName));
        return "";
    }

    @RequestMapping({"/get/area/map.json"})
    public String getAreaMap(Model model){
        model.addAttribute(ResultConst.RESULT,ReadAreaUtils.getAreaData());
        return "";
    }


    /**
     * 新增活动点
     * @param activityAreaVo
     * @param modelMap
     * @param uploadFileIds
     * @param redirectModelMap
     * @return
     */
    @RequestMapping(value={"b/activityarea/add"},method = {RequestMethod.POST})
    public String add(ActivityAreaVo activityAreaVo, ModelMap modelMap, String uploadFileIds,RedirectAttributesModelMap redirectModelMap){
        LoginUser loginUser = HttpUtils.getLoginUser();
        activityAreaVo.setCreator(loginUser.getUsername());
        activityAreaVo.setModifier(loginUser.getUsername());
        activityAreaService.add(activityAreaVo,uploadFileIds);
        if(RunBinder.hasErrors()) {
            this.addErrorMsg(modelMap,RunBinder.getStrErrors());
            modelMap.addAttribute("areaMap", ReadAreaUtils.getAreaData());
            modelMap.addAttribute("needAudit", StringUtils.equals(SettingKit.getValue("activityAudit"),"true")?true:false);
            return "activityarea/activity-area-add";
        } else {
            this.addSuccessMsg(redirectModelMap, "添加成功");
            return "redirect:list";
        }
    }


    /**
     * 根据id获取活动点信息
     * @param activityAreaId
     * @param model
     * @return
     */
    @RequestMapping({"b/activityarea/detail"})
    public String get(Long activityAreaId,Model model){
        model.addAttribute("activityAreaVo",activityAreaService.findById(activityAreaId));
        model.addAttribute("needAudit",StringUtils.equals(SettingKit.getValue("activityAreaAudit"),"true")?true:false);
        model.addAttribute("areaMap", ReadAreaUtils.getAreaData());
        return "activityarea/activity-area-detail";
    }

    @RequestMapping("b/activityarea/searchactivities")
    public String searchActivities(Long activityAreaId,RedirectAttributesModelMap redirectAttributesModelMap){
        redirectAttributesModelMap.addAttribute("activityAreaId",activityAreaId);
        return "redirect:/b/activity/list";
    }

    /**
     * 发布活动点
     * @param activityAreaVo
     * @param isNow
     * @param redirectAttributesModelMap
     * @return
     */
    @RequestMapping({"b/activityarea/issue"})
    public String issuer(ActivityAreaVo activityAreaVo,boolean isNow, RedirectAttributesModelMap redirectAttributesModelMap){
        if(isNow){
            activityAreaVo.setGmtIssue(new Date());
        }
        activityAreaVo.setIssuer(HttpUtils.getLoginUser().getUsername());
        activityAreaService.IssuerActivityArea(activityAreaVo);
        if(RunBinder.hasErrors()) {
            this.addErrorMsg(redirectAttributesModelMap,RunBinder.getStrErrors());
        } else {
            this.addSuccessMsg(redirectAttributesModelMap, "活动点发布成功");
        }
        return "redirect:list";
    }

    @RequestMapping({"b/activityarea/enable.json"})
    public String enable(Long activityAreaId,Model model){
        activityAreaService.enableActivityArea(activityAreaId);
        if(RunBinder.hasErrors()){
            model.addAttribute(ResultConst.MESSAGE,RunBinder.getStrErrors());
            model.addAttribute(ResultConst.SUCCESS,false);
        }else {
            model.addAttribute(ResultConst.SUCCESS,true);
        }
        return "";
    }

    /**
     * 移动端活动点列表接口
     * @param searchVo
     * @param model
     * @return
     */
    @RequestMapping(value = {"m/activityarea/list.json"})
    public String mobileActivityAreaList(SearchVo searchVo, Model model){
        List<ActivityAreaVo> activityAreaVos = activityAreaService.mobilePageList(searchVo);
        if(RunBinder.hasErrors()){
            model.addAttribute(ResultConst.SUCCESS,false);
            model.addAttribute(ResultConst.MESSAGE,RunBinder.getStrErrors());
        }else {
            model.addAttribute(ResultConst.RESULT,activityAreaVos);
            model.addAttribute(ResultConst.SUCCESS,true);
        }
        return "";
    }

    /**
     * 移动端活动点详情接口
     * @param activityAreaId
     * @param model
     * @return
     */
    @RequestMapping("m/activityarea/get.json")
    public String mobileGet(Long activityAreaId,String weixin,Model model){
        ActivityAreaVo activityAreaVo = activityAreaService.mobileFindById(activityAreaId,weixin);
        if(RunBinder.hasErrors()){
            model.addAttribute(ResultConst.SUCCESS,false);
            model.addAttribute(ResultConst.MESSAGE,RunBinder.getStrErrors());
        }else {
            model.addAttribute(ResultConst.RESULT,activityAreaVo);
            model.addAttribute(ResultConst.SUCCESS,true);
        }

        return "";
    }


}
