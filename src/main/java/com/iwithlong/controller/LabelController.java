package com.iwithlong.controller;

import com.imesne.assistant.common.runtime.RunBinder;
import com.iwithlong.context.ResultConst;
import com.iwithlong.entity.Label;
import com.iwithlong.service.LabelService;
import com.iwithlong.utils.ModelUtils;
import com.iwithlong.vo.LabelVo;
import com.ktanx.platform.admin.controller.AdminBaseController;
import com.ktanx.platform.menu.model.Menu;
import com.ktanx.platform.utils.HttpUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributesModelMap;

import javax.annotation.Resource;

/**
 *标签管理
 * <p>
 * User: TJM
 * Date: 2017/9/25 15:13
 * version $Id: LabelController.java, v 0.1  15:13 Exp $
 */
@Controller
@RequestMapping("")
public class LabelController extends AdminBaseController {

	@Resource
	private LabelService labelService;


	@RequestMapping("/a/label/list")
	public String list(LabelVo labelVo, Model model) {

		model.addAttribute("labelList", labelService.pageList (labelVo));
		return "label/label-list";
	}

	@RequestMapping("/a/label/delete.json")
	public String delete(Long labelId, Model model) {

		labelService.deleteLabel (labelId);
		ModelUtils.processModel(model, "删除成功");
		return "";
	}

	@RequestMapping("/a/label/add")
	public String add(Label label, ModelMap modelMap) {
		modelMap.put("label", label);
		return "label/label-add";
	}

	@RequestMapping(value = "a/label/add", method = RequestMethod.POST)
	public String add(Label label, ModelMap modelMap, RedirectAttributesModelMap redirectModelMap) {

		labelService.addLabel (label);
		if (RunBinder.hasErrors()) {
			addErrorMsg(modelMap, RunBinder.getStrErrors());
			return "label/label-add";
		}
		addSuccessMsg(redirectModelMap, "添加成功");
		return  "redirect:list" ;
	}



	@RequestMapping("/a/label/update")
	public String update(Long labelId, ModelMap modelMap) {
		modelMap.put("label", labelService.getLabel (labelId));
		return "label/label-update";
	}

	@RequestMapping(value = "a/label/update", method = RequestMethod.POST)
	public String update(Label label, ModelMap modelMap, RedirectAttributesModelMap redirectModelMap) {

		labelService.modifyLabel (label);
		if (RunBinder.hasErrors()) {
			addErrorMsg(modelMap, RunBinder.getStrErrors());
			return "label/label-update";
		}
		HttpUtils.removeSessionAttr(Menu.Type.ADMIN_MENU.getCode());
		addSuccessMsg(redirectModelMap, "修改成功");
		return "redirect:list" ;
	}



	@RequestMapping("a/label/update_hot_keywords")
	public String updateHotKeywords() {
		labelService.updateHotKeywords ();
		return "redirect:list";
	}


	@RequestMapping("a/label/update_my_words")
	public String updateMyWords() {
		labelService.updateMyWords ();
		return "redirect:list";
	}


	/**
	 * 手机端 热门搜索列表
	 *
	 * @param model
	 * @return
	 */
	@RequestMapping("m/hot/keywords.json")
	public String hotKeywordLists(Model model) {

		model.addAttribute(ResultConst.RESULT,labelService.getHotKeywords());

		return "";
	}


	/**
	 * 手机端 根据类型获取标签列表
	 *
	 * @param model
	 * @return
	 */
	@RequestMapping("m/get/label/by/type.json")
	public String hotKeywordLists(String labelType,Model model) {

		model.addAttribute(ResultConst.RESULT,labelService.getLabelByType(labelType));

		return "";
	}

	@RequestMapping("b/label/activitylabels.json")
	public String getAllActivityLabels(Model model){
		model.addAttribute(ResultConst.RESULT,labelService.getActivityLabels());
		return "";
	}

}
