package com.iwithlong.controller;

import com.imesne.assistant.common.page.PageList;
import com.imesne.assistant.common.runtime.RunBinder;
import com.iwithlong.context.ResultConst;
import com.iwithlong.entity.Advert;
import com.iwithlong.service.ActivityService;
import com.iwithlong.service.AdvertService;
import com.iwithlong.vo.ActivityVo;
import com.iwithlong.vo.AdvertVo;
import com.ktanx.platform.admin.controller.AdminBaseController;
import com.ktanx.platform.utils.HttpUtils;
import com.ktanx.platform.web.LoginUser;
import com.ktanx.platform.web.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributesModelMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.List;

@Controller
@RequestMapping("b/advert")
public class AdvertController extends AdminBaseController {


    @Autowired
    private ActivityService activityService;

    @Autowired
    private AdvertService advertService;

    @GetMapping("list")
    public String advertList(AdvertVo advertVo,boolean getSession,Model model,HttpServletRequest request){
        HttpSession session = request.getSession();
        if(getSession){
            advertVo = (AdvertVo) session.getAttribute("advertVo");
        }else {
            session.setAttribute("advertVo",advertVo);
        }
        PageList<AdvertVo> advertVos = advertService.activityPageList(advertVo);
        model.addAttribute("advertVo",advertVo);
        model.addAttribute(ResultConst.RESULT,advertVos);
        model.addAttribute("page", Page.wrap(advertVos));
        return "advert/advert-list";
    }

    /**
     * 广告位添加页面
     *
     * @param modelMap
     * @return
     */
    @GetMapping("add")
    public String add(AdvertVo advertVo,ModelMap modelMap) {
        modelMap.addAttribute("advertVo",advertVo);
        return "advert/advert-add";
    }


    /**
     * 广告位添加
     *
     * @param advert   the advert
     * @param file     the file
     * @param modelMap the model map
     * @return string string
     */
    @PostMapping("add")
    public String add(Advert advert, MultipartFile file, ModelMap modelMap,RedirectAttributesModelMap redirectAttributesModelMap) {

        LoginUser loginUser = HttpUtils.getLoginUser();
        advert.setCreator(loginUser.getUsername());
        advertService.add(advert, file);
        if(RunBinder.hasErrors()){
            this.addErrorMsg(modelMap,RunBinder.getStrErrors());
            return "advert/advert-add";
        }else {
            this.addSuccessMsg(redirectAttributesModelMap,"新增成功");
            redirectAttributesModelMap.addAttribute("getSession", true);
            return "redirect:list";
        }
    }

    /**
     * 发布
     * @param advertId
     * @param redirectAttributesModelMap
     * @return
     */
    @PostMapping("issue")
    public String issue(Long[] advertId,RedirectAttributesModelMap redirectAttributesModelMap){
        advertService.changeStatus(advertId, Advert.Status.RELEASE.getCode());
        if(RunBinder.hasErrors()){
            this.addErrorMsg(redirectAttributesModelMap,RunBinder.getStrErrors());
        }else {
            this.addSuccessMsg(redirectAttributesModelMap,"发布成功");
        }
        redirectAttributesModelMap.addAttribute("getSession", true);
        return "redirect:list";
    }

    /**
     * 禁用
     * @param advertId
     * @param redirectAttributesModelMap
     * @return
     */
    @PostMapping("unable")
    public String unable(Long[] advertId,RedirectAttributesModelMap redirectAttributesModelMap){
        advertService.changeStatus(advertId,Advert.Status.NORMAL.getCode());
        if(RunBinder.hasErrors()){
            this.addErrorMsg(redirectAttributesModelMap,RunBinder.getStrErrors());
        }else {
            this.addSuccessMsg(redirectAttributesModelMap,"禁用成功");
        }
        redirectAttributesModelMap.addAttribute("getSession",true);
        return "redirect:list";
    }

    /**
     * 删除
     * @param advertId
     * @param redirectAttributesModelMap
     * @return
     */
    @RequestMapping("delete")
    public String delete(Long[] advertId,RedirectAttributesModelMap redirectAttributesModelMap){
        advertService.delete(advertId);
        if(RunBinder.hasErrors()){
            this.addErrorMsg(redirectAttributesModelMap,RunBinder.getStrErrors());
        }else {
            this.addSuccessMsg(redirectAttributesModelMap,"删除成功");
        }
        redirectAttributesModelMap.addAttribute("getSession",true);
        return "redirect:list";
    }

    /**
     * 更新页面
     * @param advertVo
     * @param modelMap
     * @return
     */
    @GetMapping("update")
    public String updateView(AdvertVo advertVo,ModelMap modelMap){
        modelMap.addAttribute("advertVo",advertService.findById(advertVo.getAdvertId()));
        return "advert/advert-update";
    }

    /**
     * 更新
     * @param advertVo
     * @param file
     * @param modelMap
     * @param redirectAttributesModelMap
     * @return
     */
    @PostMapping("update")
    public String update(AdvertVo advertVo,MultipartFile file,ModelMap modelMap,RedirectAttributesModelMap redirectAttributesModelMap){
        advertService.update(advertVo, file);
        if(RunBinder.hasErrors()){
            this.addErrorMsg(modelMap,RunBinder.getStrErrors());
            modelMap.addAttribute("advertVo",advertVo);
            return "advert/advert-update";
        }else {
            this.addSuccessMsg(redirectAttributesModelMap,"修改成功");
            redirectAttributesModelMap.addAttribute("getSession", true);
            return "redirect:list";
        }
    }

    /**
     * 上移
     * @param advertId
     * @param redirectAttributesModelMap
     * @return
     */
    @RequestMapping("up")
    public String up(Long advertId,RedirectAttributesModelMap redirectAttributesModelMap){
        advertService.up(advertId);
        if(RunBinder.hasErrors()){
            this.addErrorMsg(redirectAttributesModelMap, RunBinder.getStrErrors());
        }else {
            this.addSuccessMsg(redirectAttributesModelMap, "上移成功");
        }
        redirectAttributesModelMap.addAttribute("getSession",true);
        return "redirect:list";
    }

    /**
     * 下移
     * @param advertId
     * @param redirectAttributesModelMap
     * @return
     */
    @RequestMapping("down")
    public String down(Long advertId,RedirectAttributesModelMap redirectAttributesModelMap){
        advertService.down(advertId);
        if(RunBinder.hasErrors()){
            this.addErrorMsg(redirectAttributesModelMap,RunBinder.getStrErrors());
        }else {
            this.addSuccessMsg(redirectAttributesModelMap, "下移成功");
        }
        redirectAttributesModelMap.addAttribute("getSession",true);
        return "redirect:list";
    }


    /**
     * 置顶
     * @param advertId
     * @param redirectAttributesModelMap
     * @return
     */
    @RequestMapping("top")
    public String top(Long advertId,RedirectAttributesModelMap redirectAttributesModelMap){
        advertService.top(advertId);
        if(RunBinder.hasErrors()){
            this.addErrorMsg(redirectAttributesModelMap, RunBinder.getStrErrors());
        }else {
            this.addSuccessMsg(redirectAttributesModelMap,"置顶成功");
        }
        redirectAttributesModelMap.addAttribute("getSession",true);
        return "redirect:list";
    }

    /**
     * 置底
     * @param advertId
     * @param redirectAttributesModelMap
     * @return
     */
    @RequestMapping("bottom")
    public String bottom(Long advertId,RedirectAttributesModelMap redirectAttributesModelMap){
        advertService.bottom(advertId);
        if(RunBinder.hasErrors()){
            this.addErrorMsg(redirectAttributesModelMap, RunBinder.getStrErrors());
        }else {
            this.addSuccessMsg(redirectAttributesModelMap,"置底成功");
        }
        redirectAttributesModelMap.addAttribute("getSession",true);
        return "redirect:list";
    }
}
