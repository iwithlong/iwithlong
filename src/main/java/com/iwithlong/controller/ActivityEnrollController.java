package com.iwithlong.controller;

import com.imesne.assistant.common.page.PageList;
import com.imesne.assistant.common.runtime.RunBinder;
import com.iwithlong.context.ResultConst;
import com.iwithlong.enums.ActivityEnrollStatus;
import com.iwithlong.service.ActivityEnrollService;
import com.iwithlong.service.ProductSmsService;
import com.iwithlong.vo.ActivityEnrollVo;
import com.iwithlong.vo.ActivityVo;
import com.iwithlong.vo.EnrollNamelistVo;
import com.iwithlong.vo.ProductSmsVo;
import com.ktanx.platform.admin.controller.AdminBaseController;
import com.ktanx.platform.web.Page;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributesModelMap;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Map;

/**
 * Created by mhy on 2018/5/1.
 */
@Controller
public class ActivityEnrollController extends AdminBaseController {
    @Resource
    private ActivityEnrollService activityEnrollService;

    @Resource
    private ProductSmsService productSmsService;

    /**
     * 报名单列表
     * @param activityEnrollVo
     * @param getSession
     * @param request
     * @param model
     * @return
     */
    @RequestMapping({"b/activityenroll/list"})
    public String list(ActivityEnrollVo activityEnrollVo,boolean getSession,HttpServletRequest request,Model model){
        HttpSession session = request.getSession();
        if(getSession){
            activityEnrollVo = (ActivityEnrollVo)session.getAttribute("activityEnrollVo");
        }else {
            session.setAttribute("activityEnrollVo",activityEnrollVo);
        }
        PageList<ActivityEnrollVo> activityEnrollVos = activityEnrollService.pageList(activityEnrollVo);
        model.addAttribute(ResultConst.RESULT,activityEnrollVos);
        model.addAttribute("activityEnrollVo",activityEnrollVo);
        model.addAttribute("page", Page.wrap(activityEnrollVos));
        return "activityEnroll/activity-enroll-list";
    }

    /**
     * 报名单新增页面
     *
     * @return
     */
    @RequestMapping({"b/activityenroll/add"})
    public String addView() {
        return "activityEnroll/activity-enroll-add";
    }

    /**
     * 报名单新增后台
     * @param activityEnrollVo
     * @param modelMap
     * @param redirectAttributesModelMap
     * @return
     */
    @RequestMapping(value = {"b/activityenroll/add"},method = RequestMethod.POST)
    public String merchantAdd(ActivityEnrollVo activityEnrollVo, boolean addProduct,boolean deleteProduct,int deleteOrd, ModelMap modelMap,RedirectAttributesModelMap redirectAttributesModelMap) {
        if(!addProduct&&!deleteProduct){
            activityEnrollService.add(activityEnrollVo);
            if(RunBinder.hasErrors()){
                this.addErrorMsg(modelMap,RunBinder.getStrErrors());
                return "activityEnroll/activity-enroll-add";
            }else {
                this.addSuccessMsg(redirectAttributesModelMap,"新增报名单成功");
                redirectAttributesModelMap.addAttribute("getSession",true);
                return "redirect:list";
            }
        }
        //新增分组
        if(!deleteProduct){
            activityEnrollService.addEnrollProductVo(activityEnrollVo);
        }else {
            //删除分组
            activityEnrollVo.getEnrollProductVos().remove(deleteOrd);
        }
        return "activityEnroll/activity-enroll-add";
    }

    /**
     * 判断商家是否存在
     * @param activityEnrollVo
     * @param model
     * @return
     */
    @RequestMapping(value = {"b/activityenroll/existmembername.json"})
    public String existMemberName(ActivityEnrollVo activityEnrollVo,Model model){
        model.addAttribute("valid", activityEnrollService.validateMember(activityEnrollVo));
        return "";
    }

    /**
     * 判断活动是否存在或是否已经存在报名单
     * @param activityEnrollVo
     * @param model
     * @return
     */
    @RequestMapping(value = {"b/activityenroll/existactivityname.json"})
    public String existActivityName(ActivityEnrollVo activityEnrollVo,Model model){
        model.addAttribute("valid",activityEnrollService.validateActivity(activityEnrollVo));
        return "";
    }

    @RequestMapping(value = {"b/activityenroll/namefilter.json"})
    public String nameFilter(ActivityEnrollVo activityEnrollVo,Model model){
        model.addAttribute(ResultConst.RESULT,activityEnrollService.nameFilter(activityEnrollVo));
        return "";
    }

    /**
     * 活动报名更新页面
     * @param activityEnrollId
     * @param modelMap
     * @return
     */
    @RequestMapping(value = {"b/activityenroll/update"})
    public String updateView(Long activityEnrollId,ModelMap modelMap,RedirectAttributesModelMap redirectAttributesModelMap){
        ActivityEnrollVo activityEnrollVo = activityEnrollService.findById(activityEnrollId);
        if(!StringUtils.equals(activityEnrollVo.getStatus(), ActivityEnrollStatus.UNABLE.getCode())){
            this.addErrorMsg(redirectAttributesModelMap, "报名启动后无法修改");
            redirectAttributesModelMap.addAttribute("getSession", true);
            return "redirect:list";
        }
        modelMap.addAttribute("activityEnrollVo", activityEnrollVo);
        return "activityEnroll/activity-enroll-update";
    }

    /**
     * 活动报名更新
     * @param activityEnrollVo
     * @param modelMap
     * @param redirectAttributesModelMap
     * @return
     */
    @RequestMapping(value = {"b/activityenroll/update"},method = RequestMethod.POST)
    public String update(ActivityEnrollVo activityEnrollVo, boolean addProduct,boolean deleteProduct,int deleteOrd, ModelMap modelMap,RedirectAttributesModelMap redirectAttributesModelMap){
        if(!addProduct&&!deleteProduct){
            activityEnrollService.update(activityEnrollVo);
            if(RunBinder.hasErrors()){
                this.addErrorMsg(modelMap,RunBinder.getStrErrors());
                return "activityEnroll/activity-enroll-update";
            }else {
                this.addSuccessMsg(redirectAttributesModelMap,"修改成功");
                redirectAttributesModelMap.addAttribute("getSession",true);
                return "redirect:list";
            }
        }
        //新增分组
        if(!deleteProduct){
            activityEnrollService.addEnrollProductVo(activityEnrollVo);
        }else {
            //删除分组
            activityEnrollVo.getEnrollProductVos().remove(deleteOrd);
        }
        return "activityEnroll/activity-enroll-update";
    }

    /**
     * 报名单启动
     * @param activityEnrollId
     * @param redirectAttributesModelMap
     * @return
     */
    @RequestMapping(value = {"b/activityenroll/enable"})
    public String enable(Long activityEnrollId,RedirectAttributesModelMap redirectAttributesModelMap){
        activityEnrollService.enable(activityEnrollId);
        if(RunBinder.hasErrors()){
            this.addErrorMsg(redirectAttributesModelMap, RunBinder.getStrErrors());
        }else {
            this.addSuccessMsg(redirectAttributesModelMap, "启动成功");
        }
        redirectAttributesModelMap.addAttribute("getSession",true);
        return "redirect:list";
    }

    /**
     * 删除报名单
     * @param activityEnrollId
     * @param redirectAttributesModelMap
     * @return
     */
    @RequestMapping(value = {"b/activityenroll/delete"})
    public String delete(Long[] activityEnrollId,RedirectAttributesModelMap redirectAttributesModelMap){
        activityEnrollService.deleteByIds(activityEnrollId);
        if(RunBinder.hasErrors()){
            this.addErrorMsg(redirectAttributesModelMap, RunBinder.getStrErrors());
        }else {
            this.addSuccessMsg(redirectAttributesModelMap, "删除成功");
        }
        redirectAttributesModelMap.addAttribute("getSession", true);
        return "redirect:list";
    }

    /**
     * 报名单详情
     * @param enrollNamelistVo
     * @param model
     * @return
     */
    @RequestMapping(value = {"b/activityenroll/detail/list"})
    public String activityEnrollDetail(EnrollNamelistVo enrollNamelistVo, boolean getSession,HttpServletRequest request,Model model){
        HttpSession session = request.getSession();
        if(getSession){
            enrollNamelistVo = (EnrollNamelistVo)session.getAttribute("enrollNamelistVo");
        }else {
            session.setAttribute("enrollNamelistVo",enrollNamelistVo);
        }
        ActivityEnrollVo activityEnrollVo = activityEnrollService.findById(enrollNamelistVo.getActivityEnrollId());
        PageList<EnrollNamelistVo> enrollNamelistVos = activityEnrollService.namePageList(enrollNamelistVo,activityEnrollVo);
        model.addAttribute("activityEnrollVo",activityEnrollVo);
        model.addAttribute("enrollNamelistVo",enrollNamelistVo);
        model.addAttribute(ResultConst.RESULT, enrollNamelistVos);
        model.addAttribute("page",Page.wrap(enrollNamelistVos));
        return "activityEnroll/activity-enroll-detail";
    }

    /**
     * 移动端报名接口
     * @param enrollNamelistVo
     * @param model
     * @return
     */
    @RequestMapping(value = {"m/activityenroll/enroll.json"})
    public String enroll(EnrollNamelistVo enrollNamelistVo,Model model){
        activityEnrollService.enroll(enrollNamelistVo);
        if(RunBinder.hasErrors()){
            model.addAttribute(ResultConst.MESSAGE,RunBinder.getStrErrors());
            model.addAttribute(ResultConst.SUCCESS,false);
        }else {
            model.addAttribute(ResultConst.SUCCESS,true);
        }
        return "";
    }

    /**
     * 我的报名单接口
     * @param enrollNamelistVo
     * @param model
     * @return
     */
    @RequestMapping(value = "m/activityenroll/myenroll.json")
    public String myEnroll(EnrollNamelistVo enrollNamelistVo,Model model){
        PageList<EnrollNamelistVo> result = activityEnrollService.myEnrollPageList(enrollNamelistVo);
        if(RunBinder.hasErrors()){
            model.addAttribute(ResultConst.MESSAGE,RunBinder.getStrErrors());
            model.addAttribute(ResultConst.SUCCESS,false);
        }else {
            model.addAttribute(ResultConst.SUCCESS,true);
            model.addAttribute(ResultConst.RESULT,result);
        }
        return "";
    }

    /**
     * 签到
     * @param enrollNamelistId
     * @param redirectAttributesModelMap
     * @return
     */
    @RequestMapping(value = "b/activityenroll/detail/sign")
    public String sign(Long[] enrollNamelistId,RedirectAttributesModelMap redirectAttributesModelMap){
        activityEnrollService.sign(enrollNamelistId);
        if(RunBinder.hasErrors()){
            this.addErrorMsg(redirectAttributesModelMap, RunBinder.getStrErrors());
        }else {
            this.addSuccessMsg(redirectAttributesModelMap, "签到成功");
        }
        redirectAttributesModelMap.addAttribute("getSession", true);
        return "redirect:list";
    }

    /**
     * 取消并退款
     * @param enrollNamelistId
     * @param redirectAttributesModelMap
     * @return
     */
    @RequestMapping(value = "b/activityenroll/detail/cancel")
    public String cancel(Long[] enrollNamelistId,RedirectAttributesModelMap redirectAttributesModelMap){
        activityEnrollService.cancel(enrollNamelistId);
        if(RunBinder.hasErrors()){
            this.addErrorMsg(redirectAttributesModelMap, RunBinder.getStrErrors());
        }else {
            this.addSuccessMsg(redirectAttributesModelMap, "取消并退款成功");
        }
        redirectAttributesModelMap.addAttribute("getSession", true);
        return "redirect:list";
    }

    /**
     * 报名名单导出
     * @param request
     * @param response
     * @param redirectAttributesModelMap
     */
    @RequestMapping(value = "b/activityenroll/detail/export")
    public void exportExcel(HttpServletRequest request,HttpServletResponse response,RedirectAttributesModelMap redirectAttributesModelMap){
        EnrollNamelistVo enrollNamelistVo = (EnrollNamelistVo)request.getSession().getAttribute("enrollNamelistVo");
        enrollNamelistVo.setPageNum(1);
        enrollNamelistVo.setPageSize(Integer.MAX_VALUE);
        ActivityEnrollVo activityEnrollVo = activityEnrollService.findById(enrollNamelistVo.getActivityEnrollId());
        PageList<EnrollNamelistVo> enrollNamelistVos = activityEnrollService.namePageList(enrollNamelistVo,activityEnrollVo);
        activityEnrollService.exportExcel(enrollNamelistVos, response);
    }

    @RequestMapping(value = "b/activityenroll/detail/detail")
    public String enrollNamelistDetail(Long enrollNamelistId,Model model){
        EnrollNamelistVo enrollNamelistVo = activityEnrollService.getEnrollNamelistDetailById(enrollNamelistId);
        model.addAttribute(ResultConst.RESULT, enrollNamelistVo);
        return "activityEnroll/enroll-namelist-detail";
    }

    /**
     * 完成
     * @param activityEnrollId
     * @param redirectAttributesModelMap
     * @return
     */
    @RequestMapping(value = "b/activityenroll/complete")
    public String complete(Long[] activityEnrollId,RedirectAttributesModelMap redirectAttributesModelMap){
        activityEnrollService.complete(activityEnrollId);
        if(RunBinder.hasErrors()){
            this.addErrorMsg(redirectAttributesModelMap,RunBinder.getStrErrors());
        }else {
            this.addSuccessMsg(redirectAttributesModelMap, "报名单完成，报名费将转至该商家账户");
        }
        redirectAttributesModelMap.addAttribute("getSession", true);
        return "redirect:list";
    }

    @RequestMapping(value = "m/activityenroll/list.json")
    public String mobileList(ActivityEnrollVo activityEnrollVo,Model model){
        PageList<ActivityVo> activityVos = activityEnrollService.mobilePageListActivityVo(activityEnrollVo);
        if(RunBinder.hasErrors()){
            model.addAttribute(ResultConst.SUCCESS,false);
            model.addAttribute(ResultConst.MESSAGE, RunBinder.getStrErrors());
        }else {
            model.addAttribute(ResultConst.SUCCESS,true);
            model.addAttribute(ResultConst.RESULT, activityVos);
        }
        return "";
    }

    /**
     * 按产品短信群发
     * @param productId
     * @param model
     * @param redirectAttributesModelMap
     * @return
     */
    @RequestMapping(value = "b/activityenroll/detail/sendsmsbyproduct")
    public String sendsms(Long productId,Model model,RedirectAttributesModelMap redirectAttributesModelMap){
        ProductSmsVo result =  productSmsService.sendMsgByProduct(productId);
        if(RunBinder.hasErrors()){
            this.addErrorMsg(redirectAttributesModelMap, RunBinder.getStrErrors());
            redirectAttributesModelMap.addAttribute("getSession",true);
            return "redirect:list";
        }else {
            model.addAttribute("productSmsVo",result);
            return "productSms/product-sms-send";
        }
    }

    /**
     * 短信群发
     * @param enrollNamelistId
     * @param model
     * @param redirectAttributesModelMap
     * @return
     */
    @RequestMapping(value = "b/activityenroll/detail/sendsms")
    public String sendsms(Long[] enrollNamelistId,Model model,RedirectAttributesModelMap redirectAttributesModelMap){
        ProductSmsVo result =  productSmsService.sendMsgByEnrollNamelistIds(enrollNamelistId);
        if(RunBinder.hasErrors()){
            this.addErrorMsg(redirectAttributesModelMap,RunBinder.getStrErrors());
            redirectAttributesModelMap.addAttribute("getSession",true);
            return "redirect:list";
        }else {
            model.addAttribute("productSmsVo",result);
            return "productSms/product-sms-send";
        }
    }
}
