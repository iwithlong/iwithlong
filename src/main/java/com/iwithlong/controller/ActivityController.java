package com.iwithlong.controller;

import com.imesne.assistant.common.page.PageList;
import com.imesne.assistant.common.runtime.RunBinder;
import com.iwithlong.context.ResultConst;
import com.iwithlong.enums.ActivityStatus;
import com.iwithlong.service.ActivityAreaService;
import com.iwithlong.service.ActivityService;
import com.iwithlong.utils.SessionUtil;
import com.iwithlong.vo.ActivityVo;
import com.iwithlong.vo.SearchVo;
import com.ktanx.platform.admin.controller.AdminBaseController;
import com.ktanx.platform.setting.kit.SettingKit;
import com.ktanx.platform.utils.HttpUtils;
import com.ktanx.platform.web.Page;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributesModelMap;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by xiexiaohan on 2017/10/10.
 * 活动控制层
 */
@Controller
public class ActivityController extends AdminBaseController {
    @Resource
    private ActivityService activityService;


    @Resource
    private ActivityAreaService activityAreaService;


    /**
     * 活动列表
     * @param activityVo
     * @param model
     * @return
     */
    @RequestMapping("b/activity/list")
    public String list(ActivityVo activityVo, boolean getSession, HttpServletRequest request, Model model){
        HttpSession session = request.getSession();
        if(getSession){
            activityVo = (ActivityVo) session.getAttribute("searchVo");
        }else {
            session.setAttribute("searchVo",activityVo);
        }
        model.addAttribute("activityVo",activityVo);
        PageList<ActivityVo> activityVos = activityService.pageList(activityVo);
        model.addAttribute(ResultConst.RESULT,activityVos);
        model.addAttribute("page", Page.wrap(activityVos));
        model.addAttribute("needAudit", StringUtils.equals(SettingKit.getValue("activityAudit"),"true")?true:false);
        return "activity/activity-list";
    }

    /**
     * 活动添加页面
     * @return
     */
    @RequestMapping("b/activity/add")
    public String addView(Model model){
        model.addAttribute("needAudit", StringUtils.equals(SettingKit.getValue("activityAudit"),"true")?true:false);
        return "activity/activity-add";
    }

    /**
     * 添加活动
     * @param activityVo
     * @param uploadFileIds
     * @param modelMap
     * @param redirectAttributesModelMap
     * @return
     */
    @RequestMapping(value = {"b/activity/add"},method = {RequestMethod.POST})
    public String add(ActivityVo activityVo, String uploadFileIds, ModelMap modelMap, RedirectAttributesModelMap redirectAttributesModelMap){
        activityService.add(activityVo, uploadFileIds);
        if(RunBinder.hasErrors()){
            this.addErrorMsg(modelMap,RunBinder.getStrErrors());
            modelMap.addAttribute("needAudit", StringUtils.equals(SettingKit.getValue("activityAudit"),"true")?true:false);
            return "activity/activity-add";
        }else {
            this.addSuccessMsg(redirectAttributesModelMap,"新增活动成功");
            redirectAttributesModelMap.addAttribute("getSession",true);
            return "redirect:list";
        }
    }

    @RequestMapping(value = {"b/activity/existactivityname.json"})
    public String existActivityName(String name,Model model){
        model.addAttribute("valid", !activityService.existActivityName(name));
        return "";
    }

    @RequestMapping(value = {"b/activity/existactivityareaname.json"})
    public String existActivityAreaName(ActivityVo activityVo,Model model){
        model.addAttribute("valid",activityAreaService.existAreaName(activityVo.getActivityAreaVo().getAreaName()));
        return "";
    }


    /**
     * 获取活动详情
     * @param activityId
     * @param model
     * @return
     */
    @RequestMapping("b/activity/get")
    public String get(Long activityId,Model model){
        model.addAttribute(ResultConst.RESULT,activityService.findById(activityId));
        return "/activity/get";
    }

    /**
     * 获取活动详情
     * @param activityId
     * @param model
     * @return
     */
    @RequestMapping("b/activity/get.json")
    public String getJson(Long activityId,Model model){
        model.addAttribute(ResultConst.RESULT,activityService.findById(activityId));
        return "/activity/get";
    }


    /**
     * 删除活动
     * @param activityId
     * @param redirectAttributesModelMap
     * @return
     */
    @RequestMapping("b/activity/delete")
    public String delete(Long[] activityId,RedirectAttributesModelMap redirectAttributesModelMap){
        activityService.deleteByIds(activityId);
        if(RunBinder.hasErrors()){
            this.addErrorMsg(redirectAttributesModelMap,RunBinder.getStrErrors());
        }else {
            this.addSuccessMsg(redirectAttributesModelMap,"删除成功");
        }
        redirectAttributesModelMap.addAttribute("getSession",true);
        return "redirect:list";
    }

    /**
     *启用／禁用活动
     * @param activityId
     * @param model
     * @return
     */
    @RequestMapping("b/activity/enable.json")
    public String enable(Long activityId,Model model){
        activityService.enable(activityId);
        if(RunBinder.hasErrors()){
            model.addAttribute(ResultConst.SUCCESS,false);
            model.addAttribute(ResultConst.MESSAGE,RunBinder.getStrErrors());
        }else {
            model.addAttribute(ResultConst.SUCCESS,true);
        }
        return "";
    }

    /**
     * 更新活动
     * @param activityId
     * @param modelMap
     * @param redirectAttributesModelMap
     * @return
     */
    @RequestMapping("b/activity/update")
    public String update(Long activityId,ModelMap modelMap,RedirectAttributesModelMap redirectAttributesModelMap){
        modelMap.addAttribute("activityVo",activityService.findById(activityId));
        modelMap.addAttribute("needAudit", StringUtils.equals(SettingKit.getValue("activityAudit"), "true") ? true : false);
        return "activity/activity-update";
    }

    /**
     * 更新活动
     * @param activityVo
     * @param modelMap
     * @param uploadFileIds
     * @param redirectAttributesModelMap
     * @return
     */
    @RequestMapping(value = {"b/activity/update"},method = {RequestMethod.POST})
    public String update(ActivityVo activityVo,ModelMap modelMap,String uploadFileIds,RedirectAttributesModelMap redirectAttributesModelMap){
        activityService.update(activityVo, uploadFileIds);
        if(RunBinder.hasErrors()){
            this.addErrorMsg(modelMap,RunBinder.getStrErrors());
            modelMap.addAttribute("needAudit", StringUtils.equals(SettingKit.getValue("activityAudit"),"true")?true:false);
            return "activity/activity-update";
        }else {
            this.addSuccessMsg(redirectAttributesModelMap,"修改成功");
            redirectAttributesModelMap.addAttribute("getSession",true);
            return "redirect:list";
        }
    }

    /**
     * 详情
     * @param activityId
     * @param modelMap
     * @return
     */
    @RequestMapping(value = {"b/activity/detail"})
    public String detail(Long activityId,ModelMap modelMap){
        modelMap.addAttribute("activityVo", activityService.findById(activityId));
        modelMap.addAttribute("needAudit", StringUtils.equals(SettingKit.getValue("activityAudit"),"true")?true:false);
        return "activity/activity-detail";
    }


    /**
     * 审核列表
     * @param activityVo
     * @param modelMap
     * @return
     */
    @RequestMapping(value = {"a/activity/audit/list"})
    public String auditList(ActivityVo activityVo,ModelMap modelMap){
        activityVo.setStatus(ActivityStatus.NEEDREVIEW.getCode());
        PageList<ActivityVo> activityVos = activityService.pageList(activityVo);
        modelMap.addAttribute(ResultConst.RESULT,activityVos);
        modelMap.addAttribute("page", Page.wrap(activityVos));
        return "activity/activity-audit-list";
    }

    /**
     * 审核详情
     * @param modelMap
     * @return
     */
    @RequestMapping(value = {"a/activity/audit/goaudit"})
    public String goAudit(ActivityVo activityVo,ModelMap modelMap){
        modelMap.addAttribute("activityVo",activityService.findById(activityVo.getActivityId()));
        return "activity/activity-audit-detail";
    }

    @RequestMapping(value = {"a/activity/audit/doaudit"},method = {RequestMethod.POST})
    public String doAudit(ActivityVo activityVo,ModelMap modelMap,RedirectAttributesModelMap redirectAttributesModelMap){
        activityService.doAudit(activityVo);
        if(RunBinder.hasErrors()){
            this.addErrorMsg(modelMap,RunBinder.getStrErrors());
            modelMap.addAttribute("activityVo",activityService.findById(activityVo.getActivityId()));
            return "activity/activity-audit-detail";
        }else {
            this.addSuccessMsg(redirectAttributesModelMap,"审核成功");
            return "redirect:list";
        }
    }

    @RequestMapping(value = {"b/activity/issue.json"},method = {RequestMethod.POST})
    public String issuer(ActivityVo activityVo,boolean isNow, Model model){
        if(isNow){
            activityVo.setGmtIssue(new Date());
        }
        activityVo.setIssuer(HttpUtils.getLoginUser().getUsername());
        activityService.IssuerActivityArea(activityVo);
        if(RunBinder.hasErrors()) {
            model.addAttribute(ResultConst.SUCCESS, false);
            model.addAttribute(ResultConst.MESSAGE,RunBinder.getStrErrors());
        } else {
            model.addAttribute(ResultConst.SUCCESS, true);
        }
        return "";
    }

    /**
     * 感兴趣
     * @param activityId
     * @param weixin
     * @param isInterest
     * @param model
     * @return
     */
    @RequestMapping("m/activity/interest.json")
    public String interestActivity(Long activityId,String weixin,boolean isInterest,Model model){
        Map<String,Object> result = activityService.interest(activityId, weixin, isInterest);
        if(RunBinder.hasErrors()){
            model.addAttribute(ResultConst.SUCCESS,false);
            model.addAttribute(ResultConst.MESSAGE,RunBinder.getStrErrors());
        }else {
            model.addAllAttributes(result);
        }
        return "";
    }

    /**
     * 感兴趣列表
     * @param searchVo
     * @param model
     * @return
     */
    @RequestMapping("m/activity/interest/list.json")
    public String interstActivityList(SearchVo searchVo,Model model){
        List<ActivityVo> activityVos = activityService.searchActivity(searchVo);
        if(RunBinder.hasErrors()){
            model.addAttribute(ResultConst.SUCCESS,false);
            model.addAttribute(ResultConst.MESSAGE,RunBinder.getStrErrors());
        }else {
            model.addAttribute(ResultConst.SUCCESS,true);
            model.addAttribute(ResultConst.RESULT, activityVos);
        }
        return "";
    }


    /**
     * 根据坐标点和查询条件查找活动
     * @param searchVo
     * @param model
     * @return
     */
    @RequestMapping("m/search/activity.json")
    public String searchActivity(SearchVo searchVo, Model model){

        //根据查询条件查找活动
        List<ActivityVo> activityVos=activityService.searchActivity(searchVo);
//        activityService.dealActivityInterest(activityVos,weixin);
        if(RunBinder.hasErrors()){
            model.addAttribute(ResultConst.SUCCESS,false);
            model.addAttribute(ResultConst.MESSAGE,RunBinder.getStrErrors());
        }else {
            model.addAttribute(ResultConst.SUCCESS,true);
            model.addAttribute(ResultConst.RESULT,activityVos);
        }
        return "";
    }



    /**
     * 发现活动接口，取权重最高前50条
     * @param model
     * @return
     */
    @RequestMapping("m/discover/activity.json")
    public String discover(SearchVo searchVo, Model model){

//        SearchVo searchVo=new SearchVo ();
//        searchVo.setPageSize (50);
        //根据查询条件查找活动
        List<ActivityVo> activityVos=activityService.searchActivity(searchVo);
//        activityService.dealActivityInterest(activityVos,weixin);
        if(RunBinder.hasErrors()){
            model.addAttribute(ResultConst.SUCCESS,false);
            model.addAttribute(ResultConst.MESSAGE,RunBinder.getStrErrors());
        }else {
            model.addAttribute(ResultConst.SUCCESS,true);
            model.addAttribute(ResultConst.RESULT,activityVos);
        }
        return "";
    }

    /**
     * 手机端活动详情接口
     * @param activityId
     * @param weixin
     * @param model
     * @return
     */
    @RequestMapping("m/activity/get.json")
    public String mobileGet(Long activityId,String weixin,Model model){
        ActivityVo activityVo = activityService.mobileFindById(activityId, weixin);
        if(RunBinder.hasErrors()){
            model.addAttribute(ResultConst.SUCCESS,false);
            model.addAttribute(ResultConst.MESSAGE,RunBinder.getStrErrors());
        }else {
            model.addAttribute(ResultConst.RESULT,activityVo);
            model.addAttribute(ResultConst.SUCCESS,true);
        }
        return "";
    }

    @RequestMapping("b/activity/namefilter.json")
    public String activityNameFilter(ActivityVo activityVo,Model model){
        List<ActivityVo> activityVos = activityService.nameFilter(activityVo);
        model.addAttribute(ResultConst.RESULT,activityVos);
        return "";
    }




}
