package com.iwithlong.controller;

import com.imesne.assistant.common.runtime.RunBinder;
import com.iwithlong.context.ResultConst;
import com.iwithlong.service.UploadFileService;
import com.ktanx.platform.admin.controller.AdminBaseController;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.OutputStream;

/**
 * Created by xiexiaohan on 2017/10/12.
 */
@Controller
@RequestMapping()
public class UploadFileController extends AdminBaseController {
    @Resource
    UploadFileService uploadFileService;

    @Value("${uploadFilePath:null}")
    private String uploadFilePath;

    @RequestMapping(value="b/uploadfile/upload.json",method = {RequestMethod.POST})
    public String upload(MultipartFile[] multipartFile, Model model){
        model.addAttribute(ResultConst.RESULT,uploadFileService.uploadFile(multipartFile));
        if(RunBinder.hasErrors()){
            model.addAttribute(ResultConst.MESSAGE,RunBinder.getStrErrors());
            model.addAttribute(ResultConst.SUCCESS,false);
        }else {
            model.addAttribute(ResultConst.SUCCESS,true);
        }
        return "";
    }

    @RequestMapping(value = "b/ueditor/upload.json")
    public String uEditorFileUpload(MultipartFile[] multipartFile,Model model){
        uploadFileService.uEditorUploadFile(multipartFile);
        return "";
    }

    @RequestMapping(value = "upload/get/image.json")
    public String getImage(HttpServletRequest request, HttpServletResponse response,String url){
        if(StringUtils.isBlank(url)){
            return "";
        }
        String path = uploadFilePath+url;
        response.setContentType("/image" + url.substring(url.lastIndexOf(".")));
        response.setHeader("Cache-Control", "no-store");
        response.setHeader("Pragma", "no-cache");
        response.setDateHeader("Expires", 0);

        byte[] buf;
        try {
            buf = FileUtils.readFileToByteArray(new File(path));
            //写图片
            OutputStream outputStream=new BufferedOutputStream(response.getOutputStream());
            outputStream.write(buf);
            outputStream.flush();
            outputStream.close();
        } catch (IOException e) {
        }
        return "";
    }

    @RequestMapping(value = "b/uploadfile/delete.json",method = {RequestMethod.POST})
    public String delete(String uploadFileId, Model model){
//        uploadFileService.deleteById(uploadFileId);
        if(RunBinder.hasErrors()){
            model.addAttribute(ResultConst.MESSAGE,RunBinder.getStrErrors());
            model.addAttribute(ResultConst.SUCCESS,false);
        }else {
            model.addAttribute(ResultConst.SUCCESS,true);
        }
        return "";
    }
}
