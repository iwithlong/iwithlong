package com.iwithlong.controller;

import com.imesne.assistant.common.page.PageList;
import com.imesne.assistant.common.runtime.RunBinder;
import com.iwithlong.context.ResultConst;
import com.iwithlong.entity.ProductSms;
import com.iwithlong.service.ProductSmsService;
import com.iwithlong.vo.ProductSmsVo;
import com.ktanx.platform.admin.controller.AdminBaseController;
import com.ktanx.platform.web.Page;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributesModelMap;

import javax.annotation.Resource;

/**
 * Created by mhy on 2018/6/1.
 */
@Controller
public class ProductSmsController extends AdminBaseController {
    @Resource
    private ProductSmsService productSmsService;

    /**
     * 新增群发短信
     * @param productSmsVo
     * @param modelMap
     * @param redirectAttributesModelMap
     * @return
     */
    @RequestMapping("b/productsms/add")
    public String add(ProductSmsVo productSmsVo,ModelMap modelMap,RedirectAttributesModelMap redirectAttributesModelMap){
        productSmsService.addProductSms(productSmsVo);
        if(RunBinder.hasErrors()){
            this.addErrorMsg(modelMap,RunBinder.getStrErrors());
            modelMap.addAttribute("productSmsVo",productSmsVo);
            return "productSms/product-sms-send";
        }else {
            this.addSuccessMsg(redirectAttributesModelMap,"提交群发短信申请成功");
            redirectAttributesModelMap.addAttribute("getSession",true);
            return "redirect:/b/activityenroll/detail/list";
        }
    }

    /**
     * 短信审核列表
     * @param productSmsVo
     * @param modelMap
     * @return
     */
    @RequestMapping("b/productsms/audit/list")
    public String auditProductSmsList(ProductSmsVo productSmsVo,ModelMap modelMap){
        productSmsVo.setStatus(ProductSms.Status.UNAUDIT.getCode());
        PageList<ProductSmsVo> productSmsVos =  productSmsService.pageList(productSmsVo);
        modelMap.addAttribute("productSmsVo",productSmsVo);
        modelMap.addAttribute(ResultConst.RESULT,productSmsVos);
        modelMap.addAttribute("page", Page.wrap(productSmsVos));
        return "productSms/product-sms-audit-list";
    }

    /**
     * 审核页面
     * @param productSmsVo
     * @param model
     * @return
     */
    @RequestMapping("b/productsms/audit/audit")
    public String auditView(ProductSmsVo productSmsVo,Model model){
        model.addAttribute("productSmsVo",productSmsService.findById(productSmsVo.getProductSmsId()));
        return "productSms/product-sms-audit-audit";
    }

    @RequestMapping("b/productsms/audit/pass")
    public String auditPass(ProductSmsVo productSmsVo,ModelMap modelMap,RedirectAttributesModelMap redirectAttributesModelMap){
        productSmsService.pass(productSmsVo);
        if(RunBinder.hasErrors()){
            this.addErrorMsg(redirectAttributesModelMap, RunBinder.getStrErrors());
        }else {
            this.addSuccessMsg(redirectAttributesModelMap, "审核通过并成功发送短信");
        }
        return "redirect:list";
    }

    @RequestMapping("b/productsms/audit/notpass")
    public String auditNotPass(ProductSmsVo productSmsVo,RedirectAttributesModelMap redirectAttributesModelMap){
        productSmsService.notPass(productSmsVo);
        if(RunBinder.hasErrors()){
            this.addErrorMsg(redirectAttributesModelMap,RunBinder.getStrErrors());
        }else {
            this.addSuccessMsg(redirectAttributesModelMap,"审核驳回成功");
        }
        return "redirect:list";
    }

    /**
     * 短信列表
     * @param productSmsVo
     * @param modelMap
     * @return
     */
    @RequestMapping("b/productsms/list")
    public String productSmsList(ProductSmsVo productSmsVo,ModelMap modelMap){
        PageList<ProductSmsVo> productSmsVos =  productSmsService.pageList(productSmsVo);
        modelMap.addAttribute("productSmsVo",productSmsVo);
        modelMap.addAttribute(ResultConst.RESULT,productSmsVos);
        modelMap.addAttribute("page", Page.wrap(productSmsVos));
        return "productSms/product-sms-list";
    }
}
