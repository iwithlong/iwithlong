package com.iwithlong.controller;

import com.imesne.assistant.common.page.PageList;
import com.iwithlong.entity.Advert;
import com.iwithlong.service.AdvertService;
import com.iwithlong.vo.AdvertVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("advert")
public class FrontAdvertController {

    @Autowired
    private AdvertService advertService;

    /**
     * 首页广告列表
     *
     * @param modelMap
     * @return
     */
    @RequestMapping("index/list.json")
    public String indexList(Advert advert, ModelMap modelMap) {

        advert.setStatus(Advert.Status.RELEASE.getCode());
        PageList<AdvertVo> pageList = advertService.queryPageList(advert);
        modelMap.put("list", pageList);

        return "json";
    }


}
