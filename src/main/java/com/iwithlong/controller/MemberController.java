package com.iwithlong.controller;

import com.imesne.assistant.common.bean.BeanKit;
import com.iwithlong.context.Constants;
import com.iwithlong.context.ResultConst;
import com.iwithlong.entity.Member;
import com.iwithlong.enums.MemberStatus;
import com.iwithlong.enums.Rank;
import com.iwithlong.service.MemberGrowthService;
import com.iwithlong.service.MemberService;
import com.iwithlong.utils.EnumUtils;
import com.iwithlong.utils.ModelUtils;
import com.iwithlong.vo.MemberVo;
import com.ktanx.platform.admin.controller.AdminBaseController;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

/**
 * 会员管理
 * User: TJM
 * Date: 2017/10/10 18:47
 * version $Id: MemberController.java, v 0.1  18:47 Exp $
 */
@Controller
public class MemberController extends AdminBaseController {

    @Resource
    private MemberService memberService;


    @Resource
    private MemberGrowthService memberGrowthService;

    @RequestMapping("/a/member/list")
    public String list(MemberVo vo, Model model) {

        model.addAttribute("memberList", memberService.pageList(vo));
        model.addAttribute("ranks", EnumUtils.getEnumMap(Rank.class));
        model.addAttribute("memberStatus", MemberStatus.values());
        return "member/member-list";
    }

    @RequestMapping("/a/member/enable.json")
    public String enable(Long id, Model model) {
        memberService.enable(id);
        processModel(model, "启用成功");
        return "";
    }


    @RequestMapping("/a/member/disable.json")
    public String disable(Long id, Model model) {
        memberService.disable(id);
        processModel(model, "禁用成功");
        return "";
    }

    @RequestMapping("/a/member/delete.json")
    public String delete(Long id, Model model) {
        memberService.delete(id);
        processModel(model, "删除成功");
        return "";
    }

    @RequestMapping("/a/member/detail")
    public String detail(Long id, Model model) {
        Member member = memberService.get(id);
        MemberVo convert = BeanKit.convert(new MemberVo(), member);
        model.addAttribute("memberDetail", convert);
        model.addAttribute("ranks", EnumUtils.getEnumMap(Rank.class));
        return "member/member-detail";
    }

    private void processModel(Model model, String successMsg) {
        ModelUtils.processModel(model, successMsg);
    }


    /**
     * 登录接口
     *
     * @param session
     * @param model
     * @return
     */
    @PostMapping({"/weixin/logon", "/weixin/logon.json"})
    public String logon(MemberVo vo, HttpSession session, Model model) {

        MemberVo member = memberService.login(vo.getWeixin());
        if (member != null) {
            model.addAttribute(ResultConst.RESULT, member);
            session.setAttribute(Constants.LOGIN_MEMBER, member);
            model.addAttribute("sessionId", session.getId());
        }

        processModel(model, "登录成功");

        return "";
    }


    /**
     * 会员更新
     *
     * @param session
     * @param model
     * @return
     */
    @PostMapping({"/member/update", "/member/update.json"})
    public String update(MemberVo vo, HttpSession session, Model model) {

        memberService.updateMember(vo);

        processModel(model, "更新成功");

        return "";
    }

    /**
     * 获取用户信息接口
     * @param vo
     * @param model
     * @return
     */
    @PostMapping({"/member/get.json"})
    public String get(MemberVo vo,Model model){
        model.addAttribute(ResultConst.RESULT,memberService.findByWeixin(vo.getWeixin()));
        return "";
    }


}
