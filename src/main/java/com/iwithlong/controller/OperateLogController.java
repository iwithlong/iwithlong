package com.iwithlong.controller;

import com.iwithlong.context.ResultConst;
import com.iwithlong.log.LogLevel;
import com.iwithlong.log.LogOperatorType;
import com.iwithlong.service.OperateLogService;
import com.iwithlong.utils.EnumUtils;
import com.iwithlong.vo.OperateLogVo;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;

/**
 * User : liulu
 * Date : 2017-09-22 15:41
 * version $Id: OprateLogController.java, v 0.1 Exp $
 */
@Controller
@RequestMapping("/log")
public class OperateLogController {

    @Resource
    private OperateLogService logService;

    @RequestMapping("/list")
    public String list(OperateLogVo logVo, Model model) {

        model.addAttribute(ResultConst.RESULT, logService.pageList(logVo));
        model.addAttribute("operatorTypes", EnumUtils.getEnumMap(LogOperatorType.class));

        return "log/list";
    }

    @RequestMapping("/detail")
    public String detail(Long operateLogId, Model model) {

        model.addAttribute(ResultConst.RESULT, logService.get(operateLogId));
        model.addAttribute("operatorTypes", EnumUtils.getEnumMap(LogOperatorType.class));
        model.addAttribute("logLevels", EnumUtils.getEnumMap(LogLevel.class));
        return "log/detail";
    }

}
