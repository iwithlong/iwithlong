package com.iwithlong.entity;

import com.imesne.assistant.common.page.Pageable;
import com.imesne.assistant.jdbc.annotation.Table;
import com.iwithlong.context.Constants;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * 会员成长变动记录
 * <p>
 * User: TJM
 * Date: 2017/8/25 10:56
 * version $Id: MemberGrowthChangeRecord.java, v 0.1  10:56 Exp $
 */
@Getter
@Setter
@Table(name = Constants.TABLE_PRE + "Member_Growth_Change_Record")
public class MemberGrowthChangeRecord extends Pageable {
	private static final long serialVersionUID = -5444079543785238921L;

	/**
	 * 会员成长变动记录ID
	 */
	private Long memberGrowthChangeRecordId;

	/**
	 * 会员成长值ID
	 */
	private Long memberGrowthAccountId;

	/**
	 * 变动值
	 */
	private Integer changeValue;

	/**
	 * 摘要
	 */
	private String memo;

	/**
	 * 记录时间
	 */
	private Date gmtRecord;


}
