package com.iwithlong.entity;

import com.imesne.assistant.common.page.Pageable;
import com.imesne.assistant.jdbc.annotation.Table;
import com.iwithlong.context.Constants;
import lombok.Data;

/**
 * Created by mhy on 2018/4/17.
 * 商户
 */
@Data
@Table(name = Constants.TABLE_PRE +"member",pkField = "memberId")
public class Merchant extends Pageable{

    /**
     * 会员用户ID
     */
    private Long memberId;

    /**
     * 帐号
     */
    private String memberCode;

    /**
     * 密码
     */
    private String memberPassword;

    /**
     * 姓名
     */
    private String memberName;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 用户状态：
     01,启用
     02,禁用
     */
    private String status;

    /**
     * 头像  URL
     */
    private String head;

    /**
     * 昵称
     */
    private String nickName;

    /**
     * 会员类型
     */
    private String memberType;

    /**
     * 商户电话
     */
    private String serviceTel;

    /**
     * 账户类型
     */
    private String accountType;

    /**
     * 账户号
     */
    private String accountCode;

    /**
     * 账户名称
     */
    private String accountName;

    /**
     * 商户简介
     */
    private String intro;

    /**
     * 是否启用
     */
    private boolean enable;
}
