package com.iwithlong.entity;

import com.imesne.assistant.jdbc.annotation.Table;
import com.iwithlong.context.Constants;
import lombok.Data;

import java.math.BigDecimal;

/**
 *客户出勤率
 * Created by mhy on 2018/4/17.
 */
@Data
@Table(name = Constants.TABLE_PRE +"customer_attendance")
public class CustomerAttendance {
    private long customerAttendanceId;
    private String mobile;
    private String name;
    private BigDecimal percent;
}
