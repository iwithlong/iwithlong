package com.iwithlong.entity;

import com.imesne.assistant.jdbc.annotation.Table;
import com.iwithlong.context.Constants;
import lombok.Data;

/**
 * Created by mhy on 2018/5/31.
 */
@Data
@Table(name = Constants.TABLE_PRE +"namelist_sms_rela")
public class NamelistSmsRela {
    private Long enrollNamelistId;
    private Long productSmsId;
}
