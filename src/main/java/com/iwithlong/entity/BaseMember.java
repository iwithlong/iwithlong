package com.iwithlong.entity;

import com.imesne.assistant.common.page.Pageable;
import lombok.Getter;
import lombok.Setter;


/**
 * Created by mhy on 2018/4/17.
 */
@Getter
@Setter
public class BaseMember extends Pageable {
    private static final long serialVersionUID = 3252779906562675472L;

    /**
     * 会员用户ID
     */
    private Long memberId;

    /**
     * 帐号
     */
    private String memberCode;

    /**
     * 密码
     */
    private String memberPassword;

    /**
     * 姓名
     */
    private String memberName;

    /**
     * 邮箱
     */
    private String email;

    /**
     * 用户状态：
     01,启用
     02,禁用
     */
    private String status;

    /**
     * 头像  URL
     */
    private String head;

    /**
     * 昵称
     */
    private String nickName;

    /**
     * 会员类型
     */
    private String memberType;
}
