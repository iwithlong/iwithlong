package com.iwithlong.entity;

import com.imesne.assistant.common.enums.IEnum;
import com.imesne.assistant.common.page.Pageable;
import com.imesne.assistant.jdbc.annotation.Table;
import com.iwithlong.context.Constants;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@Table(name = Constants.TABLE_PRE + "advert")
public class Advert extends Pageable {

    private Long advertId;

    private Long bizId;

    private String bizType;

    private String advertType;

    private String title;

    private String status;

    private String image;

    private Integer orderNo;

    private String creator;

    private Date gmtCreate;

    private String issuer;

    private Date gmtIssue;

    private String description;

    @Getter
    @AllArgsConstructor
    public enum Status implements IEnum {

        NORMAL("1", "未发布"),

        RELEASE("2", "已发布");

        private String code;

        private String desc;

    }

    @Getter
    @AllArgsConstructor
    public enum BizType implements IEnum {

        ACTIVITY("1", "活动"),
        ACTIVITYAREA("2","活动点"),
        PUBLIC("3","公众号"),
        ARTICLE("4","文章");

        private String code;

        private String desc;

    }

    @Getter
    @AllArgsConstructor
    public enum AdvertType implements IEnum {

        ADVERT("1", "广告位"),

        HOT_ENTER("2", "热门报名"),

        WONDERFUL_ACTIVITY("3", "精彩活动");

        private String code;

        private String desc;
    }
}
