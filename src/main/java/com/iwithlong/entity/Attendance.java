package com.iwithlong.entity;

import com.imesne.assistant.jdbc.annotation.Table;
import com.iwithlong.context.Constants;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * Created by mhy on 2018/5/30.
 */
@Getter
@Setter
@Table(name = Constants.TABLE_PRE + "attendance")
public class Attendance {
    /**
     * ID
     */
    private Long attendanceId;
    /**
     * 微信号
     */
    private String weixin;
    /**
     * 到场次数
     */
    private int attendTime;

    /**
     * 报名总次数
     */
    private int totalTime;
    /**
     * 出勤率
     */
    private double attendance;
    /**
     * 修改时间
     */
    private Date gmtModify;

}
