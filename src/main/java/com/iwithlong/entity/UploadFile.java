package com.iwithlong.entity;

import com.imesne.assistant.jdbc.annotation.Table;
import com.iwithlong.context.Constants;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.util.Date;

@Getter
@Setter
@Table(name = Constants.TABLE_PRE + "upload_file")
public class UploadFile implements Serializable{

  /**
   * 上传文件ID
   */
  private Long uploadFileId;

  /**
   * 业务文件ID
   */
  private Long bizUploadFileId;

  /**
   * 文件名
   */
  private String fileName;

  /**
   * 文件路径：采用相对路径
   */
  private String fileUrl;

  /**
   * 上传时间
   */
  private Date gmtUpload;

  /**
   * 序顺号
   */
  private Long ord;

}
