package com.iwithlong.entity;

import com.imesne.assistant.jdbc.annotation.Table;
import com.iwithlong.context.Constants;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 关键字实体
 * <p>
 * User: TJM
 * Date: 2017/9/25 15:06
 * version $Id: SearchKey.java, v 0.1  15:06 Exp $
 */
@Data
@Table(name = Constants.TABLE_PRE + "search_key")
public class SearchKey implements Serializable {
	private static final long serialVersionUID = -8231214615710274681L;

	/**
	 * 查询关键字ID
	 */
	private Long searchKeyId;

	/**
	 * 关键字
	 */
	private String keyword;

	/**
	 * 查询时间
	 */
	private Date gmtSearch;
}
