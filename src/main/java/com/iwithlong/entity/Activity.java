package com.iwithlong.entity;

import com.imesne.assistant.jdbc.annotation.Table;
import com.iwithlong.context.Constants;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
@Table(name = Constants.TABLE_PRE + "activity")
public class Activity implements Serializable{

  /**
   * 活动ID
   */
  private Long activityId;

  /**
   * 活动点ID
   */
  private Long activityAreaId;

  /**
   * 名称
   */
  private String name;

  /**
   * 简述
   */
  private String brief;

  /**
   * 详情
   */
  private String content;

  /**
   * 阅数量
   */
  private Long numberOfClick;

  /**
   * 兴趣量
   */
  private Long numberOfInterest;

  /**
   * 联系电话
   */
  private String telephone;

  /**
   *  购票地址
   */
  private String ticketUrl;

  /**
   * 收费情况
   */
  private String charging;

  /**
   * 推荐度：取值0~5
   */
  private Long recommended;

  /**
   * 开始时间
   */
  private Date beginDate;

  /**
   * 结束时间
   */
  private Date endDate;

  /**
   * 创建人
   */
  private String creator;

  /**
   * 创建时间
   */
  private Date gmtCreate;

  /**
   * 状态
   */
  private String status;

  /**
   * 修改人
   */
  private String modifier;

  /**
   * 修改时间
   */
  private Date gmtModify;

  /**
   * 退回理由
   */
  private String reason;

  /**
   * 审核人
   */
  private String auditor;

  /**
   * 审核时间
   */
  private Date gmtAudit;

  /**
   * 发布人
   */
  private String issuer;

  /**
   * 发布时间
   */
  private Date gmtIssue;

  /**
   * 标签
   */
  private String activityLabel;

  /**
   * 是否启用
   */
  private boolean enable;

}
