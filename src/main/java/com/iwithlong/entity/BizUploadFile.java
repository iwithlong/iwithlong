package com.iwithlong.entity;

import com.imesne.assistant.jdbc.annotation.Table;
import com.iwithlong.context.Constants;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Table(name = Constants.TABLE_PRE + "biz_upload_file")
public class BizUploadFile {

  /**
   * 业务文件ID
   */
  private Long bizUploadFileId;

  /**
   * 业务类型
   */
  private String bizType;

  /**
   * 业务值
   */
  private Long bizId;
}
