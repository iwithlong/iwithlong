package com.iwithlong.entity;

import com.imesne.assistant.jdbc.annotation.Table;
import com.iwithlong.context.Constants;
import lombok.Data;

import java.sql.Timestamp;

/**
 *
 * Created by mhy on 2018/4/17.
 */
@Data
@Table(name = Constants.TABLE_PRE +"cash_out")
public class CashOut {
    private long cashOutId;
    private long amount;
    private Timestamp gmtApply;
    private String status;
    private Timestamp gmtAudit;
    private String auditor;
    private String reason;

}
