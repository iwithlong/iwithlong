package com.iwithlong.entity;

import com.imesne.assistant.jdbc.annotation.Table;
import com.iwithlong.context.Constants;
import com.iwithlong.vo.ActivityAreaVo;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

@Data
@Table(name = Constants.TABLE_PRE + "activity_area")
public class ActivityArea implements Serializable{

  /**
   * 活动点ID
   */
  private Long activityAreaId;

  /**
   * 活动点名称
   */
  private String areaName;

  /**
   * 简述
   */
  private String brief;

  /**
   * 活动点介绍
   */
  private String areaIntroduction;

  /**
   * 省份
   */
  private String province;

  /**
   * 城市
   */
  private String city;

  /**
   * 区、镇
   */
  private String district;

  /**
   * 纬度值
   */
  private Double latitude;

  /**
   * 经度值
   */
  private Double longitude;

  /**
   * 地址
   */
  private String address;

  /**
   * 创建人
   */
  private String creator;

  /**
   * 创建时间
   */
  private Date gmtCreate;

  /**
   * 修改人
   */
  private String modifier;

  /**
   * 修改时间
   */
  private Date gmtModify;

  /**
   * 状态
   */
  private String status;

  /**
   * 退回理由
   */
  private String reason;

  /**
   * 审核人
   */
  private String auditor;

  /**
   * 审核时间
   */
  private Date gmtAudit;

  /**
   * 发布人
   */
  private String issuer;

  /**
   * 发布时间
   */
  private Date gmtIssue;

  /**
   * 是否启用
   */
  private boolean enable;

}
