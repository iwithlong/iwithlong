package com.iwithlong.entity;

import com.imesne.assistant.jdbc.annotation.Table;
import com.iwithlong.context.Constants;
import lombok.Data;

import java.io.Serializable;

/**
 * 标签实体
 * <p>
 * User: TJM
 * Date: 2017/9/25 14:57
 * version $Id: Label.java, v 0.1  14:57 Exp $
 */
@Data
@Table(name = Constants.TABLE_PRE + "label")
public class Label implements Serializable {
	private static final long serialVersionUID = 7445950913468069687L;


	/**
	 * 标签ID
	 */
	private Long labelId;

	/**
	 * 标签名
	 */
	private String labelName;

	/**
	 * 标签类型
	 */
	private String labelType;

	/**
	 * 序顺号
	 */
	private Integer ord;
}
