package com.iwithlong.entity;

import com.imesne.assistant.jdbc.annotation.Table;
import com.iwithlong.context.Constants;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 会员成长值
 * <p>
 * User: TJM
 * Date: 2017/8/25 10:49
 * version $Id: MemberGrowthAccount.java, v 0.1  10:49 Exp $
 */

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Table(name = Constants.TABLE_PRE + "Member_Growth_Account")
public class MemberGrowthAccount {


	private static final long serialVersionUID = 300186642708427824L;


	/**
	 * 会员成长值ID
 	 */
	private Long memberGrowthAccountId;


	/**
	 * 会员用户ID
	 */
	private Long memberId;

	/**
	 * 当前值
	 */
	private Integer balance;

}
