package com.iwithlong.entity;

import com.imesne.assistant.jdbc.annotation.Table;
import com.iwithlong.context.Constants;
import lombok.Data;

/**
 * 报名销售产品
 * Created by mhy on 2018/5/10.
 */
@Data
@Table(name = Constants.TABLE_PRE + "enroll_product")
public class EnrollProduct {
    private Long enrollProductId;

    private Long activityEnrollId;

    /**
     * 名称
     */
    private String name;

    /**
     * 数量
     */
    private int quantity;

    /**
     * 摘要
     */
    private String memo;


    /**
     * 顺序号
     */
    private int ord;
}
