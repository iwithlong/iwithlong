package com.iwithlong.entity;

import com.imesne.assistant.jdbc.annotation.Table;
import com.iwithlong.context.Constants;
import lombok.Data;

import java.util.Date;

/**
 * 报名名单
 * Created by mhy on 2018/5/10.
 */
@Data
@Table(name = Constants.TABLE_PRE + "enroll_namelist")
public class EnrollNamelist {
    private Long enrollNamelistId;
    private Long enrollProductId;

    /**
     * 名称
     */
    private String name;

    /**
     * 联系电话
     */
    private String mobile;

    /**
     * 备注
     */
    private String memo;

    /**
     * 状态
     */
    private String status;

    /**
     * 数量
     */
    private int quantity;

    /**
     * 报名时间
     */
    private Date gmtEnroll;

    /**
     * 微信
     */
    private String weixin;

}
