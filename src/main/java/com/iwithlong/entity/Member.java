package com.iwithlong.entity;

import com.imesne.assistant.common.page.Pageable;
import com.imesne.assistant.jdbc.annotation.Table;
import com.iwithlong.context.Constants;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * 会员用户
 * Member : liulu
 * Date : 2017/6/3 15:31
 * version $Id: Member.java, v 0.1 Exp $
 */

@Getter
@Setter
@Table (name = Constants.TABLE_PRE + "member")
public class Member extends Pageable {
	private static final long serialVersionUID = 3252779906562675472L;

	/**
	 * 会员用户ID
	 */
	private Long memberId;

	/**
	 * 帐号
	 */
	private String memberCode;

	/**
	 * 密码
	 */
	private String memberPassword;

	/**
	 * 姓名
	 */
	private String memberName;

	/**
	 * 邮箱
	 */
	private String email;

	/**
	 * 用户状态：
	 01,启用
	 02,禁用
	 */
	private String status;

	/**
	 * 头像  URL
	 */
	private String head;

	/**
	 * 昵称
	 */
	private String nickName;

	/**
	 * 会员类型
	 */
	private String memberType;

	/**
	 * 手机号
	 */
	private String mobile;

	/**
	 * 性别
	 */
	private Boolean sex;

	/**
	 * 出生日期
	 */
	private Date birthday;

	/**
	 * 会员等级
	 */
	private String rank;

	/**
	 *微信帐号
	 */
	private String weixin;

}
