package com.iwithlong.entity;

import com.imesne.assistant.jdbc.annotation.Table;
import com.iwithlong.context.Constants;
import lombok.Data;

import java.util.Date;

/**
 * Created by xiexiaohan on 2017/10/18.
 */
@Data
@Table(name = Constants.TABLE_PRE + "interest")
public class Interest {
    /**
     * 兴趣ID
     */
    private long interestId;

    /**
     * 活动ID
     */
    private long activityId;

    /**
     * 拥有者
     */
    private String owner;

    /**
     * 创建时间
     */
    private Date gmtCreate;
}
