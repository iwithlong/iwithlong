package com.iwithlong.entity;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * 行政区域
 * <p>
 * User: TJM
 * Date: 2017/10/12 9:50
 * version $Id: Area.java, v 0.1  9:50 Exp $
 */
@Data
public class Area {

	/**
	 * 行政编码
	 */
	private String code ;

	/**
	 * 名称
	 */
	private String name;

	/**
	 * 行政级别 0:省/直辖市 1:地级市 2:县级市
	 */
	private int level;

	/**
	 * 上一级的行政区划代码
	 */
	private String parentCode;


	private List<Area> childrenList;

	public Area() {
		super();
	}

	public Area(String code, String name, int level) {
		super();
		this.code = code;
		this.name = name;
		this.level = level;
	}

	public Area(String code, String name, int level, String parentCode) {
		super();
		this.code = code;
		this.name = name;
		this.level = level;
		this.parentCode = parentCode;
	}

	public void addChildren (Area children) {
		if(childrenList==null){
			childrenList=new ArrayList<> ();
		}
		childrenList.add (children);
		children.setParentCode (this.getCode ());
	}
}
