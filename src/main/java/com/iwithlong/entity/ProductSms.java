package com.iwithlong.entity;

import com.imesne.assistant.common.enums.IEnum;
import com.imesne.assistant.jdbc.annotation.Table;
import com.iwithlong.context.Constants;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;

import java.util.Date;

/**
 * Created by mhy on 2018/5/31.
 */
@Data
@Table(name = Constants.TABLE_PRE +"product_sms")
public class ProductSms {
    /**
     * 产品短信ID
     */
    private Long productSmsId;

    /**
     * 活动报名ID
     */
    private Long activityEnrollId;

    /**
     * 详情
     */
    private String content;

    /**
     * 标题
     */
    private String title;

    /**
     * 数量
     */
    private int quantity;

    /**
     * 账号
     */
    private String memberCode;

    /**
     * 状态
     */
    private String status;

    /**
     * 创建时间
     */
    private Date gmtCreate;

    /**
     * 审核时间
     */
    private Date gmtAudit;

    /**
     * 审核人
     */
    private String auditor;

    /**
     * 发送时间
     */
    private Date gmtSend;

    /**
     * 驳回原因
     */
    private String reason;

    @Getter
    @AllArgsConstructor
    public enum Status implements IEnum {

        UNAUDIT("1", "未审核"),

        AUDIT("2", "审核通过"),

        REFUSED("3","审核拒绝");

        private String code;

        private String desc;

    }
}
