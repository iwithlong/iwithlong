package com.iwithlong.entity;

import com.imesne.assistant.jdbc.annotation.Table;
import com.iwithlong.context.Constants;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * User : liulu
 * Date : 2017/6/3 15:35
 * version $Id: OperateLog.java, v 0.1 Exp $
 */
@Data
@Table(name = Constants.TABLE_PRE + "operate_log")
public class OperateLog implements Serializable {

    private static final long serialVersionUID = 1885283432842929925L;

    /**
     * 操作日志ID
     */
    private Long operateLogId;

    /**
     * 操作人
     */
    private String operator;

    /**
     * 姓名
     */
    private String operatorName;

    /**
     * 操作类型
     */
    private String operatorType;

    /**
     * 记录时间
     */
    private Date logTime;

    /**
     * 日志等级
     */
    private String logLevel;

    /**
     * url
     */
    private String url;

    /**
     * 日志详情
     */
    private String logContent;

    /**
     * 系统编码
     */
    private String systemCode;

    /**
     * 系统名称
     */
    private String systemName;

    /**
     * 模块编码
     */
    private String moduleCode;

    /**
     * 模块名称
     */
    private String moduleName;

    /**
     * 客户端IP
     */
    private String clientIp;

    /**
     * 客户端浏览器
     */
    private String clientBrowser;

    /**
     * 客户端所在地
     */
    private String clientLocation;

    /**
     * 客户端操作系统
     */
    private String clientOs;


}
