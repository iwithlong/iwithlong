package com.iwithlong.entity;

import com.imesne.assistant.jdbc.annotation.Table;
import com.iwithlong.context.Constants;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

/**
 * Created by mhy on 2018/5/1.
 */
@Getter
@Setter
@Table(name = Constants.TABLE_PRE + "activity_enroll")
public class ActivityEnroll {
    /**
     * 报名单ID
     */
    private Long activityEnrollId;

    /**
     * 活动ID
     */
    private Long activityId;

    /**
     * 商家ID
     */
    private Long memberId;

    /**
     * 标题
     */
    private String title;

    /**
     * 地址
     */
    private String address;

    /**
     * 开始时间
     */
    private Date gmtBegin;

    /**
     * 结束时间
     */
    private Date gmtEnd;

    /**
     * 备注
     */
    private String memo;

    /**
     * 状态
     */
    private String status;

    /**
     * 创建时间
     */
    private Date gmtCreate;

    /**
     * 创建人
     */
    private String creator;

    /**
     * 活动开始时间
     */
    private Date gmtActivityStart;

    /**
     * 活动结束时间
     */
    private Date gmtActivityEnd;
}
