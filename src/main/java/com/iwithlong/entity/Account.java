package com.iwithlong.entity;

import com.imesne.assistant.jdbc.annotation.Table;
import com.iwithlong.context.Constants;
import lombok.Data;

/**
 * Created by mhy on 2018/4/17.
 */
@Data
@Table(name = Constants.TABLE_PRE +"account")
public class Account {

    private long accountId;
    private long amount;
    private long freeze;
}
