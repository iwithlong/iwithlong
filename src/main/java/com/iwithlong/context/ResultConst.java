package com.iwithlong.context;

/**
 * User : liulu
 * Date : 2017/6/6 18:51
 * version $Id: ResultConst.java, v 0.1 Exp $
 */
public interface ResultConst {

    String RESULT = "result";

    String MESSAGE = "message";

    String SUCCESS = "success";


}
