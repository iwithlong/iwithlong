package com.iwithlong.context;

/**
 * User : liulu
 * Date : 2017-09-22 15:20
 * version $Id: Constants.java, v 0.1 Exp $
 */
public class Constants {


    public static final String TABLE_PRE = "iwl_";

    public static final String PASSWORD_PREFIX = "*(^&S!^%$*&^%#!~*&";

    public static final String LOGIN_MEMBER = "LOGIN_MEMBER";

    public static final String LOGIN_MERCHANT = "LOGIN_MERCHANT";

    public static final String HOT_KEYWORDS="hot_keywords";

    public static final String NUMBER_OF_CLICK_COEFFICIENT="numberOfClick_coefficient";

    public static final String NUMBER_OF_INTEREST_COEFFICIENT="numberOfInterest_coefficient";

    public static final String RECOMMENDED_COEFFICIENT="recommended_coefficient";


    public static final String VERIFICATION_CODE = "VERIFICATION_CODE";

}
