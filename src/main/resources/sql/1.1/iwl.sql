ALTER TABLE iwl_member ADD COLUMN ACCOUNT_TYPE varchar(32) comment '账户类型';
ALTER TABLE iwl_member ADD COLUMN ACCOUNT_CODE varchar(32) comment '账号';
ALTER TABLE iwl_member ADD COLUMN ACCOUNT_NAME varchar(64) comment '户名';
ALTER TABLE iwl_member ADD COLUMN INTRO varchar(4000) comment '商家简介';
ALTER TABLE iwl_member ADD COLUMN SERVICE_TEL varchar(20) comment '客户电话';
ALTER TABLE iwl_member ADD COLUMN MEMBER_TYPE varchar(32) comment '会员类型';
ALTER TABLE iwl_member ADD COLUMN ENABLE BOOLEAN comment '是否启用';





drop table if exists iwl_activity_enroll;

/*==============================================================*/
/* Table: iwl_activity_enroll                                   */
/*==============================================================*/
create table iwl_activity_enroll
(
   ACTIVITY_ENROLL_ID   BIGINT NOT NULL AUTO_INCREMENT COMMENT '活动报名ID',
   MEMBER_ID            BIGINT COMMENT '商家ID',
   ACTIVITY_ID          BIGINT COMMENT '活动ID',
   TITLE                VARCHAR(256) COMMENT '标题',
   ADDRESS              VARCHAR(256) COMMENT '地址',
   GMT_BEGIN            DATETIME COMMENT '开始时间',
   GMT_END              DATETIME COMMENT '结束时间',
   MEMO                 VARCHAR(4000) COMMENT '摘要',
   STATUS               VARCHAR(32) COMMENT '状态',
   GMT_ACTIVITY_START   DATETIME COMMENT '活动开始时间',
   GMT_ACTIVITY_END     DATETIME COMMENT '活动结束时间',
   GMT_CREATE           DATETIME COMMENT '创建时间',
   CREATOR              VARCHAR(64) COMMENT '创建人',
   PRIMARY KEY (ACTIVITY_ENROLL_ID)
);



alter table iwl_activity_enroll add constraint FK_PF_ACTIVITY_ENROLL foreign key (ACTIVITY_ID)
references iwl_activity (ACTIVITY_ID) on delete restrict on update restrict;

alter table iwl_activity_enroll add constraint FK_PF_A_MEMBER_ENROLL foreign key (MEMBER_ID)
references iwl_member (MEMBER_ID) on delete restrict on update restrict;



drop table if exists iwl_enroll_product;

/*==============================================================*/
/* Table: iwl_enroll_product                                    */
/*==============================================================*/
create table iwl_enroll_product
(
   ENROLL_PRODUCT_ID    BIGINT NOT NULL AUTO_INCREMENT COMMENT '报名销售产品ID',
   ACTIVITY_ENROLL_ID   BIGINT COMMENT '活动报名ID',
   NAME                 VARCHAR(64) COMMENT '名称',
   QUANTITY             INT COMMENT '数量',
   MEMO                 VARCHAR(4000) COMMENT '摘要',
   ORD                  INT COMMENT '序顺号',
   PRIMARY KEY (ENROLL_PRODUCT_ID)
);

alter table iwl_enroll_product add constraint FK_PF_A_E_PROD foreign key (ACTIVITY_ENROLL_ID)
references iwl_activity_enroll (ACTIVITY_ENROLL_ID) on delete restrict on update restrict;



drop table if exists iwl_enroll_namelist;

/*==============================================================*/
/* Table: iwl_enroll_namelist                                   */
/*==============================================================*/
create table iwl_enroll_namelist
(
   ENROLL_NAMELIST_ID   BIGINT NOT NULL AUTO_INCREMENT COMMENT '报名名单ID',
   ENROLL_PRODUCT_ID    BIGINT COMMENT '报名销售产品ID',
   WEIXIN               VARCHAR(64) COMMENT '微信帐号',
   NAME                 VARCHAR(64) COMMENT '名称',
   MOBILE               VARCHAR(20) COMMENT '手机号',
   MEMO                 VARCHAR(4000) COMMENT '摘要',
   STATUS               VARCHAR(32) COMMENT '状态',
   QUANTITY             INT COMMENT '数量',
   GMT_ENROLL           DATETIME COMMENT '报名时间',
   PRIMARY KEY (ENROLL_NAMELIST_ID)
);

alter table iwl_enroll_namelist add constraint FK_PF_A_PROD_NAME foreign key (ENROLL_PRODUCT_ID)
references iwl_enroll_product (ENROLL_PRODUCT_ID) on delete restrict on update restrict;




UPDATE iwl_member SET MEMBER_TYPE='member' WHERE MEMBER_TYPE = NULL;



CREATE TABLE `iwl_advert` (
	`advert_id` bigint NOT NULL AUTO_INCREMENT COMMENT '主键id',
	`biz_id` bigint NOT NULL COMMENT '业务id',
	`biz_type` varchar(32) NOT NULL COMMENT '业务类型',
	`advert_type` varchar(32) NOT NULL COMMENT '广告类型',
	`title` varchar(256) NOT NULL COMMENT '标题',
	`status` varchar(32) NOT NULL COMMENT '状态',
	`image` varchar(2000) NOT NULL COMMENT '图片',
	`order_no` int NOT NULL COMMENT '排序',
	`creator` varchar(64) NOT NULL COMMENT '创建人',
	`gmt_create` datetime NOT NULL COMMENT '创建时间',
	`issuer` VARCHAR (64) NULL COMMENT '发布人',
	`gmt_issue` datetime NULL COMMENT '发布时间',
	`description` VARCHAR(1024) NULL COMMENT '说明',
	PRIMARY KEY (`advert_id`)
) COMMENT='广告';


create table iwl_attendance
(
   ATTENDANCE_ID        BIGINT NOT NULL AUTO_INCREMENT COMMENT '出勤率ID',
   WEIXIN               VARCHAR(64) COMMENT '微信帐号',
   ATTEND_TIME          INT COMMENT '到场数',
   TOTAL_TIME           INT COMMENT '报名数',
   ATTENDANCE           Decimal(3,2) COMMENT '到场率',
   GMT_MODIFY           DATETIME COMMENT '更新时间',
   PRIMARY KEY (ATTENDANCE_ID)
);


create table iwl_product_sms
(
   PRODUCT_SMS_ID       BIGINT NOT NULL AUTO_INCREMENT COMMENT '活动短信ID',
   ACTIVITY_ENROLL_ID    BIGINT COMMENT '报名单ID',
   TITLE                VARCHAR(64) COMMENT '标题',
   CONTENT              TEXT COMMENT '详情',
   QUANTITY             INT COMMENT '数量',
   MEMBER_CODE          VARCHAR(64) COMMENT '帐号',
   STATUS               VARCHAR(32) COMMENT '状态',
   GMT_CREATE           DATETIME COMMENT '创建时间',
   GMT_AUDIT            DATETIME COMMENT '审核时间',
   AUDITOR              VARCHAR(64) COMMENT '审核人',
   GMT_SEND             DATETIME COMMENT '发送时间',
   REASON               VARCHAR(256) COMMENT '退回理由',
   PRIMARY KEY (PRODUCT_SMS_ID)
);

ALTER TABLE iwl_product_sms ADD CONSTRAINT FK_PF_PROD_SMS FOREIGN KEY (ACTIVITY_ENROLL_ID)
REFERENCES iwl_activity_enroll (ACTIVITY_ENROLL_ID) ON DELETE RESTRICT ON UPDATE RESTRICT;


create table iwl_namelist_sms_rela
(
   ENROLL_NAMELIST_ID   BIGINT NOT NULL COMMENT '报名名单ID',
   PRODUCT_SMS_ID       BIGINT NOT NULL COMMENT '活动短信ID',
   PRIMARY KEY (ENROLL_NAMELIST_ID, PRODUCT_SMS_ID)
);

ALTER TABLE iwl_namelist_sms_rela ADD CONSTRAINT FK_IWL_NAMELIST_SMS_RELA FOREIGN KEY (ENROLL_NAMELIST_ID)
REFERENCES iwl_enroll_namelist (ENROLL_NAMELIST_ID) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE iwl_namelist_sms_rela ADD CONSTRAINT FK_IWL_NAMELIST_SMS_RELA2 FOREIGN KEY (PRODUCT_SMS_ID)
REFERENCES iwl_product_sms (PRODUCT_SMS_ID) ON DELETE RESTRICT ON UPDATE RESTRICT;











