DROP TABLE IF EXISTS IWL_ACTIVITY;

DROP TABLE IF EXISTS IWL_ACTIVITY_AREA;

DROP TABLE IF EXISTS IWL_BIZ_UPLOAD_FILE;

DROP TABLE IF EXISTS IWL_INTEREST;

DROP TABLE IF EXISTS IWL_LABEL;

DROP TABLE IF EXISTS IWL_MEMBER;

DROP TABLE IF EXISTS IWL_MEMBER_GROWTH_ACCOUNT;

DROP TABLE IF EXISTS IWL_MEMBER_GROWTH_CHANGE_RECORD;

DROP TABLE IF EXISTS IWL_OPERATE_LOG;

DROP TABLE IF EXISTS IWL_SEARCH_KEY;

DROP TABLE IF EXISTS iwl_upload_file;

/*==============================================================*/
/* Table: IWL_ACTIVITY                                          */
/*==============================================================*/
CREATE TABLE iwl_activity
(
   ACTIVITY_ID          BIGINT NOT NULL AUTO_INCREMENT COMMENT '活动ID',
   ACTIVITY_AREA_ID     BIGINT COMMENT '活动点ID',
   NAME                 VARCHAR(64) COMMENT '名称',
   BRIEF                VARCHAR(4000) COMMENT '简述',
   CONTENT              TEXT COMMENT '详情',
   NUMBER_OF_CLICK      INT COMMENT '阅数量',
   NUMBER_OF_INTEREST   INT COMMENT '兴趣量',
   CHARGING             VARCHAR(32) COMMENT '收费情况',
   RECOMMENDED          INT COMMENT '推荐度：取值0~5',
   TELEPHONE            VARCHAR(32) COMMENT '活动联系电话',
   TICKET_URL           VARCHAR(1000) COMMENT '购票地址',
   BEGIN_DATE           DATETIME COMMENT '开始时间',
   END_DATE             DATETIME COMMENT '结束时间',
   CREATOR              VARCHAR(64) COMMENT '创建人',
   GMT_CREATE           DATETIME COMMENT '创建时间',
   STATUS               VARCHAR(32) COMMENT '用户状态：01,已注册   02,已认证 03,已禁用',
   MODIFIER             VARCHAR(64) COMMENT '修改人',
   GMT_MODIFY           DATETIME COMMENT '修改时间',
   REASON               VARCHAR(256) COMMENT '退回理由',
   AUDITOR              VARCHAR(64) COMMENT '审核人',
   GMT_AUDIT            DATETIME COMMENT '审核时间',
   ISSUER               VARCHAR(64) COMMENT '发布人',
   GMT_ISSUE            DATETIME COMMENT '发布时间',
   ACTIVITY_LABEL       VARCHAR(256) COMMENT '标签',
   ENABLE               bool COMMENT '是否启用',
   PRIMARY KEY (ACTIVITY_ID)
);

ALTER TABLE iwl_activity COMMENT '活动';

/*==============================================================*/
/* Table: IWL_ACTIVITY_AREA                                     */
/*==============================================================*/
CREATE TABLE iwl_activity_area
(
   ACTIVITY_AREA_ID     BIGINT NOT NULL AUTO_INCREMENT COMMENT '活动点ID',
   AREA_NAME            VARCHAR(64) COMMENT '活动点名称',
   BRIEF                VARCHAR(4000) COMMENT '简述',
   AREA_INTRODUCTION    TEXT COMMENT '活动点介绍',
   PROVINCE             VARCHAR(6) COMMENT '省份',
   CITY                 VARCHAR(6) COMMENT '城市',
   DISTRICT             VARCHAR(6) COMMENT '区、镇',
   LATITUDE             NUMERIC(15,12) COMMENT '纬度值',
   LONGITUDE            NUMERIC(15,12) COMMENT '经度值',
   ADDRESS              VARCHAR(256) COMMENT '地址',
   CREATOR              VARCHAR(64) COMMENT '创建人',
   GMT_CREATE           DATETIME COMMENT '创建时间',
   MODIFIER             VARCHAR(64) COMMENT '修改人',
   GMT_MODIFY           DATETIME COMMENT '修改时间',
   STATUS               VARCHAR(32) COMMENT '用户状态：01,已注册   02,已认证 03,已禁用',
   ENABLE               bool COMMENT '是否启用',
   AUDITOR              VARCHAR(64) COMMENT '审核人',
   GMT_AUDIT            DATETIME COMMENT '审核时间',
   REASON               VARCHAR(256) COMMENT '退回理由',
   GMT_AUDIT            DATETIME COMMENT '审核时间',
   ISSUER               VARCHAR(64) COMMENT '发布人',
   GMT_ISSUE            DATETIME COMMENT '发布时间',
   PRIMARY KEY (ACTIVITY_AREA_ID)
);

ALTER TABLE iwl_activity_area COMMENT '活动点';

/*==============================================================*/
/* Table: IWL_BIZ_UPLOAD_FILE                                   */
/*==============================================================*/
CREATE TABLE iwl_biz_upload_file
(
   BIZ_UPLOAD_FILE_ID   BIGINT NOT NULL AUTO_INCREMENT COMMENT '业务文件ID',
   BIZ_TYPE             VARCHAR(32) COMMENT '业务类型',
   BIZ_ID               BIGINT COMMENT '业务值',
   PRIMARY KEY (BIZ_UPLOAD_FILE_ID)
);

ALTER TABLE iwl_biz_upload_file COMMENT '业务文件';

/*==============================================================*/
/* Table: IWL_INTEREST                                          */
/*==============================================================*/
CREATE TABLE iwl_interest
(
   INTEREST_ID          BIGINT NOT NULL AUTO_INCREMENT COMMENT '兴趣ID',
   ACTIVITY_ID          BIGINT COMMENT '活动ID',
   OWNER                VARCHAR(64) COMMENT '兴趣人',
   GMT_CREATE           DATETIME COMMENT '创建时间',
   PRIMARY KEY (INTEREST_ID)
);

ALTER TABLE iwl_interest COMMENT '兴趣';

/*==============================================================*/
/* Table: IWL_LABEL                                             */
/*==============================================================*/
CREATE TABLE iwl_label
(
   LABEL_ID             BIGINT NOT NULL AUTO_INCREMENT COMMENT '标签:允许多个标签，采用;隔离',
   LABEL_NAME           VARCHAR(64) COMMENT '标签名',
   LABEL_TYPE           VARCHAR(32) COMMENT '标签类型',
   ORD                  INT COMMENT '序顺号',
   PRIMARY KEY (LABEL_ID)
);

ALTER TABLE iwl_label COMMENT '标签';

/*==============================================================*/
/* Table: IWL_MEMBER                                            */
/*==============================================================*/
CREATE TABLE iwl_member
(
   MEMBER_ID            BIGINT NOT NULL AUTO_INCREMENT COMMENT '会员号',
   MOBILE               VARCHAR(20) COMMENT '手机号',
   MEMBER_CODE          VARCHAR(64) COMMENT '帐号',
   MEMBER_PASSWORD      VARCHAR(32) COMMENT '密码',
   MEMBER_NAME          VARCHAR(64) COMMENT '姓名',
   EMAIL                VARCHAR(256) COMMENT '邮箱',
   SEX                  VARCHAR(32) COMMENT '性别',
   BIRTHDAY             DATE COMMENT '出生日期',
   RANK                 VARCHAR(32) COMMENT '会员等级',
   STATUS               VARCHAR(32) COMMENT '用户状态：01,已启用 02,已禁用',
   WEIXIN               VARCHAR(64) COMMENT '微信帐号',
   HEAD                 VARCHAR(2000) COMMENT '头像',
   NICK_NAME            VARCHAR(64) COMMENT '呢称',
   PRIMARY KEY (MEMBER_ID)
);

ALTER TABLE iwl_member COMMENT '会员用户';

/*==============================================================*/
/* Table: IWL_MEMBER_GROWTH_ACCOUNT                             */
/*==============================================================*/
CREATE TABLE iwl_member_growth_account
(
   MEMBER_GROWTH_ACCOUNT_ID INT NOT NULL AUTO_INCREMENT COMMENT '会员成长值ID',
   MEMBER_ID            BIGINT COMMENT '会员号',
   BALANCE              INT COMMENT '当前值',
   PRIMARY KEY (MEMBER_GROWTH_ACCOUNT_ID)
);

ALTER TABLE iwl_member_growth_account COMMENT '会员成长值';

/*==============================================================*/
/* Table: IWL_MEMBER_GROWTH_CHANGE_RECORD                       */
/*==============================================================*/
CREATE TABLE iwl_member_growth_change_record
(
   MEMBER_GROWTH_CHANGE_RECORD_ID BIGINT NOT NULL AUTO_INCREMENT COMMENT '会员成长变动记录ID',
   MEMBER_GROWTH_ACCOUNT_ID INT COMMENT '会员成长值ID',
   CHANGE_VALUE         INT COMMENT '变动值',
   MEMO                 CHAR(10) COMMENT '摘要',
   GMT_RECORD           DATETIME COMMENT '记录时间',
   PRIMARY KEY (MEMBER_GROWTH_CHANGE_RECORD_ID)
);

ALTER TABLE iwl_member_growth_change_record COMMENT '会员成长变动记录';

/*==============================================================*/
/* Table: IWL_OPERATE_LOG                                       */
/*==============================================================*/
CREATE TABLE iwl_operate_log
(
   OPERATE_LOG_ID       BIGINT NOT NULL AUTO_INCREMENT COMMENT '操作日志ID',
   OPERATOR             VARCHAR(64) COMMENT '操作人',
   OPERATOR_NAME        CHAR(10) COMMENT '姓名',
   OPERATOR_TYPE        VARCHAR(32) COMMENT '操作类型',
   LOG_TIME             DATETIME COMMENT '记录时间',
   LOG_LEVEL            VARCHAR(32) COMMENT '日志等级',
   URL                  VARCHAR(2000) COMMENT 'URL',
   LOG_CONTENT          TEXT COMMENT '日志详情',
   SYSTEM_CODE          VARCHAR(32) COMMENT '系统编码',
   SYSTEM_NAME          VARCHAR(64) COMMENT '系统名称',
   MODULE_CODE          VARCHAR(32) COMMENT '模块编码',
   MODULE_NAME          VARCHAR(64) COMMENT '模块名称',
   CLIENT_IP            VARCHAR(64) COMMENT '客户端IP',
   CLIENT_BROWSER       VARCHAR(64) COMMENT '客户端浏览器',
   CLIENT_LOCATION      VARCHAR(64) COMMENT '客户端所在地',
   CLIENT_OS            VARCHAR(64) COMMENT '客户端操作系统',
   PRIMARY KEY (OPERATE_LOG_ID)
);

ALTER TABLE iwl_operate_log COMMENT '操作日志';

/*==============================================================*/
/* Table: IWL_SEARCH_KEY                                        */
/*==============================================================*/
CREATE TABLE iwl_search_key
(
   SEARCH_KEY_ID        BIGINT NOT NULL AUTO_INCREMENT COMMENT '查询关键字ID',
   KEYWORD              VARCHAR(64) COMMENT '关键字',
   GMT_SEARCH           DATETIME COMMENT '查询时间',
   PRIMARY KEY (SEARCH_KEY_ID)
);

ALTER TABLE iwl_search_key COMMENT '查询关键字';

/*==============================================================*/
/* Table: IWL_UPLOAD_FILE                                       */
/*==============================================================*/
CREATE TABLE iwl_upload_file
(
   UPLOAD_FILE_ID       BIGINT NOT NULL AUTO_INCREMENT COMMENT '上传文件ID',
   BIZ_UPLOAD_FILE_ID   BIGINT COMMENT '业务文件ID',
   FILE_NAME            VARCHAR(256) COMMENT '文件名',
   FILE_URL             VARCHAR(2000) COMMENT '文件路径：采用相对路径',
   GMT_UPLOAD           DATETIME COMMENT '上传时间',
   ORD                  INT COMMENT '序顺号',
   PRIMARY KEY (UPLOAD_FILE_ID)
);

ALTER TABLE iwl_upload_file COMMENT '上传文件';

ALTER TABLE iwl_activity ADD CONSTRAINT FK_PF_A_AREA FOREIGN KEY (ACTIVITY_AREA_ID)
      REFERENCES iwl_activity_area (ACTIVITY_AREA_ID) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE iwl_interest ADD CONSTRAINT FK_PF_A_INTEREST FOREIGN KEY (ACTIVITY_ID)
      REFERENCES iwl_activity (ACTIVITY_ID) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE iwl_member_growth_account ADD CONSTRAINT FK_PF_M_G_A FOREIGN KEY (MEMBER_ID)
      REFERENCES iwl_member (MEMBER_ID) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE iwl_member_growth_change_record ADD CONSTRAINT FK_PF_M_G_C_R FOREIGN KEY (MEMBER_GROWTH_ACCOUNT_ID)
      REFERENCES iwl_member_growth_account (MEMBER_GROWTH_ACCOUNT_ID) ON DELETE RESTRICT ON UPDATE RESTRICT;

ALTER TABLE iwl_upload_file ADD CONSTRAINT FK_PF_BIZ_U_F FOREIGN KEY (BIZ_UPLOAD_FILE_ID)
      REFERENCES iwl_biz_upload_file (BIZ_UPLOAD_FILE_ID) ON DELETE RESTRICT ON UPDATE RESTRICT;
