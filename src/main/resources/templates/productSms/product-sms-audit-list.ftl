<#import "../layout/admin-assembly.ftl" as adminAssembly>
<#assign importjs=[
"lib/js/jqueryui/jquery-ui.min.js",
"js/productsms/product-sms-audit-list.js",
"lib/js/magicsuggest/magicsuggest.js"
]>

<#assign importCss=[
"css/iwithlong.css",
"lib/css/jqueryui/jquery-ui.css",
"lib/css/magicsuggest/magicsuggest-min.css"
]>

<@adminAssembly.layout importjavascript=importjs importCss=importCss>

<section class="content container-fluid">
    <form id="mainForm" action="list" method="post">
        <div class="box box-primary">
            <div class="box-body">
                <div class="form-group">
                    <div class="col-sm-4">
                        <label for="memberCode" class="control-label col-sm-3">账号</label>

                        <div class="col-sm-7">
                            <input type="text" id="memberCode" class="form-control" name="memberCode"
                                   value="${(productSmsVo.memberCode)!}">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <label for="activityEnrollName" class="control-label col-sm-3">报名单</label>

                        <div class="col-sm-7">
                            <input type="text" id="activityEnrollName" class="form-control" name="activityEnrollName"
                                   value="${(productSmsVo.activityEnrollName)!}">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <input type="hidden" name="pageSize" value="${(productSmsVo.pageSize)!}"/>
                        <input id="orderBy" type="hidden" name="orderBy" value="${(productSmsVo.orderBy)!}">
                        <button id="data-query" type="submit" class="btn btn-primary">查询</button>
                        <button id="query-reset" type="button" class="btn btn-reddit">重置</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="box box-primary">
            <div class="box-body table-responsive no-padding">
                <table class="table table-hover">
                    <tr>
                        <th><input type="checkbox" class="header-chk-all"/></th>
                        <th>账号</th>
                        <th>报名单</th>
                        <th>短信标题</th>
                        <th>短信数量</th>
                        <th>操作</th>
                    </tr>
                    <tbody>
                        <#list result as item>
                        <tr>
                            <td><input name="productSmsId" type="checkbox" class="row-chk" value="${(item.activityEnrollId)!}"></td>
                            <td>${(item.memberCode)!}</td>
                            <td>${(item.activityEnrollName)!}</td>
                            <td>${(item.title)!}</td>
                            <td>${(item.quantity)!}</td>
                            <td><a href="audit?productSmsId=${(item.productSmsId)}">审核</a></td>
                        </tr>
                        </#list>
                    </tbody>
                </table>
            </div>
            <@adminAssembly.page pagination=page.pager uri="list"/>
        </div>
    </form>
</section>

</@adminAssembly.layout>