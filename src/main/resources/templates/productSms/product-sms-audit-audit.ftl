<#import "../layout/admin-assembly.ftl" as adminAssembly>
<#assign importjs=[
"js/productsms/product-sms-audit.js"
]>

<@adminAssembly.layout importjavascript=importjs>

<section class="content container-fluid">
    <form id="addActivityForm" role="form" class="form-horizontal" method="post">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">
                    <i class="<@spring.messageText code="admin.list.btn.add.ico" text="fa fa-plus"/>"
                       aria-hidden="true"></i>
                    短信审核
                </h3>
            </div>
            <div class="box-body">
                <div class="form-group">
                    <label for="activityEnrollName" class="col-sm-2 control-label">
                        报名单
                    </label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="activityEnrollName" name="activityEnrollName"
                               value="${(productSmsVo.activityEnrollName)!}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">
                        账号
                    </label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="memberCode" name="memberCode"
                               value="${(productSmsVo.memberCode)!}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="title" class="col-sm-2 control-label">
                        短信标题
                    </label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="title" name="title"
                               value="${(productSmsVo.title)!}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="comment" class="col-sm-2 control-label">
                        短信内容
                    </label>
                    <div class="col-sm-10">
                        <div>您已成功报名<input type="text" name="params[0]" value="${(productSmsVo.params[0])!}"/>，时间:<input type="text" class="date-picker" readonly name="params[1]" value="${(productSmsVo.params[1])!}"/>，地点：<input type="text" value="${(productSmsVo.params[2])!}" name="params[2]"/>，请准时参加，现场签到，其他注意事项以报名页面提示内容为准，如有特殊情况需取消报名，请联系<input type="text" value="${(productSmsVo.params[3])!}" name="params[3]"/>，无故缺席将计入出勤率，影响之后的活动报名。
                            备注：<input type="text" name="params[4]" value="${(productSmsVo.params[4])!}"
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <a href="pass?productSmsId=${(productSmsVo.productSmsId)!}" type="button" class="btn btn-primary">通过</a>
                        <a id="notPass" type="button" class="btn btn-primary">驳回</a>
                        <a class="btn btn-default" href="list">返 回</a>
                    </div>
                </div>
            </div>
        </div>
    </form>
</section>

<div id="auditRefuseDiv" style="display: none;">
    <form id="auditForm" class="form-horizontal" action="notpass" method="post">
        <div class="form-group">
            <label for="reason" class="col-sm-2 control-label">
                原因
            </label>
            <div class="col-sm-8">
                <textarea type="text" class="form-control" id="resultReason"
                          name="reason">${(productSmsVo.reason)!}</textarea>
            </div>
        </div>
        <input type="hidden" name="productSmsId" value="${(productSmsVo.productSmsId)!}"/>
    </form>
</div>

</@adminAssembly.layout>