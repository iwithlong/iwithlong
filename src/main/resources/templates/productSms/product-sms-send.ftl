<#import "../layout/admin-assembly.ftl" as adminAssembly>
<#assign importjs=[
"lib/js/bootstrapvalidate/bootstrapValidator.min.js",
"lib/js/datepicker/bootstrap-datetimepicker.js",
"js/productsms/product_sms_send.js"
]>
<#assign importCss=[
"lib/css/datepicker/bootstrap-datetimepicker.css",
"lib/css/bootstrapvalidate/bootstrapValidator.min.css"
]>

<@adminAssembly.layout importjavascript=importjs importCss=importCss>

<section class="content container-fluid">
    <form id="addProductSmsForm" role="form" class="form-horizontal" action="${rc.contextPath}/b/productsms/add" method="post">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">
                    <i class="<@spring.messageText code="admin.list.btn.add.ico" text="fa fa-plus"/>"
                       aria-hidden="true"></i>
                    短信群发
                </h3>
            </div>
            <div class="box-body">
                <div class="form-group">
                    <label class="col-sm-2 control-label">
                        说明
                    </label>
                    <div class="col-sm-10">
                        <textarea class="form-control" readonly>本短信通知服务仅限于向已报名用户发送活动相关信息，请勿包含任何营销、广告信息，发送内容健康、合法合规，严禁携带任何政治、敏感词汇，否则可能无法通过审核，严重者停用账号并需承担一切法律后果。</textarea>
                    </div>
                </div>
                <input type="hidden" name="enrollNamelistIds" value="${(productSmsVo.enrollNamelistIds)!}"/>
                <input type="hidden" name="activityEnrollId" value="${(productSmsVo.activityEnrollId)!}">
                <input type="hidden" name="quantity" value="${(productSmsVo.quantity)!}">
                <div class="form-group">
                    <label  class="col-sm-2 control-label">
                    </label>
                    <div class="col-sm-10">
                        <span>已选择${(productSmsVo.quantity)!}个用户，本次发送${(productSmsVo.quantity)!}条短信</span>
                    </div>
                </div>
                <div class="form-group">
                    <label for="title" class="col-sm-2 control-label">
                        短信标题
                    </label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="title" name="title"
                               value="${(productSmsVo.title)!}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">
                        短信内容
                    </label>
                    <div class="col-sm-10">
                        <div>您已成功报名<input type="text" name="params[0]" value="${(productSmsVo.params[0])!}"/>，时间:<input type="text" class="date-picker" readonly name="params[1]" value="${(productSmsVo.params[1])!}"/>，地点：<input type="text" value="${(productSmsVo.params[2])!}" name="params[2]"/>，请准时参加，现场签到，其他注意事项以报名页面提示内容为准，如有特殊情况需取消报名，请联系<input type="text" value="${(productSmsVo.params[3])!}" name="params[3]"/>，无故缺席将计入出勤率，影响之后的活动报名。
                            备注：<input type="text" name="params[4]" value="${(productSmsVo.params[4])!}"
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <a id="addProductSms" type="button" class="btn btn-primary">确 定</a>
                        <a class="btn btn-default" href="list?getSession=true">取 消</a>
                    </div>
                </div>
            </div>
        </div>
    </form>
</section>

</@adminAssembly.layout>