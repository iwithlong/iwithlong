<#import "../layout/admin-assembly.ftl" as adminAssembly>

<@adminAssembly.layout>

<section class="content container-fluid">
	<form role="form" class="form-horizontal" action="update" method="post">
		<div class="box box-primary">
			<div class="box-header with-border">
				<h3 class="box-title">
					<i class="<@spring.messageText code="admin.list.btn.update.ico" text="fa fa-pencil"/>"
					   aria-hidden="true"></i>会员详情</h3>
			</div>
			<input type="hidden" name="memberId" value="${(memberDetail.memberId)!}"/>
			<div class="box-body">
				<div class="form-group">
					<label for="menuName" class="col-sm-2 control-label">微信帐号</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="weixin" name="weixin" value="${(memberDetail.weixin)!}" disabled>
					</div>
				</div>
				<div class="form-group">
					<label for="menuName" class="col-sm-2 control-label">昵称</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="nickName" name="nickName" value="${(memberDetail.nickName)!}" disabled>
					</div>
				</div>
				<div class="form-group">
					<label for="menuIco" class="col-sm-2 control-label">会员等级</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="menuIco" name="menuIco" value="${ranks['${memberDetail.rank}']}" disabled>
					</div>
				</div>
				<div class="form-group">
					<label for="menuUrl" class="col-sm-2 control-label">性别</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="sex" name="sex" value="${memberDetail.sexStr!}" disabled>
					</div>
				</div>
				<div class="form-group">
					<label for="parentMenuId" class="col-sm-2 control-label">手机号</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="mobile" name="mobile" value="${(memberDetail.mobile)!}" disabled>
					</div>
				</div>
				<div class="form-group">
					<label for="menuType" class="col-sm-2 control-label">邮箱</label>
					<div class="col-sm-10">
						<input type="text" class="form-control" id="email" name="email" value="${(memberDetail.email)!}" disabled>
					</div>
				</div>
				<div class="form-group">
					<div class="col-sm-offset-2 col-sm-10">
						<a class="btn btn-default" href="list"><@spring.messageText code="admin.form.btn.return.text" text="返 回"/></a>
					</div>
				</div>
			</div>
        </div>
	</form>
</section>

</@adminAssembly.layout>




