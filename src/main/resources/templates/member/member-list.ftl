<#import "../layout/admin-assembly.ftl" as adminAssembly>
<#assign importjs=[
"js/member.js"
]>

<@adminAssembly.layout importjavascript=importjs>

<section class="content container-fluid">
    <form id="mainForm" action="list" method="post">
        <div class="box box-primary">

            <div class="box-body table-responsive no-padding">
                <table class="table table-hover">
                    <tr>
                        <th><input type="checkbox" class="header-chk-all"></th>
                        <th>微信帐号</th>
                        <th>昵称</th>
                        <th>会员等级</th>
                        <th>性别</th>
						<th>操作</th>
                    </tr>
                    <tbody>
                        <#list memberList as item>
                        <tr>
                            <td><input name="memberId" type="checkbox" class="row-chk" value="${item.memberId}"></td>
                            <td>${item.weixin}</td>
                            <td>${(item.nickName)!}</td>
                            <td>
                            ${ranks['${item.rank}']}
                            </td>
                            <td>${item.sexStr}</td>
							<td>
                                <#if item.status == "01">
									<a href="#" class="data-disable">禁用</a>
                                <#elseif item.status == "02">
									<a href="#" class="data-enable">启用</a>
                                </#if>

								<a href="detail?id=${item.memberId}" class="data-detail">详情</a>

								<a href="#" class="data-delete">删除</a>
                            </td>
                        </tr>
                        </#list>
                    </tbody>
                </table>
            </div>
        </div>
    </form>
</section>

</@adminAssembly.layout>