<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>

<style>
    html,body {
        height: 100%;
    }
</style>

<body>
<iframe id="article" style="width: 100%; height: 100%" frameborder="0">

</iframe>

<script type="text/javascript">
    function GetQueryString(name)
    {
        var reg = new RegExp("(^|&)"+ name +"=([^&]*)(&|$)");
        var r = window.location.search.substr(1).match(reg);
        if(r!=null)return  unescape(r[2]); return null;
    }

    var url = decodeURIComponent(GetQueryString("url"));
    var iframe = document.getElementById("article");
    iframe.setAttribute("src", url.replace(/http/, "https"))
</script>

</body>
</html>
