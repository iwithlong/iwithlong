<#import "../layout/admin-assembly.ftl" as adminAssembly>

<#assign importjs=[
"lib/js/datepicker/bootstrap-datetimepicker.js",
"js/activityarea/activity_area_list.js"
]>

<#assign importCss=[
"lib/css/datepicker/bootstrap-datetimepicker.css",
"css/iwithlong.css"
]>

<@adminAssembly.layout importjavascript=importjs importCss=importCss>

<section class="content container-fluid">
    <form id="mainForm" action="list" method="post">
        <div class="box box-primary">
            <div class="box-body">
                <#--<form id="queryForm" class="form-horizontal" th:action="@{/b/activityarea/list}" method="post">-->
                    <div class="form-group">
                        <div class="col-sm-4">
                            <label for="keyword" class="control-label col-sm-3">关键词</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control" id="keyword" name="keyword"
                                       value="${(activityAreaVo.keyword)!}">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <label for="status" class="col-sm-3">发布状态</label>
                            <div class="col-sm-7">
                                <select id="status" class="form-control" name="status">
                                    <option value="">请选择</option>
                                    <@dickit dicKey="activity_area_status">
                                        <#list dictionaries as dic>
                                        <#if activityAreaVo.status??>
                                            <#if activityAreaVo.status==dic.dicKey>
                                                <option value="${(dic.dicKey)!}" selected>${(dic.dicValue)!}</option>
                                            <#else >
                                                <option value="${(dic.dicKey)!}">${(dic.dicValue)!}</option>
                                            </#if>
                                        <#else >
                                            <option value="${(dic.dicKey)!}">${(dic.dicValue)!}</option>
                                        </#if>
                                        </#list>
                                    </@dickit>
                                </select>
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <label for="enableSearch" class="col-sm-3">启动状态</label>
                            <div class="col-sm-7">
                                <select id="enableSearch" class="form-control" name="enableSearch">
                                    <option value="">请选择</option>
                                    <#if activityAreaVo.enableSearch??>
                                        <#if activityAreaVo.enableSearch=="0">
                                            <option value="0" selected>禁用</option>
                                            <option value="1">启用</option>
                                        <#elseif activityAreaVo.enableSearch=="1">
                                            <option value="0">禁用</option>
                                            <option value="1" selected>启用</option>
                                        <#else >
                                            <option value="0">禁用</option>
                                            <option value="1">启用</option>
                                        </#if>
                                    <#else >
                                        <option value="0">禁用</option>
                                        <option value="1">启用</option>
                                    </#if>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-4">
                            <label for="modifier" class="control-label col-sm-3">修改人</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control" id="modifier" name="modifier"
                                       value="${(activityAreaVo.modifier)!}">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <label for="issuer" class="control-label col-sm-3">发布人</label>
                            <div class="col-sm-7">
                                <input type="text" class="form-control" id="issuer" name="issuer"
                                       value="${(activityAreaVo.issuer)!}">
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <#--<input type="hidden" name="pageNum" value="${(activityAreaVo.pageNum)!}">-->
                            <input type="hidden" name="pageSize" value="${(activityAreaVo.pageSize)!}">
                            <button id="data-query" type="submit" class="btn btn-primary">查询</button>
                            <button type="button" id="query-reset" class="btn ">重置</button>
                        </div>
                    </div>
                <#--</form>-->
            </div>
        </div>
        <div class="box box-primary">

            <div class="box-header">
                <div class="box-tools">
                    <a href="add" class="btn btn-sm bg-olive btn-add">
                        <i class="<@spring.messageText code="admin.list.btn.add.ico" text="fa fa-plus"/>"
                           aria-hidden="true"></i> <@spring.messageText code="admin.list.btn.add.text" text="添加"/>
                    </a>
                    <a href="update" class="btn btn-sm bg-orange btn-update">
                        <i class="<@spring.messageText code="admin.list.btn.update.ico" text="fa fa-pencil"/>"
                           aria-hidden="true"></i> <@spring.messageText code="admin.list.btn.update.text" text="修改"/>
                    </a>
                    <a href="delete" class="btn btn-sm bg-danger btn-confirm"
                       msg="<@spring.messageText code="admin.list.btn.delete.confirm.msg" text="删除操作不可恢复，确认删除？"/>">
                        <i class="<@spring.messageText code="admin.list.btn.delete.ico" text="fa fa-trash"/>"
                           aria-hidden="true"></i> <@spring.messageText code="admin.list.btn.delete.text" text="删除"/>
                    </a>
                </div>
            </div>
            <div class="box-body table-responsive no-padding">
                <table class="table table-hover">
                    <tr>
                        <th><input type="checkbox" class="header-chk-all""></th>
                        <th>缩略图</th>
                        <th>名称</th>
                        <th>地址</th>
                        <th>修改人</th>
                        <th>修改时间</th>
                        <th>发布人</th>
                        <th>发布时间</th>
                        <th>状态</th>
                        <th>启用状态</th>
                        <th>操作</th>
                    </tr>
                    <tbody>
                        <#list result as item>
                        <tr <#if item.enable==false>class="gray" </#if>>
                            <td><input name="activityAreaId" type="checkbox" class="row-chk" value="${(item.activityAreaId)!}"></td>
                            <td>
                                <#list item.uploadFileVos as uploadFileVo>
                                    <#if uploadFileVo_index==0>
                                        <img src="${rc.contextPath}/upload/get/image.json?url=${(uploadFileVo.fileUrl)!}" style="width: 80px;height: 80px;" class="img-responsive" />
                                    </#if>
                                </#list>
                            </td>
                            <td>${(item.areaName)!}</td>
                            <td>${(item.address)!}</td>
                            <td>${(item.modifier)!}</td>
                            <td>${(item.gmtModify?string("yyyy-MM-dd hh:mm"))!}</td>
                            <td>${(item.issuer)!}</td>
                            <td>${(item.gmtIssue?string("yyyy-MM-dd hh:mm"))!}</td>
                            <td>
                                <@dickit dicKey="activity_area_status">
                                    <#list dictionaries as dic>
                                        <#if item.status==dic.dicKey>${(dic.dicValue)!}</#if>
                                    </#list>
                                </@dickit>
                            </td>
                            <td><#if item.enable == true>
                                启用
                            <#elseif item.enable == false>
                                禁用
                            </#if></td>
                            <td>
                                <a href="detail?activityAreaId=${(item.activityAreaId)!}" class="data-detail">详情</a>
                                <a href="update?activityAreaId=${(item.activityAreaId)!}">修改</a>
                                <#if item.status=='4'>
                                    <a href="#" class="goIssuer" data-id="${(item.activityAreaId)!}">发布</a>
                                </#if>
                                <#if item.enable == true>
                                    <a data-id="${(item.activityAreaId)!}" href="#" class="data-enable">禁用</a>
                                <#elseif item.enable == false>
                                    <a data-id="${(item.activityAreaId)!}" href="#" class="data-enable">启用</a>
                                </#if>
                                <a href="/b/activity/list?activityAreaVo.areaName=${(item.areaName)!}">活动</a>
                                <#--<a href="#" class="activity-list">查看活动</a>-->
                            </td>
                        </tr>
                        </#list>
                    </tbody>
                </table>
            </div>
            <@adminAssembly.page pagination=page.pager uri="list"/>
        <#--<div th:include="admin/widgets/admin-widgets :: admin-page(${menuList.pager},@{/menu/list})"></div>-->
        </div>
    </form>
</section>

<div id="goIssuerForm" style="display: none;">
    <form id="issueForm" action="issue" class="form-horizontal" method="post">
        <input type="hidden" name="activityAreaId" id="issuerActivityAreaId">
        <div class="form-group">
            <label class="col-sm-2 control-label">
                发布类型
            </label>
            <div class="col-sm-9">
                <input type="hidden" id="isNow" name="isNow" value="true"/>
                <select id="issueType" class="form-control">
                    <option value="true" selected>立即发布</option>
                    <option value="false">定时发布</option>
                </select>
            </div>
        </div>
        <div class="form-group gmtIssueDiv" hidden>
            <label class="col-sm-2 control-label">
                发布时间
            </label>
            <div class="col-sm-9">
                <input type="text" class="form-control date-picker" id="gmtIssueInput" name="gmtIssue"
                       readonly value="">
            </div>
        </div>
    </form>
</div>

</@adminAssembly.layout>