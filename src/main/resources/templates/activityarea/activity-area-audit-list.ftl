<#import "../layout/admin-assembly.ftl" as adminAssembly>

<@adminAssembly.layout>

<section class="content container-fluid">
    <form id="mainForm" action="list" method="post">
        <div class="box box-primary">
            <div class="box-body table-responsive no-padding">
                <table class="table table-hover">
                    <tr>
                        <th><input type="checkbox" class="header-chk-all""></th>
                        <th>缩略图</th>
                        <th>名称</th>
                        <th>提交人</th>
                        <th>提交时间</th>
                        <th>操作</th>
                    </tr>
                    <tbody>
                        <#list result as item>
                        <tr>
                            <td><input name="activityAreaId" type="checkbox" class="row-chk" value="${(item.activityAreaId)!}"></td>
                            <td>
                                <#list item.uploadFileVos as uploadFileVo>
                                    <#if uploadFileVo_index==0>
                                        <img src="${rc.contextPath}/upload/get/image.json?url=${(uploadFileVo.fileUrl)!}" style="width: 80px;height: 80px;" class="img-responsivek" />
                                    </#if>
                                </#list>
                            </td>
                            <td>${(item.areaName)!}</td>
                            <td>${(item.modifier)!}</td>
                            <td>${(item.gmtModify)!}</td>
                            <td>
                                <#--<a href="detail?activityAreaId=${(item.activityAreaId)!}" class="data-detail">查看</a>-->
                                <a href="detail?activityAreaId=${(item.activityAreaId)!}" class="data-audit">审核</a>
                            </td>
                        </tr>
                        </#list>
                    </tbody>
                </table>
            </div>
            <@adminAssembly.page pagination=page.pager uri="list"/>
        <#--<div th:include="admin/widgets/admin-widgets :: admin-page(${menuList.pager},@{/menu/list})"></div>-->
        </div>
    </form>
</section>

</@adminAssembly.layout>