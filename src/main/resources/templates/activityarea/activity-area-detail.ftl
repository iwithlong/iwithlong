<#import "../layout/admin-assembly.ftl" as adminAssembly>
<#assign importjs=[
"js/init-area.js",
"lib/js/bootstrapfileinput/fileinput.min.js",
"lib/js/bootstrapfileinput/locales/zh.js",
"js/pic_upload.js",
"ueditor/ueditor.config.js",
"ueditor/ueditor.all.js",
"js/init_ueditor.js"
]>

<#assign  importCss=[
"lib/css/bootstrapfileinput/fileinput.min.css",
"ueditor/themes/default/css/ueditor.min.css"
]>

<@adminAssembly.layout importjavascript=importjs importCss=importCss>

<section class="content container-fluid">
    <form id="updateForm" role="form" class="form-horizontal" th:action="@{/b/activityarea/update}" method="post">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">
                    <i class="<@spring.messageText code="admin.list.btn.update.ico" text="fa fa-plus"/>"
                       aria-hidden="true"></i>
                    活动点详情
                </h3>
            </div>
            <input type="hidden" name="activityAreaId" value="${(activityAreaVo.activityAreaId)!}">
            <div class="box-body">
                <div class="form-group">
                    <label for="areaName" class="col-sm-2 control-label">
                        活动点名称
                    </label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="areaName" name="areaName" value="${(activityAreaVo.areaName)!}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="brief" class="col-sm-2 control-label">
                        简述
                    </label>
                    <div class="col-sm-10">
                        <textarea class="form-control" id="brief" name="brief">${(activityAreaVo.brief)!}</textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label for="province" class="col-sm-2 control-label">
                        省份
                    </label>
                    <div class="col-sm-2">
                        <select name="province" id="province" class="form-control provinceSelect">
                            <#list areaMap.provinceList as area>
                                <#if activityAreaVo.province==area.code><option value="${(area.code)!}">${(area.name)!}</option></#if>
                            </#list>
                        </select>
                    </div>
                    <label for="city" class="col-sm-2 control-label" class="form-control">
                        城市
                    </label>
                    <div class="col-sm-2">
                        <select name="city" id="city" class="form-control citySelect">
                            <#list areaMap.townList as area>
                                <#if area.parentCode==activityAreaVo.province>
                                    <#if activityAreaVo.city==area.code><option value="${(area.code)!}" >${(area.name)!}</option></#if>
                                </#if>
                            </#list>
                        </select>
                    </div>
                    <label for="district" class="col-sm-2 control-label">
                        区、镇
                    </label>
                    <div class="col-sm-2">
                        <select name="district" id="district" class="form-control townSelect">
                            <option value="">请选择</option>
                            <#list areaMap.countyList as area>
                                <#if area.parentCode==activityAreaVo.city>
                                    <#if activityAreaVo.district==area.code><option value="${(area.code)!}" >${(area.name)!}</option></#if>
                                </#if>
                            </#list>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="address" class="col-sm-2 control-label">
                        地址
                    </label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="address" name="address" value="${(activityAreaVo.address)!}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="longitude" class="col-sm-2 control-label">
                        经度
                    </label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" id="longitude" name="longitude"
                               value="${(activityAreaVo.longitude?string("0.############"))!}">
                    </div>
                    <label for="longitude" class="col-sm-2 control-label">
                        纬度
                    </label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" id="latitude" name="latitude"
                               value="${(activityAreaVo.latitude?string("0.############"))!}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="activityCount" class="col-sm-2 control-label">
                        活动数
                    </label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" id="activityCount" name="activityCount" value="${(activityAreaVo.activityVos?size)!}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="areaIntroduction" class="col-sm-2 control-label">
                        介绍
                    </label>
                    <div class="col-sm-10">
                        <script id="container" type="text/plain">
                            ${(activityAreaVo.areaIntroduction)!}
                        </script>
                    </div>
                </div>
                <div id="uploadedPic" style="display: none">
                    <#if activityAreaVo??>
                        <#list activityAreaVo.uploadFileVos as uploadFileVo>
                            <input data-file-name="${uploadFileVo.fileName}" data-key="${uploadFileVo_index}" data-id="${(uploadFileVo.uploadFileId!)}" class="initPic" type="hidden" value="${rc.contextPath}/upload/get/image.json?url=${(uploadFileVo.fileUrl)!}" />
                        </#list>
                    </#if>

                </div>
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label">
                        图片
                    </label>
                    <div class="col-sm-10">
                        <input readonly type="file" name="multipartFile" multiple class="picUpload"/>
                    </div>
                </div>
                <input id="uploadFileIds" name="uploadFileIds" type="hidden"/>
                <div class="form-group">
                    <label for="oldStatus" class="col-sm-2 control-label">
                        状态
                    </label>
                    <div class="col-sm-10">
                        <select class="form-control">
                            <@dickit dicKey="activity_area_status">
                                <#list dictionaries as dic>
                                    <#if activityAreaVo.status==dic.dicKey>
                                        <option selected>${(dic.dicValue)!}</option>
                                    </#if>
                                </#list>
                            </@dickit>
                        </select>
                    </div>
                    <label for="oldStatus" class="col-sm-2 control-label">
                        启用状态
                    </label>
                    <div class="col-sm-10">
                        <select class="form-control">
                            <#if activityAreaVo.enable==true>
                                <option selected>已启用</option>
                            <#else>
                                <option selected>已禁用</option>
                            </#if>
                        </select>
                    </div>
                </div>
                <input type="hidden" name="uploadFileIds"/>
                <#if needAudit==true>
                    <div class="form-group">
                        <label for="auditor" class="col-sm-2 control-label">
                            审核人
                        </label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="auditor" name="auditor" value="${(activityAreaVo.auditor)!}">
                        </div>
                        <label for="gmtAudit" class="col-sm-2 control-label">
                            审核时间
                        </label>
                        <div class="col-sm-3">
                            <input type="text" class="form-control" id="gmtAudit" name="gmtAudit" value="${(activityAreaVo.gmtAudit)!}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="oldStatus" class="col-sm-2 control-label">
                            审核意见
                        </label>
                        <div class="col-sm-10">
                            <input type="text" class="form-control" id="reason" name="reason" value="${(activityAreaVo.reasion)!}">
                        </div>
                    </div>
                </#if>
                <div class="form-group">
                    <label for="creator" class="col-sm-2 control-label">
                        创建人
                    </label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" id="creator" name="creator" value="${(activityAreaVo.gmtModify)!}">
                    </div>
                    <label for="gmtCreate" class="col-sm-2 control-label">
                        创建时间
                    </label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" id="gmtCreate" name="gmtCreate" value="${(activityAreaVo.gmtCreate)!}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="modifier" class="col-sm-2 control-label">
                        修改人
                    </label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" id="modifier" name="modifier" value="${(activityAreaVo.modifier)!}">
                    </div>
                    <label for="gmtModify" class="col-sm-2 control-label">
                        修改时间
                    </label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" id="gmtModify" name="gmtModify" value="${(activityAreaVo.gmtModify)!}">
                    </div>
                </div>
                <input id="status" type="hidden" name="status" value=""/>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <a class="btn btn-default" href="list">返 回</a>
                    </div>
                </div>
            </div>
        </div>
    </form>
</section>


</@adminAssembly.layout>