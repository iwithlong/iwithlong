<#import "../layout/admin-assembly.ftl" as adminAssembly>
<#assign importjs=[
"lib/js/bootstrapfileinput/fileinput.min.js",
"lib/js/bootstrapfileinput/locales/zh.js",
"js/pic_upload.js",
"js/init-area.js",
"ueditor/ueditor.config.js",
"ueditor/ueditor.all.js",
"js/init_ueditor.js",
"lib/js/bootstrapvalidate/bootstrapValidator.min.js",
"js/activityarea/activity_area_update.js"
]>
<#assign  importCss=[
"lib/css/bootstrapfileinput/fileinput.min.css",
"ueditor/themes/default/css/ueditor.min.css",
"lib/css/bootstrapvalidate/bootstrapValidator.min.css"
]>

<@adminAssembly.layout importjavascript=importjs importCss=importCss>

<section class="content container-fluid">
    <form id="updateForm" role="form" class="form-horizontal" th:action="@{/b/activityarea/update}" method="post">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">
                    <i class="<@spring.messageText code="admin.list.btn.update.ico" text="fa fa-plus"/>"
                       aria-hidden="true"></i>
                    修改活动点
                </h3>
            </div>
            <input type="hidden" name="activityAreaId" value="${(activityAreaVo.activityAreaId)!}">
            <div class="box-body">
                <div class="form-group">
                    <label for="areaName" class="col-sm-2 control-label">
                        活动点名称
                    </label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="areaName" name="areaName" value="${(activityAreaVo.areaName)!}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="brief" class="col-sm-2 control-label">
                        简述
                    </label>
                    <div class="col-sm-10">
                        <textarea class="form-control" id="brief" name="brief">${(activityAreaVo.brief)!}</textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label for="province" class="col-sm-2 control-label">
                        省份
                    </label>
                    <div class="col-sm-2">
                        <select name="province" id="province" class="form-control provinceSelect">
                            <option value="">请选择</option>
                            <#list areaMap.provinceList as area>
                                <option value="${(area.code)!}" <#if activityAreaVo.province==area.code>selected</#if>>${(area.name)!}</option>
                            </#list>
                        </select>
                    </div>
                    <label for="city" class="col-sm-2 control-label" class="form-control">
                        城市
                    </label>
                    <div class="col-sm-2">
                        <select name="city" id="city" class="form-control citySelect">
                            <option value="">请选择</option>
                            <#list areaMap.townList as area>
                            <#if area.parentCode==activityAreaVo.province>
                                <option value="${(area.code)!}" <#if activityAreaVo.city==area.code>selected</#if>>${(area.name)!}</option>
                            </#if>
                            </#list>
                        </select>
                    </div>
                    <label for="district" class="col-sm-2 control-label">
                        区、镇
                    </label>
                    <div class="col-sm-2">
                        <select name="district" id="district" class="form-control townSelect">
                            <option value="">请选择</option>
                            <#list areaMap.countyList as area>
                            <#if area.parentCode==activityAreaVo.city>
                                <option value="${(area.code)!}" <#if activityAreaVo.district==area.code>selected</#if>>${(area.name)!}</option>
                            </#if>
                            </#list>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="address" class="col-sm-2 control-label">
                        地址
                    </label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="address" name="address" value="${(activityAreaVo.address)!}">
                    </div>
                </div>
                <div class="form-group">
                <#--<label for="longitude" class="col-sm-2 control-label">-->
                <#--经度-->
                <#--</label>-->
                <#--<div class="col-sm-3">-->
                    <input type="hidden" id="longitude" name="longitude"
                           value="${(activityAreaVo.longitude)!}">
                <#--</div>-->
                <#--<label for="longitude" class="col-sm-2 control-label">-->
                <#--纬度-->
                <#--</label>-->
                <#--<div class="col-sm-3">-->
                    <input type="hidden" id="latitude" name="latitude"
                           value="${(activityAreaVo.latitude)!}">
                <#--</div>-->
                    <label for="longitude" class="col-sm-2 control-label">
                        经纬度
                    </label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" id="longitudeAndLatitude" name="longitudeAndLatitude" value="${(activityAreaVo.latitude?string("#.############"))!}<#if activityAreaVo??&&activityAreaVo.latitude??>,</#if>${(activityAreaVo.longitude?string("#.############"))!}">
                    </div>
                </div>
                <div class="form-group">
                    <input id="containerInitValue" type="hidden" name="areaIntroduction" value="">
                    <label for="areaIntroduction" class="col-sm-2 control-label">
                        介绍
                    </label>
                    <div class="col-sm-10">
                        <script id="container" type="text/plain">
                            ${(activityAreaVo.areaIntroduction)!}
                        </script>
                    </div>
                </div>
                <div class="form-group">
                    <label for="oldStatus" class="col-sm-2 control-label">
                        状态
                    </label>
                    <div class="col-sm-10">
                        <select readonly="readonly" class="form-control">
                            <@dickit dicKey="activity_area_status">
                                <#list dictionaries as dic>
                                    <#if activityAreaVo.status==dic.dicKey>
                                        <option selected>${(dic.dicValue)!}</option>
                                    </#if>
                                </#list>
                            </@dickit>
                        </select>
                    </div>
                </div>
                <#if needAudit==true>
                    <div class="form-group">
                        <label for="auditor" class="col-sm-2 control-label">
                            审核人
                        </label>
                        <div class="col-sm-3">
                            <input type="text" readonly="readonly" class="form-control" id="auditor" name="auditor" value="${(activityAreaVo.auditor)!}">
                        </div>
                        <label for="gmtAudit" class="col-sm-2 control-label">
                            审核时间
                        </label>
                        <div class="col-sm-3">
                            <input type="text" readonly="readonly" class="form-control" id="gmtAudit" name="gmtAudit" value="${(activityAreaVo.gmtAudit?string("yyyy-MM-dd HH:mm:ss"))!}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="oldStatus" class="col-sm-2 control-label">
                            审核意见
                        </label>
                        <div class="col-sm-10">
                            <input type="text" readonly="readonly" class="form-control" id="reason" name="reason" value="${(activityAreaVo.reason)!}">
                        </div>
                    </div>
                </#if>
                <div class="form-group">
                    <label for="creator" class="col-sm-2 control-label">
                        创建人
                    </label>
                    <div class="col-sm-3">
                        <input type="text" readonly="readonly" class="form-control" id="creator" name="creator" value="${(activityAreaVo.creator)!}">
                    </div>
                    <label for="gmtCreate" readonly="readonly" class="col-sm-2 control-label">
                        创建时间
                    </label>
                    <div class="col-sm-3">
                        <input type="text" readonly="readonly" class="form-control" id="gmtCreate" name="gmtCreate" value="${(activityAreaVo.gmtCreate?string("yyyy-MM-dd HH:mm:ss"))!}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="modifier" class="col-sm-2 control-label">
                        修改人
                    </label>
                    <div class="col-sm-3">
                        <input type="text" readonly="readonly" class="form-control" id="modifier" name="modifier" value="${(activityAreaVo.modifier)!}">
                    </div>
                    <label for="gmtModify" class="col-sm-2 control-label">
                        修改时间
                    </label>
                    <div class="col-sm-3">
                        <input type="text" readonly="readonly" class="form-control" id="gmtModify" name="gmtModify" value="${(activityAreaVo.gmtModify?string("yyyy-MM-dd HH:mm:ss"))!}">
                    </div>
                </div>
                <input id="status" type="hidden" name="status" value=""/>
                <div id="uploadedPic" style="display: none">
                    <#if activityAreaVo??>
                        <#list activityAreaVo.uploadFileVos as uploadFileVo>
                            <input data-file-name="${uploadFileVo.fileName}" data-key="${uploadFileVo_index}" data-id="${(uploadFileVo.uploadFileId!)}" class="initPic" type="hidden" value="${rc.contextPath}/upload/get/image.json?url=${(uploadFileVo.fileUrl)!}" />
                        </#list>
                    </#if>

                </div>
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label">
                        图片
                    </label>
                    <div class="col-sm-10">
                        <input type="file" name="multipartFile" multiple class="picUpload"/>
                        <p class="help-block">支持jpg、jpeg、png、gif格式，大小不超过2.0M</p>
                    </div>
                </div>
                <input id="uploadFileIds" name="uploadFileIds" type="hidden"/>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="button" class="btn btn-primary save">保存</button>
                        <#if needAudit==true>
                            <button type="button" class="btn btn-primary audit">提交审核</button>
                        <#else>
                            <button type="button" class="btn btn-primary issuer">发布</button>
                        </#if>
                        <a class="btn btn-default" href="list">返 回</a>
                    </div>
                </div>
            </div>
        </div>
    </form>
</section>

</@adminAssembly.layout>