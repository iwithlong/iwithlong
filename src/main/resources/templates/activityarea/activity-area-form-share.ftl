<div class="box-body">
    <div class="form-group">
        <label for="areaName" class="col-sm-2 control-label">
        活动点名称
        </label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="areaName" name="areaName" value="${(activityAreaVo.areaName)!}">
        </div>
    </div>
    <div class="form-group">
        <label for="brief" class="col-sm-2 control-label">
            简述
        </label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="brief" name="brief" value="${(activityAreaVo.brief)!}">
        </div>
    </div>
    <div class="form-group">
        <label for="province" class="col-sm-2 control-label">
            省份
        </label>
        <div class="col-sm-1">
            <select name="province" id="province" class="form-control">
                <option>浙江</option>
            </select>
        </div>
        <label for="city" class="col-sm-2 control-label" class="form-control">
            城市
        </label>
        <div class="col-sm-1">
            <select name="city" id="city" class="form-control">
                <option>杭州</option>
            </select>
        </div>
        <label for="district" class="col-sm-2 control-label">
            区、镇
        </label>
        <div class="col-sm-1">
            <select name="district" id="district" class="form-control">
                <option>西湖</option>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label for="address" class="col-sm-2 control-label">
            地址
        </label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="address" name="address" value="${(activityAreaVo.address)!}">
        </div>
    </div>
    <div class="form-group">
        <label for="areaIntroduction" class="col-sm-2 control-label">
            介绍
        </label>
        <div class="col-sm-10">
            <textarea class="form-control" id="areaIntroduction" name="areaIntroduction" value="${(activityAreaVo.areaIntroduction)!}">
            </textarea>
        </div>
    </div>
</div>