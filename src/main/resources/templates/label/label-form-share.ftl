<div class="box-body">
    <div class="form-group">
        <label for="labelName" class="col-sm-2 control-label">
        <@spring.messageText code="admin.model.text.menu.menuName" text="标签名"/>
        </label>
        <div class="col-sm-10">
            <input type="text" class="form-control" id="labelName" name="labelName" value="${(label.labelName)!}">
        </div>
    </div>
    <div class="form-group">
        <label for="labelType" class="col-sm-2 control-label">
        <@spring.messageText code="admin.model.text.menu.menuType" text="标签类型"/>
        </label>
        <div class="col-sm-10">
            <select name="labelType" id="labelType" class="form-control">
            <@dickit dicKey="label_type">
                <#list dictionaries as dic>
                    <#if "hot_keywords" != dic.dicKey>
						<option value="${dic.dicKey}" <#if (label.labelType)! == dic.dicKey>selected</#if>>${(dic.dicValue)!}</option>
                    </#if>
                </#list>
            </@dickit>
            </select>
        </div>
    </div>
    <div class="form-group">
        <label for="ord" class="col-sm-2 control-label">
        <@spring.messageText code="admin.model.text.menu.orderNo" text="序顺号"/>
        </label>
        <div class="col-sm-10">
            <input type="number" class="form-control" id="ord" name="ord" value="${(label.ord)!}" max="99999">
        </div>
    </div>
    <div class="form-group">
        <div class="col-sm-offset-2 col-sm-10">
            <button type="submit" class="btn btn-primary"><@spring.messageText code="admin.form.btn.submit.text" text="确 定"/></button>
            <a class="btn btn-default" href="list"><@spring.messageText code="admin.form.btn.return.text" text="返 回"/></a>
        </div>
    </div>
</div>