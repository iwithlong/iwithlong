<#import "../layout/admin-assembly.ftl" as adminAssembly>

<@adminAssembly.layout>

<section class="content container-fluid">
    <form role="form" class="form-horizontal" th:action="@{/label/add}" method="post">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">
                    <i class="<@spring.messageText code="admin.list.btn.add.ico" text="fa fa-plus"/>"
                       aria-hidden="true"></i>
                    <@spring.messageText code="admin.model.add.header.menu" text="添加标签"/>
                </h3>
            </div>
            <#include "label-form-share.ftl">
        </div>
    </form>
</section>

</@adminAssembly.layout>