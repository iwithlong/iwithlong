<#import "../layout/admin-assembly.ftl" as adminAssembly>
<#assign importjs=[
"js/label.js"
]>

<@adminAssembly.layout importjavascript=importjs>

<section class="content container-fluid">
    <form id="mainForm" action="list" method="post">

		<div class="box box-primary">
			<div class="box-body">
				<div class="form-group">

					<div class="col-sm-4">
						<label for="keyword" class="control-label col-sm-3">关键词</label>
						<div class="col-sm-7">
							<input type="text" class="form-control" id="keyword" name="keyword"
								   value="${(labelVo.keyword)!}">
						</div>
					</div>
					<div class="col-sm-4">
						<label for="status" class="col-sm-3">标签类型</label>
						<div class="col-sm-7">
							<select class="form-control" id="labelType" name="labelType">
								<@dickit dicKey="label_type">
									<option value="">请选择</option>
									<#list dictionaries as dic>
										<option value="${dic.dicKey}" <#if (labelVo.labelType)! == dic.dicKey>selected</#if>>${(dic.dicValue)!}</option>
									</#list>
								</@dickit>
							</select>
						</div>
					</div>

					<div class="col-sm-4">
						<input type="hidden" name="pageNum" value="${(labelVo.pageNum)!}">
						<input type="hidden" name="pageSize" value="${(labelVo.pageSize)!}">
						<button id="data-query" type="submit" class="btn btn-primary">查询</button>
					</div>
				</div>
			</div>
		</div>

        <div class="box box-primary">
			<div class="box-header">
				<div class="box-tools">
					<a href="add" class="btn btn-sm bg-olive btn-add">
						<i class="<@spring.messageText code="admin.list.btn.add.ico" text="fa fa-plus"/>"
						   aria-hidden="true"></i> <@spring.messageText code="admin.list.btn.add.text" text="添加"/>
					</a>

					<a href="update_hot_keywords" class="btn btn-sm bg-olive btn-add">
						<i class="<@spring.messageText code="admin.list.btn.add.ico" text="fa fa-plus"/>"
						   aria-hidden="true"></i> 更新热门关键字标签
					</a>

					<a href="update_my_words" class="btn btn-sm bg-olive btn-add">
						<i class="<@spring.messageText code="admin.list.btn.add.ico" text="fa fa-plus"/>"
						   aria-hidden="true"></i> 更新我的分词词库
					</a>

				</div>
			</div>
            <div class="box-body table-responsive no-padding">
                <table class="table table-hover">
                    <tr>
                        <th><input type="checkbox" class="header-chk-all"></th>
                        <th>标签名</th>
                        <th>标签类型</th>
                        <th>序顺号</th>
						<th>操作</th>
                    </tr>
                    <tbody>
                        <#list labelList as item>
                        <tr>
                            <td><input name="labelId" type="checkbox" class="row-chk" value="${item.labelId}"></td>
                            <td>${item.labelName}</td>
                            <td>
                                <@dickit dicKey="label_type">
                                    <#list dictionaries as dic>
										 <#if (item.labelType)! == dic.dicKey>${(dic.dicValue)!}</#if>
                                    </#list>
                                </@dickit>
                            </td>
                            <td>${(item.ord)!}</td>
							<td>
								<#if (item.labelType)! =="hot_keywords">
									<span>修改</span>
								<#else >
									<a href="update?labelId=${item.labelId}" class="data-update">修改</a>
								</#if>
								<a href="#" class="data-delete">删除</a>
                            </td>
                        </tr>
                        </#list>
                    </tbody>
                </table>
            </div>
        </div>
    </form>
</section>

</@adminAssembly.layout>