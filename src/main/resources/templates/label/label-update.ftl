<#import "../layout/admin-assembly.ftl" as adminAssembly>

<@adminAssembly.layout>

<section class="content container-fluid">
    <form role="form" class="form-horizontal" th:action="@{/menu/update}" method="post">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">
                    <i class="<@spring.messageText code="admin.list.btn.update.ico" text="fa fa-plus"/>"
                       aria-hidden="true"></i>
                    <@spring.messageText code="admin.model.update.header.menu" text="修改标签"/>
                </h3>
            </div>
            <input type="hidden" name="labelId" value="${(label.labelId)!}">
            <#include "label-form-share.ftl">
        </div>
    </form>
</section>

</@adminAssembly.layout>