<#import "../layout/admin-assembly.ftl" as adminAssembly>
<#assign importjs=[
"lib/js/bootstrapvalidate/bootstrapValidator.min.js",
"lib/js/jqueryui/jquery-ui.min.js",
"js/advert/advert_update.js"
]>
<#assign importCss=[
"lib/css/bootstrapvalidate/bootstrapValidator.min.css",
"lib/css/jqueryui/jquery-ui.css"
]>

<@adminAssembly.layout importjavascript=importjs importCss=importCss>

<section class="content container-fluid">
    <form id="updateAdvertForm" role="form" class="form-horizontal" action="${rc.contextPath}/b/advert/update" enctype="multipart/form-data"
          method="post">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">
                    <i class="<@spring.messageText code="admin.list.btn.add.ico" text="fa fa-plus"/>"
                       aria-hidden="true"></i>
                    修改<@dickit dicKey="index_advert_type">
                    <#list dictionaries as dic>
                        <#if advertVo.advertType==dic.dicKey>${(dic.dicValue)!}</#if>
                    </#list>
                </@dickit>
                    <input type="hidden" name="advertType" value="${(advertVo.advertType)!}"/>
                    <input type="hidden" name="advertId" value="${(advertVo.advertId)!}"/>
                </h3>
            </div>
            <div class="box-body">
                <div class="form-group">
                    <label for="memberName" class="col-sm-2 control-label">
                        链接资源
                    </label>
                    <div class="col-sm-10">
                        <input type="text" id="bizName" name="bizName" class="form-control" value="${(advertVo.bizName)!}"/>
                        <input id="bizId" type="hidden" name="bizId" value="${(advertVo.bizId)!}"/>
                        <input type="hidden" name="bizType" value="1"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="title" class="col-sm-2 control-label">
                        前台显示标题
                    </label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="title" name="title"
                               value="${(advertVo.title)!}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="description" class="col-sm-2 control-label">
                        描述
                    </label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="description" name="description"
                               value="${(advertVo.description)!}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="intro" class="col-sm-2 control-label">
                        前台显示缩略图
                    </label>
                    <div class="col-sm-10">
                        <input type="file" name="file">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <a id="updateAdvert" type="button" class="btn btn-primary">确 定</a>
                        <a class="btn btn-default" href="list?getSession=true">返 回</a>
                    </div>
                </div>
            </div>
        </div>
    </form>
</section>

</@adminAssembly.layout>