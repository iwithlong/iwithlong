<#import "../layout/admin-assembly.ftl" as adminAssembly>
<#assign importCss=[
"css/iwithlong.css"
]>

<@adminAssembly.layout importCss=importCss>

<section class="content container-fluid">
    <form id="mainForm" action="list" method="post" enctype="multipart/form-data">
        <input type="hidden" name="pageSize" value="${(advertVo.pageSize)!}"/>
        <input type="hidden" name="advertType" value="${(advertVo.advertType)}"/>

        <div class="box box-primary">
            <div class="box-header">
                <div class="box-tools">
                    <a href="add?advertType=${(advertVo.advertType)!}" class="btn btn-sm bg-olive btn-add">
                        <i class="<@spring.messageText code="admin.list.btn.add.ico" text="fa fa-plus"/>"
                           aria-hidden="true"></i> <@spring.messageText code="admin.list.btn.add.text" text="添加"/>
                    </a>
                    <a href="update" class="btn btn-sm bg-orange btn-update">
                        <i class="<@spring.messageText code="admin.list.btn.update.ico" text="fa fa-pencil"/>"
                           aria-hidden="true"></i> <@spring.messageText code="admin.list.btn.update.text" text="修改"/>
                    </a>
                    <a href="issue" class="btn btn-sm bg-orange btn-confirm"
                       msg="<@spring.messageText code="admin.list.btn.delete.confirm.msg" text="确认发布？"/>">
                        <i class="<@spring.messageText code="admin.list.btn.delete.ico" text="fa fa-trash"/>"
                           aria-hidden="true"></i> <@spring.messageText code="admin.list.btn.delete.text" text="发布"/>
                    </a>
                    <a href="unable" class="btn btn-sm bg-danger btn-confirm"
                       msg="<@spring.messageText code="admin.list.btn.delete.confirm.msg" text="确认禁用？"/>">
                        <i class="<@spring.messageText code="admin.list.btn.delete.ico" text="fa fa-trash"/>"
                        aria-hidden="true"></i> <@spring.messageText code="admin.list.btn.delete.text" text="禁用"/>
                    </a>
                    <a href="delete" class="btn btn-sm bg-danger btn-confirm"
                       msg="<@spring.messageText code="admin.list.btn.delete.confirm.msg" text="删除操作不可恢复，确认删除？"/>">
                        <i class="<@spring.messageText code="admin.list.btn.delete.ico" text="fa fa-trash"/>"
                           aria-hidden="true"></i> <@spring.messageText code="admin.list.btn.delete.text" text="删除"/>
                    </a>
                </div>
            </div>
            <div class="box-body table-responsive no-padding">
                <table class="table table-hover">
                    <tr>
                        <th><input type="checkbox" class="header-chk-all"></th>
                        <th>缩略图</th>
                        <th>标题</th>
                        <th>类型</th>
                        <th>备注</th>
                        <th>发布人</th>
                        <th>发布时间</th>
                        <th>发布状态</th>
                        <th>操作</th>
                    </tr>
                    <tbody>
                        <#list result as item>
                        <tr <#if item.status=="1">class="gray" </#if>>
                            <td><input name="advertId" type="checkbox" class="row-chk" value="${(item.advertId)!}"></td>
                            <td><img src="${rc.contextPath}/upload/get/image.json?url=${(item.image)!}" style="width: 80px;height: 80px;" class="img-responsivek" /></td>
                            <td>${(item.title)!}</td>
                            <td>
                                <@dickit dicKey="advert_biz_type">
                                    <#list dictionaries as dic>
                                    <#if item.bizType==dic.dicKey>${(dic.dicValue)!}</#if>
                                    </#list>
                                </@dickit>
                            </td>
                            <td>${(item.memo)!}</td>
                            <td>${(item.issuer)!}</td>
                            <td>${(item.gmtIssue?string("yyyy-MM-dd HH:mm:ss"))!}</td>
                            <td>
                                <@dickit dicKey="advert_status">
                                    <#list dictionaries as dic>
                                    <#if item.status==dic.dicKey>${(dic.dicValue)!}</#if>
                                </#list>
                                </@dickit>
                            </td>
                            <td>
                                <a href="top?advertId=${(item.advertId)!}" class="data-detail">置顶</a>
                                <a href="up?advertId=${(item.advertId)!}" class="data-detail">上移</a>
                                <a href="down?advertId=${(item.advertId)!}" class="data-detail">下移</a>
                                <a href="bottom?advertId=${(item.advertId)!}" class="data-detail">置底</a>
                            </td>
                        </tr>
                        </#list>
                    </tbody>
                </table>
            </div>
            <@adminAssembly.page pagination=page.pager uri="list"/>
        <#--<div th:include="admin/widgets/admin-widgets :: admin-page(${menuList.pager},@{/menu/list})"></div>-->
        </div>
    </form>
</section>

</@adminAssembly.layout>