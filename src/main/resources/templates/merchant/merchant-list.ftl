<#import "../layout/admin-assembly.ftl" as adminAssembly>
<#assign importjs=[
"js/merchant/merchant-list.js"
]>

<#assign importCss=[
"css/iwithlong.css"
]>

<@adminAssembly.layout importjavascript=importjs importCss=importCss>

<section class="content container-fluid">
    <form id="mainForm" action="list" method="post">
        <div class="box box-primary">
            <div class="box-body">
                <div class="form-group">
                    <div class="col-sm-4">
                        <label for="keyword" class="control-label col-sm-3">账户号</label>

                        <div class="col-sm-7">
                            <input type="text" id="accountCodeQuery" class="form-control" name="accountCode"
                                   value="${(merchantVo.accountCode)!}">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <label for+="keyword" class="control-label col-sm-3">账号名称</label>

                        <div class="col-sm-7">
                            <input type="text" id="accountNameQuery" class="form-control" name="accountName"
                                   value="${(merchantVo.accountName)!}">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <label for="status" class="col-sm-3">账号类型</label>

                        <div class="col-sm-7">
                            <select class="form-control" id="accountTypeQuery" name="accountType">
                                <option value="">请选择</option>
                                <@dickit dicKey="merchant_account_type">
                                    <#list dictionaries as dic>
                                        <#if merchantVo.accountType??>
                                            <#if merchantVo.accountType==dic.dicKey>
                                                <option value="${(dic.dicKey)!}" selected>${(dic.dicValue)!}</option>
                                            <#else >
                                                <option value="${(dic.dicKey)!}">${(dic.dicValue)!}</option>
                                            </#if>
                                        <#else >
                                            <option value="${(dic.dicKey)!}">${(dic.dicValue)!}</option>
                                        </#if>
                                    </#list>
                                </@dickit>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-4">
                        <label for="memberName" class="control-label col-sm-3">商户名</label>

                        <div class="col-sm-7">
                            <input type="text" class="form-control" id="memberNameQuery" name="memberName"
                                   value="${(merchantVo.memberName)!}">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <label for="nickName" class="control-label col-sm-3">商户昵称</label>

                        <div class="col-sm-7">
                            <input type="text" id="nickNameQuery" class="form-control" name="nickName"
                                   value="${(merchantVo.nickName)!}"/>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <input type="hidden" name="pageSize" value="${(merchantVo.pageSize)!}"/>
                        <input id="orderBy" type="hidden" name="orderBy" value="${(merchantVo.orderBy)!}">
                        <button id="data-query" type="submit" class="btn btn-primary">查询</button>
                        <button id="query-reset" type="button" class="btn btn-reddit">重置</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="box box-primary">
            <div class="box-header">
                <div class="box-tools">
                    <a href="add" class="btn btn-sm bg-olive btn-add">
                        <i class="<@spring.messageText code="admin.list.btn.add.ico" text="fa fa-plus"/>"
                           aria-hidden="true"></i> <@spring.messageText code="admin.list.btn.add.text" text="添加"/>
                    </a>
                    <a href="update" class="btn btn-sm bg-orange btn-update">
                        <i class="<@spring.messageText code="admin.list.btn.update.ico" text="fa fa-pencil"/>"
                           aria-hidden="true"></i> <@spring.messageText code="admin.list.btn.update.text" text="修改"/>
                    </a>
                    <a href="delete" class="btn btn-sm bg-danger btn-confirm"
                       msg="<@spring.messageText code="admin.list.btn.delete.confirm.msg" text="删除操作不可恢复，确认删除？"/>">
                        <i class="<@spring.messageText code="admin.list.btn.delete.ico" text="fa fa-trash"/>"
                           aria-hidden="true"></i> <@spring.messageText code="admin.list.btn.delete.text" text="删除"/>
                    </a>
                </div>
            </div>
            <div class="box-body table-responsive no-padding">
                <table class="table table-hover">
                    <tr>
                        <th><input type="checkbox" class="header-chk-all""></th>
                        <th>商家名</th>
                        <th>昵称</th>
                        <th>账户名</th>
                        <th>账户类型</th>
                        <th>账户号</th>
                        <th>客服电话</th>
                        <th>启用状态</th>
                        <th>操作</th>
                    </tr>
                    <tbody>
                        <#list result as item>
                        <tr <#if item.enable==false>class="gray" </#if>>
                            <td><input name="memberId" type="checkbox" class="row-chk" value="${(item.memberId)!}"></td>
                            <td>${(item.memberName)!}</td>
                            <td>${(item.nickName)!}</td>
                            <td>${(item.accountName)!}</td>
                            <td>
                                <@dickit dicKey="merchant_account_type">
                                    <#list dictionaries as dic>
                                    <#if item.accountType==dic.dicKey>${(dic.dicValue)!}</#if>
                                </#list>
                                </@dickit>
                            </td>
                            <td>${(item.accountCode)!}</td>
                            <td>${(item.serviceTel)!}</td>
                            <td>
                                <#if item.enable == true>
                                    已启用
                                <#else >
                                    已禁用
                                </#if>
                            </td>
                            <td>
                                <a href="detail?memberId=${(item.memberId)!}" class="data-detail">详情</a>
                                <a href="update?memberId=${(item.memberId)!}">修改</a>
                                <#if item.enable == true>
                                    <a data-id="${(item.memberId)!}" href="#" class="data-enable">禁用</a>
                                <#elseif item.enable == false>
                                    <a data-id="${(item.memberId)!}" href="#" class="data-enable">启用</a>
                                </#if>
                                <a href="#" class="deleteMerchant" data-id="${(item.memberId)!}">删除</a>
                            </td>
                        </tr>
                        </#list>
                    </tbody>
                </table>
            </div>
            <@adminAssembly.page pagination=page.pager uri="list"/>
        </div>
    </form>
</section>

</@adminAssembly.layout>