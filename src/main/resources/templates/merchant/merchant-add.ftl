<#import "../layout/admin-assembly.ftl" as adminAssembly>
<#assign importjs=[
"lib/js/bootstrapvalidate/bootstrapValidator.min.js",
"js/merchant/merchant-add.js"
]>
<#assign importCss=[
"lib/css/bootstrapvalidate/bootstrapValidator.min.css"
]>

<@adminAssembly.layout importjavascript=importjs importCss=importCss>

<section class="content container-fluid">
    <form id="addMerchantForm" role="form" class="form-horizontal" th:action="@{/b/merchant/add}" method="post">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">
                    <i class="<@spring.messageText code="admin.list.btn.add.ico" text="fa fa-plus"/>"
                       aria-hidden="true"></i>
                    添加商家
                </h3>
            </div>
            <div class="box-body">
                <div class="form-group">
                    <label for="memberName" class="col-sm-2 control-label">
                        商家名
                    </label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="memberName" name="memberName"
                               value="${(merchantVo.memberName)!}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="nickName" class="col-sm-2 control-label">
                        昵称
                    </label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="nickName" name="nickName"
                               value="${(merchantVo.nickName)!}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="intro" class="col-sm-2 control-label">
                        商家简介
                    </label>
                    <div class="col-sm-10">
                        <textarea class="form-control" id="intro" name="intro">${(merchantVo.intro)!}</textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label for="serviceTel" class="col-sm-2 control-label">
                        客服电话
                    </label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="serviceTel" name="serviceTel"
                               value="${(merchantVo.serviceTel)!}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="accountType" class="col-sm-2 control-label">
                        账号类型
                    </label>
                    <div class="col-sm-10">
                        <select class="form-control" id="accountType" name="accountType">
                            <@dickit dicKey="merchant_account_type">
                                <#list dictionaries as dic>
                                    <#if merchantVo??&&merchantVo.accountType??>
                                        <#if merchantVo.accountType==dic.dicKey>
                                            <option value="${(dic.dicKey)!}" selected>${(dic.dicValue)!}</option>
                                        <#else >
                                            <option value="${(dic.dicKey)!}">${(dic.dicValue)!}</option>
                                        </#if>
                                    <#else >
                                        <option value="${(dic.dicKey)!}">${(dic.dicValue)!}</option>
                                    </#if>
                                </#list>
                            </@dickit>
                        </select>
                    </div>
                </div>
                <div class="form-group">
                    <label for="accountName" class="col-sm-2 control-label">
                        账号名称
                    </label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="accountName" name="accountName"
                               value="${(merchantVo.accountName)!}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="accountCode" class="col-sm-2 control-label">
                        账号
                    </label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="accountCode" name="accountCode"
                               value="${(merchantVo.accountCode)!}">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <a id="addMerchant" type="button" class="btn btn-primary">确 定</a>
                        <a class="btn btn-default" href="list?getSession=true">返 回</a>
                    </div>
                </div>
            </div>
        </div>
    </form>
</section>

</@adminAssembly.layout>