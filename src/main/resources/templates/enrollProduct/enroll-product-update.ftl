<#import "../layout/admin-assembly.ftl" as adminAssembly>

<@adminAssembly.layout>

<section class="content container-fluid">
    <form id="updateEnrollProductForm" role="form" class="form-horizontal" action="${(rc.contextPath)}/b/enrollproduct/doupdate"
          method="post">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">
                    <i class="<@spring.messageText code="admin.list.btn.update.ico" text="fa fa-plus"/>"
                       aria-hidden="true"></i>
                    报名单分组管理（${(activityEnrollVo.title)}）
                </h3>
            </div>
            <div class="box-body">
                <input id="activityEnrollId" name="activityEnrollId" value="${(activityEnrollVo.activityEnrollId)!}"/>
                <#if enrollProductVos?? && enrollProductVos?size>0>
                    <#list enrollProductVos as enrollProductVo>
                    <div class="enrollProductDetail">
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">
                                分组名（免费）
                            </label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="enrollProductVos[${enrollProductVo_index}!].name"
                                       value="${(enrollProductVo.name)!}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="quantity" class="col-sm-2 control-label">
                                分组可报人数
                            </label>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" name="quantity"
                                       value="${(enrollProductVo.quantity)!}">
                            </div>
                            <label class="col-sm-2 control-label">
                                用户默认必填项
                            </label>
                            <div class="col-sm-3">
                                <input type="text" readonly class="form-control"
                                       value="姓名，电话，备注">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="memo" class="col-sm-2 control-label">
                                分组备注
                            </label>
                            <div class="col-sm-10">
                                <textarea class="form-control" name="memo">${(enrollProductVo.memo)!}</textarea>
                            </div>
                        </div>
                    </div>
                        </br>
                        <div class="col-sm-12" style="border-top: 1px solid #eee;"></div>
                    </#list>
                <#else>
                    <div class="enrollProductDetail">
                        <div class="form-group">
                            <label for="name" class="col-sm-2 control-label">
                                分组名（免费）
                            </label>
                            <div class="col-sm-10">
                                <input type="text" class="form-control" name="enrollProductVos[0].name"
                                       value="${(enrollProductVo.name)!}">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="quantity" class="col-sm-2 control-label">
                                分组可报人数
                            </label>
                            <div class="col-sm-3">
                                <input type="text" class="form-control" name="enrollProductVos[0].quantity"
                                       value="">
                            </div>
                            <label class="col-sm-2 control-label">
                                用户默认必填项
                            </label>
                            <div class="col-sm-3">
                                <input type="text" readonly class="form-control"
                                       value="姓名，电话，备注">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="memo" class="col-sm-2 control-label">
                                分组备注
                            </label>
                            <div class="col-sm-10">
                                <textarea class="form-control" name="enrollProductVos[0].memo"></textarea>
                            </div>
                        </div>
                    </div>
                    </br>
                    <div class="col-sm-12" style="border-top: 1px solid #eee;"></div>
                </#if>
                </br>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <a id="addMoreEnrollProduct" type="button" class="btn btn-primary">新增分组</a>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-primary">确 认</button>
                        <a class="btn btn-default" href="list?getSession=true">返回列表</a>
                    </div>
                </div>
            </div>
        </div>
    </form>
</section>

</@adminAssembly.layout>