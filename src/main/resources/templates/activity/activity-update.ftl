<#import "../layout/admin-assembly.ftl" as adminAssembly>
<#assign importjs=[
"lib/js/bootstrapfileinput/fileinput.min.js",
"lib/js/bootstrapfileinput/locales/zh.js",
"js/pic_upload.js",
"lib/js/datepicker/bootstrap-datetimepicker.js",
"ueditor/ueditor.config.js",
"ueditor/ueditor.all.js",
"js/init_ueditor.js",
"lib/js/jqueryui/jquery-ui.min.js",
"lib/js/bootstrapvalidate/bootstrapValidator.min.js",
"lib/js/magicsuggest/magicsuggest.js",
"js/activity/activity_update.js"
]>
<#assign importCss=[
"lib/css/datepicker/bootstrap-datetimepicker.css",
"lib/css/bootstrapfileinput/fileinput.min.css",
"ueditor/themes/default/css/ueditor.min.css",
"lib/css/bootstrapvalidate/bootstrapValidator.min.css",
"lib/css/jqueryui/jquery-ui.css",
"lib/css/magicsuggest/magicsuggest-min.css"
]>
<@adminAssembly.layout importjavascript=importjs importCss=importCss>

<section class="content container-fluid">
    <form id="updateForm" role="form" class="form-horizontal" th:action="@{/b/activity/update}" method="post">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">
                    <i class="<@spring.messageText code="admin.list.btn.add.ico" text="fa fa-plus"/>"
                       aria-hidden="true"></i>
                    修改活动
                </h3>
            </div>
            <input type="hidden" name="activityId" value="${(activityVo.activityId)!}">
            <div class="box-body">
                <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">
                        标题
                    </label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="name" name="name"
                               value="${(activityVo.name)!}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">
                        所属活动点
                    </label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="activityAreaVoName" name="activityAreaVo.areaName"
                               value="${(activityVo.activityAreaVo.areaName)!}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="brief" class="col-sm-2 control-label">
                        简述
                    </label>
                    <div class="col-sm-10">
                        <textarea class="form-control" id="brief" name="brief">${(activityVo.brief)!}</textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label for="ticketUrl" class="col-sm-2 control-label">
                        链接地址
                    </label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="ticketUrl" name="ticketUrl"
                               value="${(activityVo.ticketUrl)!}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="charging" class="col-sm-2 control-label">
                        收费情况
                    </label>
                    <div class="col-sm-3">
                        <select name="charging" class="form-control">
                            <option value="">请选择</option>
                            <#if activityVo??&&(activityVo.charging=="0")>
                                <option value="0" selected>免费</option>
                                <option value="1">收费</option>
                            <#elseif activityVo??&&(activityVo.charging=="1")>
                                <option value="0">免费</option>
                                <option value="1" selected>收费</option>
                            <#else >
                                <option value="0">免费</option>
                                <option value="1">收费</option>
                            </#if>
                        </select>
                    </div>
                    <label class="col-sm-2 control-label">
                        推荐值
                    </label>
                    <div class="col-sm-3">
                        <input type="number" step="0.1" class="form-control" id="recommended" name="recommended" placeholder="输入1～5的整数"
                               value="${(activityVo.recommended)!}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="beginDate" class="col-sm-2 control-label">
                        开始时间
                    </label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" id="beginDate date-picker" readonly name="beginDate"
                               value="${(activityVo.beginDate?string("yyyy-MM-dd HH:mm"))!}">
                    </div>
                    <label for="endDate" class="col-sm-2 control-label">
                        结束时间
                    </label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control date-picker" id="endDate" readonly name="endDate"
                               value="${(activityVo.endDate?string("yyyy-MM-dd HH:mm"))!}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="telephone" class="col-sm-2 control-label">
                        联系电话
                    </label>
                    <div class="col-sm-3">
                        <input class="form-control" id="telephone" name="telephone" value="${(activityVo.telephone)!}"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">
                        标签
                    </label>
                    <div class="col-sm-6">
                        <div class="activityLabelSuggest"></div>
                        <input class="form-control" id="activityLabel" type="hidden" name="activityLabel"
                               value="${(activityVo.activityLabel)!}">
                    </div>
                    <div class="col-sm-4">
                        <a href="${rc.contextPath}/a/label/add">没有想要的标签？快速新建</a>
                    </div>
                </div>
                <input type="hidden" name="uploadFileIds" value=""/>
                <div class="form-group">
                    <input id="containerInitValue" type="hidden" name="content" value="">
                    <label for="areaIntroduction" class="col-sm-2 control-label">
                        活动详情
                    </label>
                    <div class="col-sm-10">
                        <script id="container" type="text/plain">
                            ${(activityVo.content)!}
                        </script>
                    </div>
                </div>
                <div id="uploadedPic" style="display: none">
                    <#if activityVo??>
                        <#list activityVo.uploadFileVos as uploadFileVo>
                            <input data-file-name="${uploadFileVo.fileName}" data-key="${uploadFileVo_index}" data-id="${(uploadFileVo.uploadFileId!)}" class="initPic" type="hidden" value="${rc.contextPath}/upload/get/image.json?url=${(uploadFileVo.fileUrl)!}" />
                        </#list>
                    </#if>

                </div>
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label">
                        图片
                    </label>
                    <div class="col-sm-10">
                        <input type="file" name="multipartFile" multiple class="picUpload"/>
                        <p class="help-block">支持jpg、jpeg、png、gif格式，大小不超过2.0M</p>
                    </div>
                </div>
                <input id="uploadFileIds" name="uploadFileIds" type="hidden"/>
                <#if needAudit==true>
                    <div class="form-group">
                        <label for="auditor" class="col-sm-2 control-label">
                            审核人
                        </label>
                        <div class="col-sm-3">
                            <input type="text" readonly="readonly" class="form-control" id="auditor" name="auditor" value="${(activityVo.auditor)!}">
                        </div>
                        <label for="gmtAudit" class="col-sm-2 control-label">
                            审核时间
                        </label>
                        <div class="col-sm-3">
                            <input type="text" readonly="readonly" class="form-control" id="gmtAudit" name="gmtAudit" value="${(activityVo.gmtAudit?string("yyyy-MM-dd HH:mm:ss"))!}">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="oldStatus" class="col-sm-2 control-label">
                            审核意见
                        </label>
                        <div class="col-sm-10">
                            <input type="text" readonly="readonly" class="form-control" id="reason" name="reason" value="${(activityVo.reason)!}">
                        </div>
                    </div>
                </#if>
                <div class="form-group">
                    <label for="creator" class="col-sm-2 control-label">
                        创建人
                    </label>
                    <div class="col-sm-3">
                        <input type="text" readonly="readonly" class="form-control" id="creator" name="creator" value="${(activityVo.creator)!}">
                    </div>
                    <label for="gmtCreate" readonly="readonly" class="col-sm-2 control-label">
                        创建时间
                    </label>
                    <div class="col-sm-3">
                        <input type="text" readonly="readonly" class="form-control" id="gmtCreate" name="gmtCreate" value="${(activityVo.gmtCreate?string("yyyy-MM-dd HH:mm:ss"))!}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="modifier" class="col-sm-2 control-label">
                        修改人
                    </label>
                    <div class="col-sm-3">
                        <input type="text" readonly="readonly" class="form-control" id="modifier" name="modifier" value="${(activityVo.modifier)!}">
                    </div>
                    <label for="gmtModify" class="col-sm-2 control-label">
                        修改时间
                    </label>
                    <div class="col-sm-3">
                        <input type="text" readonly="readonly" class="form-control" id="gmtModify" name="gmtModify" value="${(activityVo.gmtModify?string("yyyy-MM-dd HH:mm:ss"))!}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="issuer" class="col-sm-2 control-label">
                        发布人
                    </label>
                    <div class="col-sm-3">
                        <input type="text" readonly="readonly" class="form-control" id="issuer" name="issuer" value="${(activityVo.issuer)!}">
                    </div>
                    <label for="gmtIssuer" class="col-sm-2 control-label">
                        发布时间
                    </label>
                    <div class="col-sm-3">
                        <input type="text" readonly="readonly" class="form-control" id="gmtIssuer" name="gmtIssuer" value="${(activityVo.gmtIssuer?string("yyyy-MM-dd HH:mm:ss"))!}">
                    </div>
                </div>
                <input id="status" type="hidden" name="status" value=""/>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="button" class="btn btn-primary save">保存</button>
                        <#if needAudit==true>
                            <button type="button" class="btn btn-primary audit">提交审核</button>
                        <#else>
                            <button type="button" class="btn btn-primary issuer">发布</button>
                        </#if>
                        <a class="btn btn-default" href="list">返 回</a>
                    </div>
                </div>
            </div>
        </div>
    </form>
</section>

</@adminAssembly.layout>