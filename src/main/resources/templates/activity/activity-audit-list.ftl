<#import "../layout/admin-assembly.ftl" as adminAssembly>

<@adminAssembly.layout>

<section class="content container-fluid">
    <form id="mainForm" action="list" method="post">
        <div class="box box-primary">
            <div class="box-body table-responsive no-padding">
                <table class="table table-hover">
                    <tr>
                        <th><input type="checkbox" class="header-chk-all""></th>
                        <th>缩略图</th>
                        <th>标题</th>
                        <th>活动点</th>
                        <th>活动时间</th>
                        <th>提交人</th>
                        <th>提交时间</th>
                        <th>活动状态</th>
                        <#--<th>启用状态</th>-->
                        <th>操作</th>
                    </tr>
                    <tbody>
                        <#list result as item>
                        <tr>
                            <td><input name="activityId" type="checkbox" class="row-chk" value="${(item.activityId)!}"></td>
                            <td>
                                <#list item.uploadFileVos as uploadFileVo>
                                    <#if uploadFileVo_index==0>
                                        <img src="${rc.contextPath}/upload/get/image.json?url=${(uploadFileVo.fileUrl)!}" style="width: 80px;height: 80px;" class="img-responsivek" />
                                    </#if>
                                </#list>
                            </td>
                            <td>${(item.name)!}</td>
                            <td>${(item.activityAreaVo.areaName)!}</td>
                            <td>${(item.beginDate?string("yyyy-MM-dd HH:mm:ss"))!}</td>
                            <#--<td>${(item.creator)!}</td>-->
                            <#--<td>${(item.gmtCreate?string("yyyy-MM-dd"))!}</td>-->
                            <td>${(item.modifier)!}</td>
                            <td>${(item.gmtModify?string("yyyy-MM-dd hh:mm"))!}</td>
                            <#--<td>${(item.auditor)!}</td>-->
                            <#--<td>${(item.gmtAuditor?string("yyyy-MM-dd"))!}</td>-->
                            <#--<td>${(item.issuer)!}</td>-->
                            <#--<td>${(item.gmtIssue?string("yyyy-MM-dd"))!}</td>-->
                            <td>
                                <@dickit dicKey="activity_status">
                                    <#list dictionaries as dic>
                                    <#if item.status==dic.dicKey>${(dic.dicValue)!}</#if>
                                </#list>
                                </@dickit>
                            </td>
                            <#--<td><#if item.enable==true>-->
                                <#--已启用-->
                            <#--<#else>-->
                                <#--已禁用-->
                            <#--</#if></td>-->
                            <td>
                                <#--<a href="goaudit?activityId=${(item.activityId)!}" class="data-detail">查看</a>-->
                                <a href="goaudit?activityId=${(item.activityId)!}" class="data-detail">审核</a>
                            </td>
                        </tr>
                        </#list>
                    </tbody>
                </table>
            </div>
            <@adminAssembly.page pagination=page.pager uri="list"/>
        <#--<div th:include="admin/widgets/admin-widgets :: admin-page(${menuList.pager},@{/menu/list})"></div>-->
        </div>
    </form>
</section>

</@adminAssembly.layout>