<#import "../layout/admin-assembly.ftl" as adminAssembly>
<#assign importjs=[
"lib/js/bootstrapfileinput/fileinput.min.js",
"lib/js/bootstrapfileinput/locales/zh.js",
"js/pic_upload.js",
"lib/js/datepicker/bootstrap-datetimepicker.js",
"ueditor/ueditor.config.js",
"ueditor/ueditor.all.js",
"js/init_ueditor.js",
"lib/js/jqueryui/jquery-ui.min.js",
"lib/js/bootstrapvalidate/bootstrapValidator.min.js",
"lib/js/magicsuggest/magicsuggest.js",
"js/activity/activity_add.js"
]>
<#assign importCss=[
"lib/css/datepicker/bootstrap-datetimepicker.css",
"lib/css/bootstrapfileinput/fileinput.min.css",
"ueditor/themes/default/css/ueditor.min.css",
"lib/css/bootstrapvalidate/bootstrapValidator.min.css",
"lib/css/jqueryui/jquery-ui.css",
"lib/css/magicsuggest/magicsuggest-min.css"
]>

<@adminAssembly.layout importjavascript=importjs importCss=importCss>

<section class="content container-fluid">
    <form id="addActivityForm" role="form" class="form-horizontal" th:action="@{/b/activity/add}" method="post">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">
                    <i class="<@spring.messageText code="admin.list.btn.add.ico" text="fa fa-plus"/>"
                       aria-hidden="true"></i>
                    添加活动
                </h3>
            </div>
            <div class="box-body">
                <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">
                        标题
                    </label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="name" name="name"
                               value="${(activityVo.name)!}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">
                        所属活动点
                    </label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="activityAreaVoName" name="activityAreaVo.areaName"
                               value="${(activityVo.activityAreaVo.areaName)!}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="brief" class="col-sm-2 control-label">
                        简述
                    </label>
                    <div class="col-sm-10">
                        <textarea class="form-control" id="brief" name="brief">${(activityVo.brief)!}</textarea>
                    </div>
                    <#--<div class="col-sm-10">-->
                        <#--<input type="text" class="form-control" id="brief" name="brief"-->
                               <#--value="${(activityVo.brief)!}">-->
                    <#--</div>-->
                </div>
                <div class="form-group">
                    <label for="ticketUrl" class="col-sm-2 control-label">
                        链接地址
                    </label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="ticketUrl" name="ticketUrl"
                               value="${(activityVo.ticketUrl)!}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="charging" class="col-sm-2 control-label">
                        收费情况
                    </label>
                    <div class="col-sm-3">
                        <#--<input type="text" class="form-control" id="charging" name="charging"-->
                               <#--value="${(activityVo.charging)!}">-->
                        <select name="charging" class="form-control">
                            <option value="">请选择</option>
                            <#if activityVo??&&(activityVo.charging=="0")>
                                <option value="0" selected>免费</option>
                                <option value="1">收费</option>
                            <#elseif activityVo??&&(activityVo.charging=="1")>
                                <option value="0">免费</option>
                                <option value="1" selected>收费</option>
                            <#else >
                                <option value="0">免费</option>
                                <option value="1">收费</option>
                            </#if>
                        </select>
                    </div>
                    <label class="col-sm-2 control-label">
                        推荐值
                    </label>
                    <div class="col-sm-3">
                        <input type="number" step="0.1" class="form-control" id="recommended" name="recommended" placeholder="请输入1～5的"
                               value="${(activityVo.recommended)!}">
                    </div>
                </div>

                <div class="form-group">
                    <label for="beginDate" class="col-sm-2 control-label">
                        开始时间
                    </label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control date-picker" id="beginDate" name="beginDate"
                               readonly value="${(activityVo.beginDate?string("yyyy-MM-dd HH:mm"))!}">
                    </div>
                    <label for="endDate" class="col-sm-2 control-label">
                        结束时间
                    </label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control date-picker" id="endDate" name="endDate"
                                readonly value="${(activityVo.endDate?string("yyyy-MM-dd HH:mm"))!}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="telephone" class="col-sm-2 control-label">
                        联系电话
                    </label>
                    <div class="col-sm-3">
                        <input class="form-control" id="telephone" name="telephone" value="${(activityVo.telephone)!}"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">
                        标签
                    </label>
                    <div class="col-sm-6">
                        <div class="activityLabelSuggest"></div>
                        <input class="form-control" id="activityLabel" type="hidden" name="activityLabel"
                               value="${(activityVo.activityLabel)!}">
                    </div>
                    <div class="col-sm-4">
                        <a href="${rc.contextPath}/a/label/add">没有想要的标签？快速新建</a>
                    </div>
                </div>
                <div class="form-group">
                    <input id="containerInitValue" type="hidden" name="content" value="">
                    <label for="areaIntroduction" class="col-sm-2 control-label">
                        活动详情
                    </label>
                    <div class="col-sm-10">
                        <script id="container" type="text/plain">
                            ${(activityVo.content)!}
                        </script>
                    </div>
                </div>
                <div id="uploadedPic" style="display: none">
                    <#if activityVo??>
                        <#list activityVo.uploadFileVos as uploadFileVo>
                            <input data-file-name="${uploadFileVo.fileName}" data-key="${uploadFileVo_index}" data-id="${(uploadFileVo.uploadFileId!)}" class="initPic" type="hidden" value="${rc.contextPath}/upload/get/image.json?url=${(uploadFileVo.fileUrl)!}" />
                        </#list>
                    </#if>

                </div>
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label">
                        图片
                    </label>
                    <div class="col-sm-10">
                        <input type="file" name="multipartFile" multiple class="picUpload"/>
                        <p class="help-block">支持jpg、jpeg、png、gif格式，大小不超过2.0M</p>
                    </div>
                </div>
                <input id="uploadFileIds" name="uploadFileIds" type="hidden"/>
                <input id="addStatus" name="status" type="hidden"/>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <a id="addActivity" type="button" class="btn btn-primary">确 定</a>
                        <#if needAudit==true>
                            <a id="auditActivity" type="button" class="btn btn-primary">提交审核</a>
                        </#if>
                        <a class="btn btn-default" href="list">返 回</a>
                    </div>
                </div>
            </div>
        </div>
    </form>
</section>

</@adminAssembly.layout>