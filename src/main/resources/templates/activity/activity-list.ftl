<#import "../layout/admin-assembly.ftl" as adminAssembly>
<#assign importjs=[
"lib/js/datepicker/bootstrap-datetimepicker.js",
"lib/js/jqueryui/jquery-ui.min.js",
"js/activity/activity_list.js"
]>

<#assign importCss=[
"lib/css/datepicker/bootstrap-datetimepicker.css",
"lib/css/jqueryui/jquery-ui.css",
"css/iwithlong.css"
]>

<@adminAssembly.layout importjavascript=importjs importCss=importCss>

<section class="content container-fluid">
    <form id="mainForm" action="list" method="post">
        <div class="box box-primary">
            <div class="box-body">
                <div class="form-group">
                    <div class="col-sm-4">
                        <label for="keyword" class="control-label col-sm-3">关键词</label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control" id="keyword" name="keyword"
                                   value="${(activityVo.keyword)!}">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <label for="status" class="col-sm-3">发布状态</label>
                        <div class="col-sm-7">
                            <select class="form-control" id="status" name="status">
                                <option value="">请选择</option>
                                <@dickit dicKey="activity_area_status">
                                    <#list dictionaries as dic>
                                        <#if activityVo.status??>
                                            <#if activityVo.status==dic.dicKey>
                                                <option value="${(dic.dicKey)!}" selected>${(dic.dicValue)!}</option>
                                            <#else >
                                                <option value="${(dic.dicKey)!}">${(dic.dicValue)!}</option>
                                            </#if>
                                        <#else >
                                            <option value="${(dic.dicKey)!}">${(dic.dicValue)!}</option>
                                        </#if>
                                    </#list>
                                </@dickit>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <label for="enableSearch" class="col-sm-3">启动状态</label>
                        <div class="col-sm-7">
                            <select class="form-control" id="enableSearch" name="enableSearch">
                                <option value="">请选择</option>
                                <#if activityVo.enableSearch??>
                                    <#if activityVo.enableSearch=="0">
                                        <option value="0" selected>禁用</option>
                                        <option value="1">启用</option>
                                    <#elseif activityVo.enableSearch=="1">
                                        <option value="0">禁用</option>
                                        <option value="1" selected>启用</option>
                                    <#else >
                                        <option value="0">禁用</option>
                                        <option value="1">启用</option>
                                    </#if>
                                <#else >
                                    <option value="0">禁用</option>
                                    <option value="1">启用</option>
                                </#if>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-4">
                        <label for="activityAreaVo.areaName" class="control-label col-sm-3">活动点名称</label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control" id="areaName" name="activityAreaVo.areaName"
                                   value="${(activityVo.activityAreaVo.areaName)!}">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <label for="modifier" class="control-label col-sm-3">修改人</label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control" id="creator" name="modifier"
                                   value="${(activityVo.modifier)!}">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <label for="issuer" class="control-label col-sm-3">发布人</label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control" id="issuer" name="issuer"
                                   value="${(activityVo.issuer)!}">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-4">
                        <label for="" class="control-label col-sm-3">开始时间从</label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control date-picker" id="beginDateStart" name="beginDateStart"
                                   value="${(activityVo.beginDateStart?string("yyyy-MM-dd HH:mm"))!}">                        </div>
                    </div>
                    <div class="col-sm-4">
                        <label for="" class="control-label col-sm-3">至</label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control date-picker" id="beginDateEnd" name="beginDateEnd"
                                   value="${(activityVo.beginDateEnd?string("yyyy-MM-dd HH:mm"))!}">                        </div>
                    </div>
                    <div class="col-sm-4">
                        <label for="" class="control-label col-sm-3">推荐值</label>
                        <div class="col-sm-3">
                            <input type="number" step="0" class="form-control" id="recommendedStart" name="recommendedStart"
                                   value="${(activityVo.recommendedStart)!}">
                        </div>
                        <div class="col-sm-1">
                            <span>~</span>
                        </div>
                        <div class="col-sm-3">
                            <input type="number" step="0" class="form-control" id="recommendedEnd" name="recommendedEnd"
                                   value="${(activityVo.recommendedEnd)!}">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-4">
                        <label for="" class="control-label col-sm-3">结束时间从</label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control date-picker" id="endDateStart" name="endDateStart"
                                   value="${(activityVo.endDateStart?string("yyyy-MM-dd HH:mm"))!}">                        </div>
                    </div>
                    <div class="col-sm-4">
                        <label for="" class="control-label col-sm-3">至</label>
                        <div class="col-sm-7">
                            <input type="text" class="form-control date-picker" id="endDateEnd" name="endDateEnd"
                                   value="${(activityVo.endDateEnd?string("yyyy-MM-dd HH:mm"))!}">                        </div>
                    </div>
                    <#--<div class="col-sm-4">-->
                        <#--<label for="" class="control-label col-sm-3">排序方式</label>-->
                        <#--<div class="col-sm-7">-->
                            <#--<select class="form-control" id="orderBy" name="orderBy">-->
                                <#--<option value="">默认</option>-->
                                <#--<#if activityVo.orderBy??>-->
                                    <#--<#if activityVo.orderBy=="numberOfClick">-->
                                        <#--<option value="numberOfClick" selected>点击量</option>-->
                                        <#--<option value="numberOfInterest">收藏量</option>-->
                                    <#--<#elseif activityVo.orderBy=="numberOfInterest">-->
                                        <#--<option value="numberOfClick">点击量</option>-->
                                        <#--<option value="numberOfInterest" selected>收藏量</option>-->
                                    <#--<#else >-->
                                        <#--<option value="numberOfClick">点击量</option>-->
                                        <#--<option value="numberOfInterest">收藏量</option>-->
                                    <#--</#if>-->
                                <#--<#else >-->
                                    <#--<option value="numberOfClick">点击量</option>-->
                                    <#--<option value="numberOfInterest">收藏量</option>-->
                                <#--</#if>-->
                            <#--</select>-->
                        <#--</div>-->
                    <#--</div>-->
                    <div class="col-sm-4">
                    <#--<input type="hidden" name="pageNum" value="${(activityVo.pageNum)!}">-->
                        <input type="hidden" name="pageSize" value="${(activityVo.pageSize)!}"/>
                        <input id="orderByType" type="hidden" name="orderByType" value="${(activityVo.orderByType)!}">
                        <input id="orderBy" type="hidden" name="orderBy" value="${(activityVo.orderBy)!}">
                        <button id="data-query" type="submit" class="btn btn-primary">查询</button>
                        <button id="query-reset" type="button" class="btn btn-reddit">重置</button>
                    </div>
                </div>
                <#--<div class="form-group">-->
                    <#--<div class="col-sm-4"></div>-->
                    <#--<div class="col-sm-4">-->
                    <#--&lt;#&ndash;<input type="hidden" name="pageNum" value="${(activityVo.pageNum)!}">&ndash;&gt;-->
                        <#--<input type="hidden" name="pageSize" value="${(activityVo.pageSize)!}">-->
                        <#--<button id="data-query" type="submit" class="btn btn-primary">查询</button>-->
                        <#--<button id="query-reset" type="button" class="btn btn-reddit">重置</button>-->
                    <#--</div>-->
                <#--</div>-->
            </div>
        </div>
        <div class="box box-primary">
            <div class="box-header">
                <div class="box-tools">
                    <a href="add" class="btn btn-sm bg-olive btn-add">
                        <i class="<@spring.messageText code="admin.list.btn.add.ico" text="fa fa-plus"/>"
                           aria-hidden="true"></i> <@spring.messageText code="admin.list.btn.add.text" text="添加"/>
                    </a>
                    <a href="update" class="btn btn-sm bg-orange btn-update">
                        <i class="<@spring.messageText code="admin.list.btn.update.ico" text="fa fa-pencil"/>"
                           aria-hidden="true"></i> <@spring.messageText code="admin.list.btn.update.text" text="修改"/>
                    </a>
                    <a href="delete" class="btn btn-sm bg-danger btn-confirm"
                       msg="<@spring.messageText code="admin.list.btn.delete.confirm.msg" text="删除操作不可恢复，确认删除？"/>">
                        <i class="<@spring.messageText code="admin.list.btn.delete.ico" text="fa fa-trash"/>"
                           aria-hidden="true"></i> <@spring.messageText code="admin.list.btn.delete.text" text="删除"/>
                    </a>

                <#--<div class="btn-group">-->
                <#--<button class="btn btn-sm dropdown-toggle bg-navy" type="button" data-toggle="dropdown"-->
                <#--aria-haspopup="true" aria-expanded="false">-->
                <#--<i class="fa fa fa-cog" aria-hidden="true"></i>-->
                <#--更多 <span class="caret"></span>-->
                <#--</button>-->
                <#--<ul class="dropdown-menu">-->
                <#--<li><a href="#">Action</a></li>-->
                <#--<li><a href="#">Another action</a></li>-->
                <#--<li><a href="#">Something else here</a></li>-->
                <#--<li role="separator" class="divider"></li>-->
                <#--<li><a href="#">Separated link</a></li>-->
                <#--</ul>-->
                <#--</div>-->
                </div>
            </div>
            <div class="box-body table-responsive no-padding">
                <table class="table table-hover">
                    <tr>
                        <th><input type="checkbox" class="header-chk-all""></th>
                        <th>缩略图</th>
                        <th>标题</th>
                        <th>活动点</th>
                        <th>活动时间</th>
                        <th id="numberOfClickOrder">点击量<span>
                            <#if activityVo.orderBy??&&"numberOfClick"==activityVo.orderBy >
                                <#if activityVo.orderByType=="asc">
                                    ∧
                                <#elseif activityVo.orderByType=="desc">
                                    ∨
                                </#if>
                            </#if>
                        </span></th>
                        <th id="numberOfInterestOrder">收藏量<span><#if activityVo.orderBy??&&activityVo.orderBy=='numberOfInterest'><#if activityVo.orderByType=='asc'>∧<#elseif activityVo.orderByType=='desc'>∨</#if></#if></span></th>
                        <#--<th>标签</th>-->
                        <#--<th>创建人</th>-->
                        <#--<th>创建时间</th>-->
                        <th>修改人</th>
                        <th>修改时间</th>
                        <#--<#if needAudit==true>-->
                            <#--<th>审核人</th>-->
                            <#--<th>审核时间</th>-->
                        <#--</#if>-->
                        <th>发布人</th>
                        <th>发布时间</th>
                        <th>活动状态</th>
                        <#--<th>启用状态</th>-->
                        <th>操作</th>
                    </tr>
                    <tbody>
                        <#list result as item>
                        <tr <#if item.enable==false>class="gray" </#if>>
                            <td><input name="activityId" type="checkbox" class="row-chk" value="${(item.activityId)!}"></td>
                            <td>
                                <#list item.uploadFileVos as uploadFileVo>
                                    <#if uploadFileVo_index==0>
                                        <img src="${rc.contextPath}/upload/get/image.json?url=${(uploadFileVo.fileUrl)!}" style="width: 80px;height: 80px;" class="img-responsivek" />
                                    </#if>
                                </#list>
                            </td>
                            <td>${(item.name)!}<#if item.activityLabel??&&item.activityLabel!=''>【${(item.activityLabel)!}】</#if></td>
                            <td>${(item.activityAreaVo.areaName)!}</td>
                            <td>${(item.beginDate?string("yyyy-MM-dd HH:mm:ss"))!}</td>
                            <td>${(item.numberOfClick)!}</td>
                            <td>${(item.numberOfInterest)!}</td>
                            <#--<td>${(item.activityLabel)!}</td>-->
                            <#--<td>${(item.creator)!}</td>-->
                            <#--<td>${(item.gmtCreate?string("yyyy-MM-dd"))!}</td>-->
                            <td>${(item.modifier)!}</td>
                            <td>${(item.gmtModify?string("MM-dd hh:mm"))!}</td>
                            <#--<#if needAudit==true>-->
                            <#--<td>${(item.auditor)!}</td>-->
                            <#--<td>${(item.gmtAuditor?string("yyyy-MM-dd"))!}</td>-->
                            <#--</#if>-->
                            <td>${(item.issuer)!}</td>
                            <td>${(item.gmtIssue?string("MM-dd hh:mm"))!}</td>
                            <td>
                                <@dickit dicKey="activity_status">
                                    <#list dictionaries as dic>
                                    <#if item.status==dic.dicKey>${(dic.dicValue)!}</#if>
                                </#list>
                                </@dickit>
                            </td>
                            <#--<td><#if item.enable==true>-->
                                <#--已启用-->
                            <#--<#else>-->
                                <#--已禁用-->
                            <#--</#if></td>-->
                            <td>
                                <a href="detail?activityId=${(item.activityId)!}" class="data-detail">详情</a>
                                <a href="update?activityId=${(item.activityId)}">修改</a>
                                <#--<a href="#">预览</a>-->
                                <#--<#if item.status=='2'>-->
                                    <#--<a href="/a/activity/audit/goaudit?activityId=${(item.activityId)!}">审核</a>-->
                                <#--</#if>-->
                                <#if item.status=='4'>
                                    <a href="#" class="goIssuer" data-id="${(item.activityId)!}">发布</a>
                                </#if>
                                <#if item.enable == true>
                                    <a data-id="${(item.activityId)!}" href="#" class="data-enable">禁用</a>
                                <#elseif item.enable == false>
                                    <a data-id="${(item.activityId)!}" href="#" class="data-enable">启用</a>
                                </#if>
                            </td>
                        </tr>
                        </#list>
                    </tbody>
                </table>
            </div>
            <@adminAssembly.page pagination=page.pager uri="list"/>
        <#--<div th:include="admin/widgets/admin-widgets :: admin-page(${menuList.pager},@{/menu/list})"></div>-->
        </div>
    </form>
</section>

<div id="goIssuerForm" style="display: none;">
    <form id="issueForm" action="issue" class="form-horizontal" method="post">
        <input type="hidden" name="activityId" id="issuerActivityId">
        <div class="form-group">
            <label class="col-sm-2 control-label">
                发布类型
            </label>
            <div class="col-sm-9">
                <input type="hidden" id="isNow" name="isNow" value="true"/>
                <select id="issueType" class="form-control">
                    <option value="true" selected>立即发布</option>
                    <option value="false">定时发布</option>
                </select>
            </div>
        </div>
        <div class="form-group gmtIssueDiv" hidden>
            <label class="col-sm-2 control-label">
                发布时间
            </label>
            <div class="col-sm-9">
                <input type="text" class="form-control date-picker" id="gmtIssueInput" name="gmtIssue"
                       readonly value="">
            </div>
        </div>
    </form>
</div>

</@adminAssembly.layout>