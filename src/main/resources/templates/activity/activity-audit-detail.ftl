<#import "../layout/admin-assembly.ftl" as adminAssembly>
<#assign importjs=[
"lib/js/bootstrapfileinput/fileinput.min.js",
"lib/js/bootstrapfileinput/locales/zh.js",
"js/pic_upload.js",
"ueditor/ueditor.config.js",
"ueditor/ueditor.all.js",
"js/init_ueditor.js",
"js/activity/activity_audit.js"
]>

<#assign importCss=[
"lib/css/bootstrapfileinput/fileinput.min.css",
"ueditor/themes/default/css/ueditor.min.css"
]>

<@adminAssembly.layout importjavascript=importjs importCss=importCss>

<section class="content container-fluid">
    <form role="form" class="form-horizontal" method="post">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">
                    <i class="<@spring.messageText code="admin.list.btn.add.ico" text="fa fa-plus"/>"
                       aria-hidden="true"></i>
                    修改活动
                </h3>
            </div>
            <input type="hidden" name="activityId" value="${(activityVo.activityId)!}">
            <div class="box-body">
                <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">
                        标题
                    </label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="name" name="name"
                               value="${(activityVo.name)!}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">
                        所属活动点
                    </label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="activityAreaVoName" name="activityAreaVo.areaName"
                               value="${(activityVo.activityAreaVo.areaName)!}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="brief" class="col-sm-2 control-label">
                        简述
                    </label>
                    <div class="col-sm-10">
                        <textarea class="form-control" id="brief" name="brief">${(activityVo.brief)!}</textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label for="ticketUrl" class="col-sm-2 control-label">
                        链接地址
                    </label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="ticketUrl" name="ticketUrl"
                               value="${(activityVo.ticketUrl)!}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="charging" class="col-sm-2 control-label">
                        收费情况
                    </label>
                    <div class="col-sm-3">
                        <select readonly="" name="charging" class="form-control">
                            <#if activityVo??&&(activityVo.charging=="0")>
                                <option value="0" selected>免费</option>
                            <#elseif activityVo??&&(activityVo.charging=="1")>
                                <option value="1" selected>收费</option>
                            <#else >
                                <option value="" selected>请选择</option>
                            </#if>
                        </select>
                    </div>
                    <label class="col-sm-2 control-label">
                        推荐值
                    </label>
                    <div class="col-sm-3">
                        <input type="number" class="form-control" id="recommended" name="recommended" placeholder="输入1～5的整数"
                               value="${(activityVo.recommended)!}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="beginDate" class="col-sm-2 control-label">
                        开始时间
                    </label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" id="beginDate" name="beginDate"
                               readonly class="datePicker" value="${(activityVo.beginDate?string("yyyy-MM-dd HH:mm"))!}">
                    </div>
                    <label for="endDate" class="col-sm-2 control-label">
                        结束时间
                    </label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control" id="endDate" name="endDate"
                               readonly class="datePicker" value="${(activityVo.endDate?string("yyyy-MM-dd HH:mm"))!}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="telephone" class="col-sm-2 control-label">
                        联系电话
                    </label>
                    <div class="col-sm-3">
                        <input class="form-control" id="telephone" name="telephone" value="${(activityVo.telephone)!}"/>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">
                        标签
                    </label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="activityLabel" name="activityLabel"
                               value="${(activityVo.activityLabel)!}">
                    </div>
                </div>

                <div class="form-group">
                    <label for="areaIntroduction" class="col-sm-2 control-label">
                        活动详情
                    </label>
                    <div class="col-sm-10">
                        <script id="container" type="text/plain">
                            ${(activityVo.content)!}
                        </script>
                    </div>
                </div>
                <div id="uploadedPic" style="display: none">
                    <#if activityVo??>
                        <#list activityVo.uploadFileVos as uploadFileVo>
                            <input data-file-name="${uploadFileVo.fileName}" data-key="${uploadFileVo_index}" data-id="${(uploadFileVo.uploadFileId!)}" class="initPic" type="hidden" value="${rc.contextPath}/upload/get/image.json?url=${(uploadFileVo.fileUrl)!}" />
                        </#list>
                    </#if>

                </div>
                <div class="form-group">
                    <label for="" class="col-sm-2 control-label">
                        图片
                    </label>
                    <div class="col-sm-10">
                        <input readonly type="file" name="multipartFile" multiple class="picUpload"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="auditor" class="col-sm-2 control-label">
                        审核人
                    </label>
                    <div class="col-sm-3">
                        <input type="text" readonly="readonly" class="form-control" id="auditor" name="auditor" value="${(activityVo.auditor)!}">
                    </div>
                    <label for="gmtAudit" class="col-sm-2 control-label">
                        审核时间
                    </label>
                    <div class="col-sm-3">
                        <input type="text" readonly="readonly" class="form-control" id="gmtAudit" name="gmtAudit" value="${(activityVo.gmtAudit?string("yyyy-MM-dd HH:mm:ss"))!}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="oldStatus" class="col-sm-2 control-label">
                        上次审核意见
                    </label>
                    <div class="col-sm-10">
                        <input type="text" readonly="readonly" class="form-control" id="reason" name="reason" value="${(activityVo.reason)!}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="creator" class="col-sm-2 control-label">
                        创建人
                    </label>
                    <div class="col-sm-3">
                        <input type="text" readonly="readonly" class="form-control" id="creator" name="creator" value="${(activityVo.creator)!}">
                    </div>
                    <label for="gmtCreate" readonly="readonly" class="col-sm-2 control-label">
                        创建时间
                    </label>
                    <div class="col-sm-3">
                        <input type="text" readonly="readonly" class="form-control" id="gmtCreate" name="gmtCreate" value="${(activityVo.gmtCreate?string("yyyy-MM-dd HH:mm:ss"))!}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="modifier" class="col-sm-2 control-label">
                        修改人
                    </label>
                    <div class="col-sm-3">
                        <input type="text" readonly="readonly" class="form-control" id="modifier" name="modifier" value="${(activityVo.modifier)!}">
                    </div>
                    <label for="gmtModify" class="col-sm-2 control-label">
                        修改时间
                    </label>
                    <div class="col-sm-3">
                        <input type="text" readonly="readonly" class="form-control" id="gmtModify" name="gmtModify" value="${(activityVo.gmtModify?string("yyyy-MM-dd HH:mm:ss"))!}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="issuer" class="col-sm-2 control-label">
                        发布人
                    </label>
                    <div class="col-sm-3">
                        <input type="text" readonly="readonly" class="form-control" id="issuer" name="issuer" value="${(activityVo.issuer)!}">
                    </div>
                    <label for="gmtIssuer" class="col-sm-2 control-label">
                        发布时间
                    </label>
                    <div class="col-sm-3">
                        <input type="text" readonly="readonly" class="form-control" id="gmtIssuer" name="gmtIssuer" value="${(activityVo.gmtIssuer?string("yyyy-MM-dd HH:mm:ss"))!}">
                    </div>
                </div>
                <input id="status" type="hidden" name="status" value=""/>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button id="auditPass" type="button" class="btn btn-primary">审核通过</button>
                        <button id="auditNotPass" type="button" class="btn btn-danger">审核不通过</button>
                        <a class="btn btn-default" href="list">返 回</a>
                    </div>
                </div>
            </div>
        </div>
    </form>
</section>

<div id="auditRefuseDiv" style="display: none;">
    <form id="auditForm" class="form-horizontal" action="doaudit" method="post">
        <div class="form-group">
            <label class="col-sm-2 control-label">
                常见原因
            </label>
            <div class="col-sm-7">
                <select id="normalReason" class="form-control">
                    <@dickit dicKey="activity_reasonlist">
                        <#list dictionaries as dic>
                            <option value="${(dic.dicKey)!}">${(dic.dicValue)!}</option>
                        </#list>
                    </@dickit>
                </select>
            </div>
            <div class="col-sm-3">
                <a class="btn btn-adn" id="addReason">添加</a>
            </div>
        </div>
        <div class="form-group">
            <label for="reason" class="col-sm-2 control-label">
                原因
            </label>
            <div class="col-sm-8">
                <textarea type="text" class="form-control" id="resultReason"
                          name="reason">${(activityVo.reason)!}</textarea>
            </div>
        </div>
        <input id="auditStatus" type="hidden" name="status" value=""/>
        <input id="auditActivityId" type="hidden" name="activityId" value="${(activityVo.activityId)!}"/>
    </form>
</div>

</@adminAssembly.layout>