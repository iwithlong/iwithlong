<#import "../layout/admin-assembly.ftl" as adminAssembly>
<@adminAssembly.layout>

<section class="content container-fluid">
    <form id="addMerchantForm" role="form" class="form-horizontal" method="post">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">
                    <i class="<@spring.messageText code="admin.list.btn.add.ico" text="fa fa-plus"/>"
                       aria-hidden="true"></i>
                    报名名单详情
                </h3>
            </div>
            <div class="box-body">
                <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">
                        姓名
                    </label>

                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="name" name="name"
                               value="${(result.name)!}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="nickName" class="col-sm-2 control-label">
                        电话
                    </label>

                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="mobile" name="mobile"
                               value="${(result.mobile)!}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="attendance" class="col-sm-2 control-label">
                        出勤率
                    </label>

                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="attendance" name="attendance"
                               value="${(result.attendance?string.percent)!}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="status" class="col-sm-2 control-label" value="">
                        状态
                    </label>

                    <div class="col-sm-10">
                        <input type="text" class="form-control" value="${(result.status)!}"/>
                    </div>
                </div>
                <div class="form-group">
                    <label for="nickName" class="col-sm-2 control-label">
                        报名时间
                    </label>

                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="mobile" name="mobile"
                               value="${(result.gmtEnroll)!}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="memo" class="col-sm-2 control-label">
                        备注
                    </label>
                    <div class="col-sm-10">
                        <textarea class="form-control" name="intro">${(result.memo)!}</textarea>
                    </div>
                </div>
                <div class="form-group">
                    <label for="title" class="col-sm-2 control-label">
                        报名单标题
                    </label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="title" name="title"
                               value="${(result.activityEnrollVo.title)!}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="gmtEnd" class="col-sm-2 control-label">
                        报名截止时间
                    </label>
                    <div class="col-sm-3">
                        <input type="text" class="form-control date-picker" id="gmtEnd" name="gmtEnd"
                               value="${(result.activityEnrollVo.gmtEnd?string("yyyy-MM-dd hh:mm"))!}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="productName" class="col-sm-2 control-label">
                        分组名称
                    </label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="productName"
                               value="${(result.enrollProductVo.name)!}">
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-sm-2 control-label">
                        缴费信息
                    </label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control"
                               value="免费">
                    </div>
                </div>
                <div class="form-group">
                    <label for="productMemo" class="col-sm-2 control-label">
                        分组注意事项
                    </label>
                    <div class="col-sm-10">
                        <textarea class="form-control" name="productMemo">${(result.enrollProductVo.memo)!}</textarea>
                    </div>
                </div>
                <#list result.productSmsVos as productSmsVo>
                    <div class="form-group">
                        <label for="productMemo" class="col-sm-2 control-label">
                            短信记录
                        </label>
                        <div class="col-sm-10">
                            <textarea class="form-control" name="productMemo">您已成功报名${(productSmsVo.params[0])!}，时间:${(productSmsVo.params[1])!}，地点：${(productSmsVo.params[2])!}，请准时参加，现场签到，其他注意事项以报名页面提示内容为准，如有特殊情况需取消报名，请联系${(productSmsVo.params[3])!}，无故缺席将计入出勤率，影响之后的活动报名。备注：${(productSmsVo.params[4])!}</textarea>
                        </div>
                    </div>
                </#list>

                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <a class="btn btn-default" href="list?getSession=true">返 回</a>
                    </div>
                </div>
            </div>
        </div>
    </form>
</section>
</@adminAssembly.layout>