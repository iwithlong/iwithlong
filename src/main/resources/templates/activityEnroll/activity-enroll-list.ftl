<#import "../layout/admin-assembly.ftl" as adminAssembly>
<#assign importjs=[
"lib/js/jqueryui/jquery-ui.min.js",
"js/activityenroll/activity-enroll-list.js"
]>

<#assign importCss=[
"css/iwithlong.css",
"lib/css/jqueryui/jquery-ui.css"
]>

<@adminAssembly.layout importjavascript=importjs importCss=importCss>

<section class="content container-fluid">
    <form id="mainForm" action="list" method="post">
        <div class="box box-primary">
            <div class="box-body">
                <div class="form-group">
                    <div class="col-sm-4">
                        <label for="keyword" class="control-label col-sm-3">商家昵称</label>

                        <div class="col-sm-7">
                            <input type="text" id="nickNameQuery" class="form-control" name="merchantVo.nickName"
                                   value="${(activityEnrollVo.merchantVo.nickName)!}">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <label for="keyword" class="control-label col-sm-3">活动名</label>

                        <div class="col-sm-7">
                            <input type="text" id="activityNameQuery" class="form-control" name="activityVo.name"
                                   value="${(activityEnrollVo.activityVo.name)!}">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <label for="keyword" class="control-label col-sm-3">报名单</label>

                        <div class="col-sm-7">
                            <input type="text" id="titleQuery" class="form-control" name="title"
                                   value="${(activityEnrollVo.title)!}">
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-4">
                        <label for="status" class="col-sm-3">报名状态</label>
                        <div class="col-sm-7">
                            <select class="form-control" id="status" name="status">
                                <option value="">请选择</option>
                                <@dickit dicKey="activity_enroll_status">
                                    <#list dictionaries as dic>
                                        <#if activityEnrollVo.status??>
                                            <#if activityEnrollVo.status==dic.dicKey>
                                                <option value="${(dic.dicKey)!}" selected>${(dic.dicValue)!}</option>
                                            <#else >
                                                <option value="${(dic.dicKey)!}">${(dic.dicValue)!}</option>
                                            </#if>
                                        <#else >
                                            <option value="${(dic.dicKey)!}">${(dic.dicValue)!}</option>
                                        </#if>
                                    </#list>
                                </@dickit>
                            </select>
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <input type="hidden" name="pageSize" value="${(activityEnrollVo.pageSize)!}"/>
                        <input id="orderBy" type="hidden" name="orderBy" value="${(activityEnrollVo.orderBy)!}">
                        <button id="data-query" type="submit" class="btn btn-primary">查询</button>
                        <button id="query-reset" type="button" class="btn btn-reddit">重置</button>
                    </div>
                    <div class="col-sm-4">
                    </div>
                </div>
            </div>
        </div>
        <div class="box box-primary">
            <div class="box-header">
                <div class="box-tools">
                    <a href="add" class="btn btn-sm bg-olive btn-add">
                        <i class="<@spring.messageText code="admin.list.btn.add.ico" text="fa fa-plus"/>"
                           aria-hidden="true"></i> <@spring.messageText code="admin.list.btn.add.text" text="添加"/>
                    </a>
                    <a href="update" class="btn btn-sm bg-orange btn-update">
                        <i class="<@spring.messageText code="admin.list.btn.update.ico" text="fa fa-pencil"/>"
                           aria-hidden="true"></i> <@spring.messageText code="admin.list.btn.update.text" text="修改"/>
                    </a>
                    <a href="complete" class="btn btn-sm bg-orange btn-confirm" msg="报名单完成代表改活动签到已经结束，签到功能将无法使用，确认完成？">
                        <i class="<@spring.messageText code="admin.list.btn.update.ico" text="fa fa-pencil"/>"
                           aria-hidden="true"></i>完成
                    </a>
                    <a href="delete" class="btn btn-sm bg-danger btn-confirm"
                       msg="<@spring.messageText code="admin.list.btn.delete.confirm.msg" text="删除操作不可恢复，确认删除？"/>">
                        <i class="<@spring.messageText code="admin.list.btn.delete.ico" text="fa fa-trash"/>"
                           aria-hidden="true"></i> <@spring.messageText code="admin.list.btn.delete.text" text="删除"/>
                    </a>
                </div>
            </div>
            <div class="box-body table-responsive no-padding">
                <table class="table table-hover">
                    <tr>
                        <th><input type="checkbox" class="header-chk-all"/></th>
                        <th>报名单</th>
                        <th>活动</th>
                        <th>商家用户名</th>
                        <th>商家昵称</th>
                        <th>报名启动时间</th>
                        <th>报名截止时间</th>
                        <th>活动开始时间</th>
                        <th>活动结束时间</th>
                        <th>状态</th>
                        <th>操作</th>
                    </tr>
                    <tbody>
                        <#list result as item>
                        <tr <#if item.status??&&item.status=="unable">class="gray" </#if><#if item.status??&&item.status=="complete">class="gray" </#if>>
                            <td><input name="activityEnrollId" type="checkbox" class="row-chk" value="${(item.activityEnrollId)!}"></td>
                            <td>${(item.title)!}</td>
                            <td>${(item.activityVo.name)!}</td>
                            <td>${(item.merchantVo.memberCode)!}</td>
                            <td>${(item.merchantVo.nickName)!}</td>
                            <td>${(item.gmtBegin?string("yyyy-MM-dd HH:mm:ss"))!}</td>
                            <td>${(item.gmtEnd?string("yyyy-MM-dd HH:mm:ss"))!}</td>
                            <td>${(item.gmtActivityStart?string("yyyy-MM-dd HH:mm:ss"))!}</td>
                            <td>${(item.gmtActivityEnd?string("yyyy-MM-dd HH:mm:ss"))!}</td>
                            <td>
                                <@dickit dicKey="activity_enroll_status">
                                    <#list dictionaries as dic>
                                        <#if item.status??&&item.status==dic.dicKey>${(dic.dicValue)!}</#if>
                                    </#list>
                                </@dickit>
                            </td>
                            <td>
                                <a href="detail/list?activityEnrollId=${(item.activityEnrollId)!}" class="data-detail">详情</a>
                                <#if item.status == "unable">
                                    <a href="update?activityEnrollId=${(item.activityEnrollId)!}">修改</a>
                                    <a data-id="${(item.activityEnrollId)!}" href="#" class="data-enable">启用</a>
                                </#if>
                                <#if item.status?? && item.status == "complete">
                                <#else>
                                    <a href="#" class="deleteActivityEnroll" data-id="${(item.activityEnrollId)!}">删除</a>
                                </#if>
                            </td>
                        </tr>
                        </#list>
                    </tbody>
                </table>
            </div>
            <@adminAssembly.page pagination=page.pager uri="list"/>
        </div>
    </form>
</section>

</@adminAssembly.layout>