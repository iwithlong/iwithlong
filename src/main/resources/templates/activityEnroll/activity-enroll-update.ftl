<#import "../layout/admin-assembly.ftl" as adminAssembly>
<#assign importjs=[
"lib/js/bootstrapvalidate/bootstrapValidator.min.js",
"js/activityenroll/activity-enroll-update.js",
"lib/js/datepicker/bootstrap-datetimepicker.js",
"lib/js/jqueryui/jquery-ui.min.js"
]>
<#assign importCss=[
"lib/css/bootstrapvalidate/bootstrapValidator.min.css",
"lib/css/jqueryui/jquery-ui.css",
"lib/css/datepicker/bootstrap-datetimepicker.css"
]>

<@adminAssembly.layout importjavascript=importjs importCss=importCss>

<section class="content container-fluid">
    <form id="updateActivityEnrollForm" role="form" class="form-horizontal" action="${(rc.contextPath)}/b/activityenroll/update"
          method="post">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">
                    <i class="<@spring.messageText code="admin.list.btn.update.ico" text="fa fa-plus"/>"
                       aria-hidden="true"></i>
                    报名单信息修改
                </h3>
            </div>
            <div class="box-body">
                <div class="form-group">
                    <input type="hidden" id="activityEnrollId" name="activityEnrollId"
                           value="${(activityEnrollVo.activityEnrollId)!}"/>
                    <label for="merchantVo.memberName" class="col-sm-2 control-label">
                        商家
                    </label>

                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="memberName" name="merchantVo.memberName"
                               value="${(activityEnrollVo.merchantVo.memberName)!}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="activityVo.activityName" class="col-sm-2 control-label">
                        活动名称
                    </label>

                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="activityName" name="activityVo.name"
                               value="${(activityEnrollVo.activityVo.name)!}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="title" class="col-sm-2 control-label">
                        标题
                    </label>

                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="title" name="title"
                               value="${(activityEnrollVo.title)!}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="address" class="col-sm-2 control-label">
                        活动地点
                    </label>

                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="address" name="address"
                               value="${(activityEnrollVo.address)!}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="activityVo.startTime" class="col-sm-2 control-label">
                        活动开始时间
                    </label>

                    <div class="col-sm-3">
                        <input type="text" class="form-control date-picker" readonly id="startTime" name="gmtActivityStart"
                               value="${(activityEnrollVo.gmtActivityStart?string("yyyy-MM-dd hh:mm"))!}">
                    </div>
                    <label for="activityVo.endDate" class="col-sm-2 control-label">
                        活动结束时间
                    </label>

                    <div class="col-sm-3">
                        <input type="text" readonly class="form-control date-picker" id="endTime" name="gmtActivityEnd"
                               value="${(activityEnrollVo.gmtActivityEnd?string("yyyy-MM-dd hh:mm"))!}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="gmtEnd" class="col-sm-2 control-label">
                        报名截止时间
                    </label>

                    <div class="col-sm-3">
                        <input type="text" class="form-control date-picker" readonly id="gmtEnd" name="gmtEnd"
                               value="${(activityEnrollVo.gmtEnd?string("yyyy-MM-dd hh:mm"))!}">
                    </div>
                </div>
                <div class="form-group">
                    <label for="address" class="col-sm-2 control-label">
                        注意事项
                    </label>

                    <div class="col-sm-10">
                        <textarea class="form-control" id="intro" name="memo">${(activityEnrollVo.memo)!}</textarea>
                    </div>
                </div>
                </br>
                <div class="col-sm-12" style="border-top: 1px solid #eee;"></div>
                </br>
                <#if activityEnrollVo??&&activityEnrollVo.enrollProductVos??>
                    <input id="enrollProductSize" type="hidden" value="${(activityEnrollVo.enrollProductVos?size)!}">
                    <#list activityEnrollVo.enrollProductVos as enrollProductVo>
                        <div class="enrollProductDetail">
                            <div class="form-group">
                                <label for="name" class="col-sm-2 control-label">
                                    分组名（免费）
                                </label>

                                <div class="col-sm-10">
                                    <input type="text" class="form-control"
                                           name="enrollProductVos[${enrollProductVo_index}].name"
                                           value="${(enrollProductVo.name)!}">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="quantity" class="col-sm-2 control-label">
                                    分组可报人数
                                </label>

                                <div class="col-sm-3">
                                    <input type="number" step="1" min="1" class="form-control quantity"
                                           name="enrollProductVos[${enrollProductVo_index}].quantity"
                                           value="${(enrollProductVo.quantity)!}">
                                </div>
                                <label class="col-sm-2 control-label">
                                    用户默认必填项
                                </label>

                                <div class="col-sm-3">
                                    <input type="text" readonly class="form-control"
                                           value="姓名，电话，备注">
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="memo" class="col-sm-2 control-label">
                                    注意事项
                                </label>

                                <div class="col-sm-10">
                                    <textarea class="form-control"
                                              name="enrollProductVos[${enrollProductVo_index}].memo">${(enrollProductVo.memo)!}</textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <a type="button" class="btn btn-danger deleteEnrollProduct"
                                       data-ord="${enrollProductVo_index}">删除分组</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-sm-12" style="border-top: 1px solid #eee;"></div>
                    </#list>
                </#if>
                <input type="hidden" value="false" id="addProduct" name="addProduct"/>
                <input type="hidden" value="false" id="deleteProduct" name="deleteProduct"/>
                <input type="hidden" value="0" id="deleteOrd" name="deleteOrd"/>

                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <a id="addMoreEnrollProduct" type="button" class="btn bg-green">新增分组</a>
                        <a id="updateActivityEnroll" type="button" class="btn btn-primary">确 定</a>
                        <a class="btn btn-default" href="list?getSession=true">返 回</a>
                    </div>
                </div>
            </div>
        </div>
    </form>
</section>

</@adminAssembly.layout>