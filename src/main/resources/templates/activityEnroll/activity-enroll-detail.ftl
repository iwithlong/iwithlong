<#import "../layout/admin-assembly.ftl" as adminAssembly>
<#assign importjs=[
"lib/js/jqueryui/jquery-ui.min.js",
"js/activityenroll/activity-enroll-detail.js"
]>

<#assign importCss=[
"css/iwithlong.css",
"lib/css/jqueryui/jquery-ui.css"
]>

<@adminAssembly.layout importjavascript=importjs importCss=importCss>

<section class="content container-fluid">
    <form id="mainForm" action="list" method="post">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3 class="box-title">
                ${(activityEnrollVo.title)!}
                </h3>
                <input type="hidden" name="activityEnrollId" value="${(enrollNamelistVo.activityEnrollId)!}"/>
            </div>
        </div>
        <div class="box box-primary">
            <div class="box-body">
                <div class="form-group">
                    <label for="name" class="col-sm-2 control-label">
                        报名情况
                    </label>
                    <div class="col-sm-10">
                        <#list activityEnrollVo.enrollProductVos as item>
                            <div class="form-group col-sm-12">
                                <label class="col-sm-1 control-label">
                                    分组名
                                </label>
                                <div class="col-sm-3">
                                    <input type="text" disabled class="form-control" value="${(item.name)!}"/>
                                </div>
                                <label class="col-sm-2 control-label">
                                    已报名/总票数
                                </label>
                                <div class="col-sm-3">
                                    <input type="text" disabled class="form-control" value="${(item.enrolledQuantity)!}/${(item.quantity)!}"/>
                                </div>
                                <div class="col-sm-1">
                                    <a href="sendsmsbyproduct?productId=${(item.enrollProductId)!}" type="button" class="btn btn-primary">群发短信</a>
                                </div>
                            </div>
                        </#list>
                    </div>
                </div>
            </div>
        </div>
        <div class="box box-primary">
            <div class="box-body">
                <div class="form-group">
                    <div class="col-sm-4">
                        <label for="name" class="control-label col-sm-3">姓名</label>

                        <div class="col-sm-7">
                            <input type="text" id="nameQuery" class="form-control" name="name"
                                   value="${(enrollNamelistVo.name)!}">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <label for="mobile" class="control-label col-sm-3">电话</label>

                        <div class="col-sm-7">
                            <input type="text" id="mobileQuery" class="form-control" name="mobile"
                                   value="${(enrollNamelistVo.mobile)!}">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <label for="keyword" class="control-label col-sm-3">状态</label>

                        <div class="col-sm-7">
                            <select class="form-control" id="status" name="status">
                                <option value="">请选择</option>
                                <@dickit dicKey="enroll_namelist_status">
                                    <#list dictionaries as dic>
                                        <#if enrollNamelistVo.status??>
                                            <#if enrollNamelistVo.status==dic.dicKey>
                                                <option value="${(dic.dicKey)!}" selected>${(dic.dicValue)!}</option>
                                            <#else >
                                                <option value="${(dic.dicKey)!}">${(dic.dicValue)!}</option>
                                            </#if>
                                        <#else >
                                            <option value="${(dic.dicKey)!}">${(dic.dicValue)!}</option>
                                        </#if>
                                    </#list>
                                </@dickit>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-4">
                        <label for="status" class="col-sm-3">分组名</label>

                        <div class="col-sm-7">
                            <input type="text" id="enrollProductNameQuery" class="form-control" name="enrollProductName"
                                   value="${(enrollNamelistVo.enrollProductName)!}">
                        </div>
                    </div>
                    <div class="col-sm-4">
                        <input type="hidden" name="pageSize" value="${(enrollNamelistVo.pageSize)!}"/>
                        <input id="orderBy" type="hidden" name="orderBy" value="${(enrollNamelistVo.orderBy)!}">
                        <button id="data-query" type="submit" class="btn btn-primary">查询</button>
                        <button id="query-reset" type="button" class="btn btn-reddit">重置</button>
                    </div>
                    <div class="col-sm-4">
                    </div>
                </div>
            </div>
        </div>
        <div class="box box-primary">
            <div class="box-header">
                <div class="box-tools">
                    <a href="sign" class="btn btn-sm bg-olive btn-confirm"
                       msg="确认签到？">
                        <i class="<@spring.messageText code="admin.list.btn.update.ico" text="fa fa-pencil"/>"
                           aria-hidden="true"></i>签到
                    </a>
                    <a href="cancel" class="btn btn-sm bg-danger btn-confirm"
                       msg="<@spring.messageText code="admin.list.btn.delete.confirm.msg" text="取消并退款后无法撤回，确认取消？"/>">
                        <i class="<@spring.messageText code="admin.list.btn.delete.ico" text="fa fa-trash"/>"
                           aria-hidden="true"></i> <@spring.messageText code="admin.list.btn.delete.text" text="取消并退款"/>
                    </a>
                    <a href="sendsms" class="btn btn-sm bg-olive btn-confirm"
                       msg="确认发送短信？">
                        <i class="<@spring.messageText code="admin.list.btn.update.ico" text="fa fa-pencil"/>"
                           aria-hidden="true"></i>群发短信
                    </a>
                    <a class="btn btn-sm bg-orange" id="export">导出excel</a>
                </div>
            </div>
            <div class="box-body table-responsive no-padding">
                <table class="table table-hover">
                    <tr>
                        <th><input type="checkbox" class="header-chk-all""></th>
                        <th>姓名</th>
                        <th>电话</th>
                        <th>出勤率</th>
                        <th>报名时间</th>
                        <th>状态</th>
                        <th>分组</th>
                        <th>操作</th>
                    </tr>
                    <tbody>
                        <#list result as item>
                        <tr <#if item.status??&&item.status=="unable">class="gray" </#if>>
                            <td><input name="enrollNamelistId" type="checkbox" class="row-chk"
                                       value="${(item.enrollNamelistId)!}"></td>
                            <td>${(item.name)!}</td>
                            <td>${(item.mobile)!}</td>
                            <td>${(item.attendance?string.percent)!}</td>
                            <td>${(item.gmtEnroll?string("yyyy-MM-dd HH:mm:ss"))!}</td>
                            <td>
                                <@dickit dicKey="enroll_namelist_status">
                                    <#list dictionaries as dic>
                                    <#if item.status??&&item.status==dic.dicKey>${(dic.dicValue)!}</#if>
                                </#list>
                                </@dickit>
                            </td>
                            <td>${(item.enrollProductName)!}</td>
                            <td>
                                <a href="detail?enrollNamelistId=${(item.enrollNamelistId)!}" class="data-detail">用户详情</a>
                            </td>
                        </tr>
                        </#list>
                    </tbody>
                </table>
            </div>
            <@adminAssembly.page pagination=page.pager uri="list"/>
        </div>
    </form>
</section>

</@adminAssembly.layout>