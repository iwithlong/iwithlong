/**
 * Created by xiexiaohan on 2017/11/2.
 */
var ue;
$(function () {
    ue = UE.getEditor('container',{
        // //为编辑器实例添加一个路径，这个不能被注释
        UEDITOR_HOME_URL: "https://www.ichangbanmini.com"+'/ueditor/',
        toolbars:[[
            'Undo', 'Redo','|',
            'fontfamily', 'fontsize','forecolor','|','Bold','italic','underline','removeformat','formatmatch','blockquote','rowspacingtop','rowspacingbottom','|',
            'justifyleft', 'justifycenter', 'justifyright', 'justifyjustify', '|',
            'horizontal','link'
            ,'simpleupload','insertvideo'
        ]],
        initialFrameHeight:300
    });
})