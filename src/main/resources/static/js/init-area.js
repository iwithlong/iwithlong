/**
 * Created by xiexiaohan on 2017/10/16.
 */
$(function () {
    var provinceDate;
    $.post(contextPath+'/get/area/map.json',{},function (result) {
        provinceDate = result.result.provinceList;
        if($('.provinceSelect').val()==''){
            initNull=true;
            $('.provinceSelect').val('330000');
            $('.provinceSelect').trigger("change");

        }
    });
    var cityDataList;
    $('.provinceSelect').change( function (e) {
        $('.townSelect').empty().append($('<option value="" selected>请选择</option>'));
        var target = $(e.target);
        var province = target.val();
        $('.citySelect').empty().append($('<option value="" selected>请选择</option>'));
        for(var i=0;i<provinceDate.length;i++){
            if(provinceDate[i].code==province){
                cityDataList = provinceDate[i].childrenList;
                for(var j=0;j<cityDataList.length;j++){
                    $('.citySelect').append($('<option value="'+cityDataList[j].code+'">'+cityDataList[j].name+'</option>'));
                }
                break;
            }
        }
        if(initNull){
            $('.citySelect').val('330100');
            $('.citySelect').trigger("change");
            initNull=false;
        }
    });

    $('.citySelect').change(function (e) {
        $('.townSelect').empty().append($('<option value="" selected>请选择</option>'));
        var target = $(e.target);
        var city = target.val();
        for(var i=0;i<cityDataList.length;i++){
            if(cityDataList[i].code==city){
                var townDataList = cityDataList[i].childrenList;
                for(var j=0;j<townDataList.length;j++){
                    $('.townSelect').append($('<option value="'+townDataList[j].code+'">'+townDataList[j].name+'</option>'));
                }
                break;
            }
        }
    })

    var initNull=false;



});