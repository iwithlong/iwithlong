$(function(){
    $('#notPass').on('click',function(){
        layer.open({
            type:1,
            title:'驳回理由',
            area:'900px',
            content:$('#auditRefuseDiv'),
            btn:['确认','取消'],
            yes:function (index) {
                $('#auditForm').submit();
                layer.close(index);
            }
        })
    })
});