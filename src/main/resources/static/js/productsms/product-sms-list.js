$(function(){
    $('#activityEnrollName').autocomplete({
        minlengh:1,delay:500,source : function (request,response) {
            var query = $('#activityEnrollName').val();
            $.post(contextPath+"/b/activityenroll/namefilter.json",{title:query,pageSize:8},function (result) {
                var data = [];
                var merchantList = result.result;
                for(var i=0;i<merchantList.length;i++){
                    data.push(merchantList[i].title);
                }
                response($.ui.autocomplete.filter(data,request.term));
            })
        },focus : function () {
            return false;
        },select:function (event,ui) {
            $('#activityEnrollName').val(ui.item.value);
            return false;
        }
    });
});//商家名匹配
