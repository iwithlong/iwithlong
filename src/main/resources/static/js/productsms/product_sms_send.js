/**
 * Created by xiexiaohan on 2017/10/20.
 */
$(function () {
    $('.date-picker').datetimepicker({
        language: "en",
        autoclose: true,
        todayHighlight: true,
        // locale: moment.locale('zh-cn'),
        format: "mm月dd日hh:ii"//日期格式
    });
    $('#addProductSms').on('click',function () {
        $('#addProductSmsForm').bootstrapValidator('validate');
    });

    $('#addProductSmsForm').bootstrapValidator({
        feedbackIcons: {/*input状态样式图片*/
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {/*验证：规则*/
            title:{
                validators: {
                    notEmpty: {
                        message: '标题不能为空'
                    },stringLength: {
                        max: 20,
                        message: '标题名称长度应小于20'
                    }
                }
            },
            'params[0]':{
                validators: {
                    notEmpty: {
                        message: '活动名不能为空'
                    },stringLength: {
                        max: 20,
                        message: '活动名长度应小于20'
                    }
                }
            },
            'params[1]':{
                validators: {
                    notEmpty: {
                        message: '活动时间不能为空'
                    },stringLength: {
                        max: 20,
                        message: '活动时间格式不对'
                    }
                }
            },
            'params[2]':{
                validators: {
                    notEmpty: {
                        message: '活动地点不能为空'
                    },stringLength: {
                        max: 20,
                        message: '活动地点长度应小于20'
                    }
                }
            },
            'params[3]':{
                validators: {
                    notEmpty: {
                        message: '联系电话不能为空'
                    },stringLength: {
                        max: 20,
                        message: '联系电话长度应小于20'
                    }
                }
            },
            'params[4]':{
                validators: {
                    notEmpty: {
                        message: '备注不能为空'
                    },stringLength: {
                        max: 20,
                        message: '备注长度应小于20'
                    }
                }
            }

        }
    }).on('success.form.bv',function () {
        document.getElementById("addProductSmsForm").submit();
    });
});