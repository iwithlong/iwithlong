$(function () {


	/**
	 * 会员启用
	 */
	$(".data-enable").on("click",function () {

		var memberId=$(this).parents("tr").find("input[name='memberId']").val();

		$.post(contextPath+'/a/member/enable.json',{id:memberId},function (result) {
			showErrorMsg( result.message );
			setTimeout(function(){
				location.reload();
			}, 1000);
		});
	});

	/**
	 * 会员禁用
	 */
	$(".data-disable").on("click",function () {

		var memberId=$(this).parents("tr").find("input[name='memberId']").val();

		$.post(contextPath+'/a/member/disable.json',{id:memberId},function (result) {
			showErrorMsg( result.message );
			setTimeout(function(){
				location.reload();
			}, 1000);
		});
	});


	/**
	 * 会员删除
	 */
	$(".data-delete").on("click",function () {

		var memberId=$(this).parents("tr").find("input[name='memberId']").val();
		layer.confirm('确定删除？', {
			icon:2,
			btn: ['确定','取消']
		}, function(){
			$.post(contextPath+'/a/member/delete.json',{id:memberId},function (result) {
				showErrorMsg( result.message );
				setTimeout(function(){
					location.reload();
				}, 1000);
			});
		});

	})



});