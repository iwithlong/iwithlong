$(function () {
    $('.date-picker').datetimepicker({
        language: "en",
        autoclose: true,
        todayHighlight: true,
        // locale: moment.locale('zh-cn'),
        format: "yyyy-mm-dd hh:ii"//日期格式
    });

    $('#gmtEnd').on('change',function(){
        var bootstrapValidator = $("#addActivityEnrollForm").data('bootstrapValidator');
        bootstrapValidator.updateStatus('gmtEnd', 'NOT_VALIDATED').validateField('gmtEnd');
    });

    $('#updateActivityEnroll').on('click', function () {
        $('#updateActivityEnrollForm').bootstrapValidator('validate');
    });

    var activityLocalList;

    //商家名匹配
    $('#memberName').autocomplete({
        minlengh:1,delay:500,source : function (request,response) {
            var query = $('#memberName').val();
            $.post(contextPath+"/b/merchant/namefilter.json",{memberName:query,pageSize:8},function (result) {
                var data = [];
                var merchantList = result.result;
                for(var i=0;i<merchantList.length;i++){
                    data.push(merchantList[i].memberName);
                }
                response($.ui.autocomplete.filter(data,request.term));
            })
        },focus : function () {
            return false;
        },select:function (event,ui) {
            $('#memberName').val(ui.item.value);
            var bootstrapValidator = $("#updateActivityEnrollForm").data('bootstrapValidator');
            bootstrapValidator.updateStatus('merchantVo.memberName', 'NOT_VALIDATED').validateField('merchantVo.memberName');
            return false;
        }
    });

    //活动名匹配
    $('#activityName').autocomplete({
        minlengh:1,delay:500,source : function (request,response) {
            var query = $('#activityName').val();
            $.post(contextPath+"/b/activity/namefilter.json",{name:query,pageSize:8},function (result) {
                var data = [];
                var activityList = result.result;
                activityLocalList={};
                for(var i=0;i<activityList.length;i++){
                    data.push(activityList[i].name);
                    activityLocalList[activityList[i].name]=activityList[i];
                }
                response($.ui.autocomplete.filter(data,request.term));
            })
        },focus : function () {
            return false;
        },select:function (event,ui) {
            $('#activityName').val(ui.item.value);
            var bootstrapValidator = $("#updateActivityEnrollForm").data('bootstrapValidator');
            bootstrapValidator.updateStatus('activityVo.name', 'NOT_VALIDATED').validateField('activityVo.name');
            $.post(contextPath+"/b/activity/get.json",{activityId:activityLocalList[ui.item.value].activityId},function(result){
                var activityVo = result.result;
                $('#address').val(activityVo.activityAreaVo.address);
                $('#startTime').val((new Date(activityVo.beginDate)).Format("yyyy-MM-dd hh:mm"));
                $('#endTime').val((new Date(activityVo.endDate)).Format("yyyy-MM-dd hh:mm"));
                $('#title').val(activityVo.name+"报名单");
            });
            return false;
        }
    });
    Date.prototype.Format = function (fmt) {
        var o = {
            "M+": this.getMonth() + 1, //月份
            "d+": this.getDate(), //日
            "h+": this.getHours(), //小时
            "m+": this.getMinutes(), //分
            "s+": this.getSeconds(), //秒
            "q+": Math.floor((this.getMonth() + 3) / 3), //季度
            "S": this.getMilliseconds() //毫秒
        };
        if (/(y+)/.test(fmt))
            fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
        for (var k in o){
            if (new RegExp("(" + k + ")").test(fmt)) {
                fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
            }
        }
        return fmt;
    };

    var validateFields = {
        'merchantVo.memberName': {
            validators: {
                notEmpty: {
                    message: '商家不能为空'
                },
                threshold :  1 , //有1字符以上才发送ajax请求，（input中输入一个字符，插件会向服务器发送一次，设置限制，6字符以上才开始）
                remote: {//ajax验证。server result:{"valid",true or false} 向服务发送当前input name值，获得一个json数据。例表示正确：{"valid",true}
                    url: 'existmembername.json',//验证地址
                    message: '商家不存在',//提示消息
                    delay :  2000,//每输入一个字符，就发ajax请求，服务器压力还是太大，设置2秒发送一次ajax（默认输入一个字符，提交一次，服务器压力太大）
                    type: 'POST'//请求方式
                }
            }
        },'activityVo.name':{
            validators: {
                notEmpty: {
                    message: '活动不能为空'
                },
                threshold :  1 , //有1字符以上才发送ajax请求，（input中输入一个字符，插件会向服务器发送一次，设置限制，1字符以上才开始）
                remote: {//ajax验证。server result:{"valid",true or false} 向服务发送当前input name值，获得一个json数据。例表示正确：{"valid",true}
                    url: 'existactivityname.json',//验证地址
                    message: '活动不存在',//提示消息
                    delay :  2000,//每输入一个字符，就发ajax请求，服务器压力还是太大，设置2秒发送一次ajax（默认输入一个字符，提交一次，服务器压力太大）
                    type: 'POST'//请求方式
                }
            }
        },address:{
            validators:{
                notEmpty:{
                    message:'活动地点不能为空'
                },
                stringLength:{
                    max:30,
                    message:'活动地点应不大于30'
                }
            }
        },gmtActivityStart:{
            validators:{
                notEmpty:{
                    message:'活动开始时间不能为空'
                }
            }
        },
        gmtActivityEnd:{
            validators:{
                notEmpty:{
                    message:'活动结束时间不能为空'
                }
            }
        },gmtEnd:{
            validators: {
                notEmpty:{
                    message:'报名截止时间不能为空'
                }
            }
        },memo:{
            validators: {
                stringLength:{
                    max:300,
                    message:'备注长度应小于300'
                }
            }
        },title:{
            validators:{
                notEmpty:{
                    message:'标题不能为空'
                },
                stringLength:{
                    max:30,
                    message:'标题应不大于30'
                }
            }
        }

    };

    function initValidateFields(){
        var size = $('#enrollProductSize').val();
        for(var i=0;i<size;i++){
            validateFields["enrollProductVos["+i+"].name"]={
                validators:{
                    notEmpty:{
                        message:'分组名不能为空'
                    },
                    stringLength:{
                        max:30,
                        message:'分组名应不大于30'
                    }
                }
            };
            validateFields["enrollProductVos["+i+"].memo"]={
                validators:{
                    stringLength:{
                        max:30,
                        message:'分组备注长度应小于300'
                    }
                }
            };
            validateFields["enrollProductVos["+i+"].quantity"]={
                validators:{
                    notEmpty:{
                        message:'分组人数不能为空'
                    }
                }
            };
        }
    }
    initValidateFields();

    //新增报名单
    $('#updateActivityEnrollForm').bootstrapValidator({
            feedbackIcons: {
                /*input状态样式图片*/
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            }, fields: validateFields
        }
    ).on('success.form.bv',function () {
            document.getElementById("updateActivityEnrollForm").submit();
        });

    //新增分组
    $('#addMoreEnrollProduct').on('click',function(){
        //解决当数量为空时后台接受出错的问题
        $('.quantity').each(function(){
            if($(this).val()==''){
                $(this).val(1);
            }
        });
        $('#addProduct').val(true);
        document.getElementById("updateActivityEnrollForm").submit();
    });

    $('.deleteEnrollProduct').on('click',function(){
        $('#deleteOrd').val($(this).data('ord'));
        $('#deleteProduct').val(true);
        document.getElementById("updateActivityEnrollForm").submit();
    });
});