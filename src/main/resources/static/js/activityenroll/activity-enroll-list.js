$(function () {
    $('#query-reset').on('click', function () {
        $('#nickNameQuery').val('');
        $('#activityNameQuery').val('');
        $('#titleQuery').val('');
        $('#status').val('');
    });

    $('.deleteActivityEnroll').on('click',function(){
        var activityEnrollId = $(this).data('id');
        layer.open({
            type:1,
            title:'商家删除',
            area:'500px',
            zIndex:2,
            content:'删除操作将无法撤回，确认删除该报名单？',
            scrollbar:false,
            btn:['确认删除','取消'],
            yes:function (index) {
                window.location.href='delete?activityEnrollId='+activityEnrollId;
            }
        })
    });

    $('.data-enable').on('click', function () {
        var activityEnrollId = $(this).data('id');
        layer.open({
            type: 1,
            title: '报名单启动',
            area: '500px',
            zIndex: 2,
            content: '报名单启动后将无法修改，确认启用？',
            scrollbar: false,
            btn: ['确认启用', '取消'],
            yes: function (index) {
                window.location.href = 'enable?activityEnrollId=' + activityEnrollId;
            }
        });
    });

    //商家名匹配
    $('#nickNameQuery').autocomplete({
        minlengh:1,delay:500,source : function (request,response) {
            var query = $('#nickNameQuery').val();
            $.post(contextPath+"/b/merchant/nicknamefilter.json",{nickName:query,pageSize:8},function (result) {
                var data = [];
                var merchantList = result.result;
                for(var i=0;i<merchantList.length;i++){
                    data.push(merchantList[i].nickName);
                }
                response($.ui.autocomplete.filter(data,request.term));
            })
        },focus : function () {
            return false;
        },select:function (event,ui) {
            $('#nickNameQuery').val(ui.item.value);
            return false;
        }
    });


    //商家名匹配
    $('#activityNameQuery').autocomplete({
        minlengh:1,delay:500,source : function (request,response) {
            var query = $('#activityNameQuery').val();
            $.post(contextPath+"/b/activity/namefilter.json",{name:query,pageSize:8},function (result) {
                var data = [];
                var activityList = result.result;
                for(var i=0;i<activityList.length;i++){
                    data.push(activityList[i].name);
                }
                response($.ui.autocomplete.filter(data,request.term));
            })
        },focus : function () {
            return false;
        },select:function (event,ui) {
            $('#activityNameQuery').val(ui.item.value);
            return false;
        }
    });


});