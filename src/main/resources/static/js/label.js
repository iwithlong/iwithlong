$(function () {


	/**
	 * 标签删除
	 */
	$(".data-delete").on("click",function () {

		var labelId=$(this).parents("tr").find("input[name='labelId']").val();
		layer.confirm('确定删除？', {
			icon:2,
			btn: ['确定','取消']
		}, function(){
			$.post(contextPath+'/a/label/delete.json',{labelId:labelId},function (result) {
				showErrorMsg( result.message );
				setTimeout(function(){
					location.reload();
				}, 1000);
			});
		});

	})



});