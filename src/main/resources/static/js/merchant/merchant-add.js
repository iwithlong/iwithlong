$(function () {
    $('#addMerchant').on('click', function () {
        $('#addMerchantForm').bootstrapValidator('validate');
    });

    $('#addMerchantForm').bootstrapValidator({
            feedbackIcons: {
                /*input状态样式图片*/
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            }, fields: {
                memberName: {
                    validators: {
                        notEmpty: {
                            message: '商家名不能为空'
                        },
                        stringLength: {
                            max: 30,
                            message: '商家名称长度应不大于30'
                        },
                        threshold :  1 , //有1字符以上才发送ajax请求，（input中输入一个字符，插件会向服务器发送一次，设置限制，6字符以上才开始）
                        remote: {//ajax验证。server result:{"valid",true or false} 向服务发送当前input name值，获得一个json数据。例表示正确：{"valid",true}
                            url: 'existmerchantname.json',//验证地址
                            message: '商家名重复',//提示消息
                            delay :  2000,//每输入一个字符，就发ajax请求，服务器压力还是太大，设置2秒发送一次ajax（默认输入一个字符，提交一次，服务器压力太大）
                            type: 'POST'//请求方式
                        }
                    }
                },nickName:{
                    validators:{
                        notEmpty:{
                            message:'昵称不能为空'
                        },
                        stringLength:{
                            max:20,
                            message:'昵称长度应不大于20'
                        }
                    }
                },intro:{
                    validators:{
                        notEmpty:{
                            message:'简介不能为空'
                        },
                        stringLength:{
                            max:300,
                            message:'简介长度应小于300'
                        }
                    }
                },serviceTel:{
                    validators: {
                        notEmpty:{
                          message:'客服电话不能为空'
                        },
                        stringLength:{
                            max:11,
                            message:'客服电话长度应不大于11'
                        },
                        callback: {
                            message: '联系电话格式不对',
                            callback:function (value) {
                                if(/^\d+$/.test(value)||value==''){
                                    return true;
                                }else {
                                    return false;
                                }
                            }
                        }
                    }
                },accountType:{
                    notEmpty:{
                        message:'账号类型未选择'
                    }
                },accountName:{
                    validators:{
                        notEmpty:{
                            message:'账号名称不能为空'
                        },
                        stringLength:{
                            max:20,
                            message:'昵称名称应不大于20'
                        }
                    }
                },accountCode:{
                    validators:{
                        notEmpty:{
                            message:'账号不能为空'
                        },
                        stringLength:{
                            max:30,
                            message:'账号长度应不大于30'
                        }
                    }
                }

            }
        }
    ).on('success.form.bv',function () {
            document.getElementById("addMerchantForm").submit();
        })
});