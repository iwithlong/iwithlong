$(function () {
    $('#query-reset').on('click', function () {
        $('#accountCodeQuery').val('');
        $('#accountNameQuery').val('');
        $('#accountTypeQuery').val('');
        $('#memberNameQuery').val('');
        $('#nickNameQuery').val('');
    });

    $('.deleteActivityEnroll').on('click',function(){
        var memberId = $(this).data('id');
        layer.open({
            type:1,
            title:'商家删除',
            area:'500px',
            zIndex:2,
            content:'删除操作将无法撤回，确认删除该商家？',
            scrollbar:false,
            btn:['确认删除','取消'],
            yes:function (index) {
                window.location.href='delete?memberId='+memberId;
            }
        })
    });

    $('.data-enable').on('click',function(){
        var memberId = $(this).data('id');
        window.location.href='enable?memberId='+memberId;
    })
});