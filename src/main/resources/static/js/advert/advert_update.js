$(function(){
    var activityLocalList;

    $('#updateAdvert').on('click', function () {
        $('#updateAdvertForm').bootstrapValidator('validate');
    });
    //活动名匹配
    $('#bizName').autocomplete({
        minlengh:1,delay:500,source : function (request,response) {
            var query = $('#bizName').val();
            $.post(contextPath+"/b/activity/namefilter.json",{name:query,pageSize:8},function (result) {
                var data = [];
                var activityList = result.result;
                activityLocalList={};
                for(var i=0;i<activityList.length;i++){
                    data.push(activityList[i].name);
                    activityLocalList[activityList[i].name]=activityList[i];
                }
                response($.ui.autocomplete.filter(data,request.term));
            })
        },focus : function () {
            return false;
        },select:function (event,ui) {
            $('#bizName').val(ui.item.value);
            $('#bizId').val(activityLocalList[ui.item.value].activityId);
            $('#title').val(ui.item.value);
            return false;
        }
    });


    //新增报名单
    $('#updateAdvertForm').bootstrapValidator({
            feedbackIcons: {
                /*input状态样式图片*/
                valid: 'glyphicon glyphicon-ok',
                invalid: 'glyphicon glyphicon-remove',
                validating: 'glyphicon glyphicon-refresh'
            }, fields: {
                bizName: {
                    validators: {
                        notEmpty: {
                            message: '资源不能为空'
                        }
                    }
                },
                title:{
                    validators:{
                        notEmpty:{
                            message:'标题不能为空'
                        }
                    }
                },
                image:{
                    validators:{
                        notEmpty:{
                            message:'图片不能为空'
                        }
                    }
                }
            }
        }
    ).on('success.form.bv',function () {
            document.getElementById("updateAdvertForm").submit();
        });
});