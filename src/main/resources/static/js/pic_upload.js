/**
 * Created by xiexiaohan on 2017/10/31.
 */
var files;
$(function () {
    var initPic = [];
    var initPicType = [];
    var picCount = 0;
    //正在上传的图片数量
    var uploadingCount = 0;
    var pics = $('#uploadedPic').children();
    files = [];
    //初始化已上传的图片
    for(var i=0;i<pics.length;i++){
        var picDom = $(pics[i]);
        var img = "<img src='"+picDom.val()+"' class='kv-preview-data file-preview-image'>";
        initPic.push(img);
        var picType = {type: "image", caption: picDom.data("fileName"),url:"/b/uploadfile/delete.json?uploadFileId="+picDom.data("id"), key: picDom.data("id"),downloadUrl:false};
        initPicType.push(picType);
        files.push({fileId:picDom.data("id"),kid:picDom.data("id")});
        picCount++;
    }
    //fileinput参数
    var uploadInputOption = {
        uploadUrl: contextPath + "/b/uploadfile/upload.json",//上传图片的url
        overwriteInitial: false,
        uploadAsync: true,
        maxFileSize: 2000,//上传文件最大的尺寸
        maxFilesNum: 3,//上传最大的文件数量
        initialCaption: "请上传活动点图片",//文本框初始话value
        slugCallback: function (filename) {
            return filename.replace('(', '_').replace(']', '_');
        },
        initialPreviewAsData: false, // allows you to set a raw markup
        initialPreviewFileType: 'image',
        language: 'zh',
        allowedFileExtensions: ['jpg', 'png', 'gif'],
        maxFileSize: 2000,
        initialPreview: initPic,
        initialPreviewConfig:initPicType
    };

    //初始化fileinput
    $('.picUpload').fileinput(uploadInputOption);

    //文件选择后自动上传
    $('.picUpload').on("filebatchselected", function(event, uploadingfiles) {
        var size = uploadingfiles.length;
        picCount = picCount+size;
        uploadingCount = size;
        for(var i=0;i<size;i++){
            files.push({});
        }
        $(this).fileinput("upload",true);
    });

    //文件上传后触发
    $('.picUpload').on('fileuploaded', function (event, data, previewId, index) {
        var result = data.response;
        if(result.success){
            var uploadFiles = result.result;
            for(var i=0;i<uploadFiles.length;i++){
                files[files.length-uploadingCount+index].fileId=uploadFiles[i].uploadFileId;
                files[files.length-uploadingCount+index].kid=previewId;
            }
        }else {
            alert(result.message+"，请删除图片后重新上传");
        }
    });

    //上传的文件被删除后触发
    $(".picUpload").on("filesuccessremove", function (event, previewId, index) {
        for(var i=0;i<files.length;i++){
            if(files[i].kid==previewId){
                files.splice(i,1);
                // files[i]={fileId:"",kid:""};
                picCount--;
                break;
            }
        }
    });

    //原先上传的图片被删除后触发
    $(".picUpload").on("filepredelete", function (event, previewId, index) {
        for(var i=0;i<files.length;i++){
            if(files[i].kid==previewId){
                files.splice(i,1);
                // files[i]={fileId:"",kid:""};
                picCount--;
                break;
            }
        }
    });
    $(".picUpload").on("filebeforedelete", function (event, previewId, index) {
        for(var i=0;i<files.length;i++){
            if(files[i].kid==previewId){
                files.splice(i,1);
                // files[i]={fileId:"",kid:""};
                picCount--;
                break;
            }
        }
    });

});