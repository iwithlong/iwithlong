/**
 * Created by xiexiaohan on 2017/11/3.
 */
$(function () {
    $('.data-enable').on('click',function (event) {
        var target = $(event.target);
        var activityAreaId = target.data("id");
        $.post(contextPath+"/b/activityarea/enable.json",{activityAreaId:activityAreaId},function (result) {
            if(result.success){
                layer.alert("操作成功",{icon:1,yes:function () {
                    $('#data-query').click();
                }});

            }else {
                layer.alert(result.message,{icon:5})
            }
        })


    });

    $('#query-reset').on('click',function (e) {
        $('#status').val("");
        $('#enableSearch').val("");
        $('#creator').val("");
        $('#issuer').val("");
    });

    $('.goIssuer').on('click',function (e) {
        var target = $(e.target);
        var id = target.data('id');
        $('#issuerActivityAreaId').val(id);
        layer.open({
            type:1,
            title:'活动点发布',
            area:'500px',
            zIndex:2,
            content:$('#goIssuerForm'),
            scrollbar:false,
            btn:['发布','取消'],
            yes:function (index) {
                if($('#issueType').val()=='false'&&!$('#gmtIssueInput').val()){
                    layer.alert("发布时间不能为空",{icon:5})
                }else {
                    $('#issueForm').submit();
                }
            }
        })
    });

    $('#issueType').on('change',function () {
        $('#isNow').val($('#issueType').val());
        if($('#issueType').val()=='false'){
            $('.gmtIssueDiv').show();
        }else {
            $('.gmtIssueDiv').hide();
        }
    });

    $('#gmtIssueInput').datetimepicker({
        language: "en",
        autoclose: true,
        todayHighlight: true,
        startDate:new Date(),
        // locale: moment.locale('zh-cn'),
        format: "yyyy-mm-dd hh:ii:ss"//日期格式
    });
});