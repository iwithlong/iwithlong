/**
 * Created by xiexiaohan on 2017/10/31.
 */
$(function () {
    $('#doAudit').on('click',function () {
        $('#activityAreaStatus').val('2');
        addActivityArea();
    });

    $('#doAdd').on('click',function () {
        $('#activityAreaStatus').val('1');
        addActivityArea();
    });

    function addActivityArea() {
        var fileIds = "";
        for(var i=0;i<files.length;i++){
            if(files[i].fileId){
                fileIds=fileIds+files[i].fileId+",";
            }
        }
        if(fileIds){
            fileIds=fileIds.substr(0,fileIds.length-1);
        }
        $('#uploadFileIds').val(fileIds);
        $('#containerInitValue').val(ue.getContent());
        $('#addForm').bootstrapValidator('validate');

    }

    $('#addForm').bootstrapValidator({
        feedbackIcons: {/*input状态样式图片*/
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {/*验证：规则*/
            areaName: {//验证input项：验证规则
                validators: {
                    notEmpty: {//非空验证：提示消息
                        message: '活动点名称不能为空'
                    },
                    stringLength: {
                        max: 100,
                        message: '活动点名称长度应小于100'
                    },
                    threshold :  1 , //有6字符以上才发送ajax请求，（input中输入一个字符，插件会向服务器发送一次，设置限制，6字符以上才开始）
                    remote: {//ajax验证。server result:{"valid",true or false} 向服务发送当前input name值，获得一个json数据。例表示正确：{"valid",true}
                        url: 'existactivityareaname.json',//验证地址
                        message: '活动点已存在',//提示消息
                        delay :  2000,//每输入一个字符，就发ajax请求，服务器压力还是太大，设置2秒发送一次ajax（默认输入一个字符，提交一次，服务器压力太大）
                        type: 'POST'//请求方式
                    }
                }
            },
            brief: {
                // message:'密码无效',
                validators: {
                    notEmpty: {
                        message: '简述不能为空'
                    },
                    stringLength: {
                        max: 300,
                        message: '简述长度应小于300'
                    }
                }
            },
            address:{
                validators: {
                    notEmpty: {
                        message: '地址不能为空'
                    }
                }
            },
            longitudeAndLatitude:{
                validators:{
                    notEmpty:{
                        message:'经纬度不能为空'
                    },
                    callback: {
                        message: '经纬度不规范,纬度-90～90,经度范围-180～180，参考：40.024864,116.224895',
                        callback:function(value, validator,$field){
                            var longitudeAndLatitude = value.split(",");
                            if(longitudeAndLatitude.length!=2){
                                return false;
                            }
                            for(var i=0;i<longitudeAndLatitude.length;i++){
                                if(!longitudeAndLatitude[i]){
                                    return false;
                                }
                            }
                            var patten = /^(-?\d+)(\.\d+)?$/;
                            if(!patten.test(longitudeAndLatitude[0])||!patten.test(longitudeAndLatitude[1])){
                                return false;
                            }
                            var latitude = parseFloat(longitudeAndLatitude[0]);
                            var longitude = parseFloat(longitudeAndLatitude[1]);
                            if(isNaN(longitude)||isNaN(latitude)){
                                return false;
                            }
                            if(longitude<-180||longitude>180){
                                return false;
                            }
                            if(latitude<-90||latitude>90){
                                return false;
                            }
                            $('#longitude').val(longitude);
                            $('#latitude').val(latitude);
                            return true;
                        }
                    }
                }
            },
            province:{
                validators: {
                    notEmpty:{
                        message: '省份不能为空'
                    }
                }
            },
            city:{
                validators: {
                    notEmpty:{
                        message: '城市不能为空'
                    }
                }
            },
            district:{
                validators: {
                    notEmpty:{
                        message: '区、镇不能为空'
                    }
                }
            }
        }
    }).on('success.form.bv',function () {
        document.getElementById("addForm").submit();
    });


});