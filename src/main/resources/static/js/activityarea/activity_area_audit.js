/**
 * Created by xiexiaohan on 2017/10/18.
 */
$(function () {
    $('#auditPass').on('click',function () {
        $('#auditStatus').val('4');
        $('#auditForm').submit();
    })
    $('#auditNotPass').on('click',function () {
        layer.open({
            type:1,
            title:'不通过理由',
            area:'900px',
            content:$('#auditRefuseDiv'),
            btn:['确认','取消'],
            yes:function (index) {
                $('#auditStatus').val('3');
                $('#auditForm').submit();
                layer.close(index);
            }
        })
    })
    
    $('#addReason').on('click',function () {
        $('#resultReason').val($('#resultReason').val()+$('#normalReason').val());
    })
});