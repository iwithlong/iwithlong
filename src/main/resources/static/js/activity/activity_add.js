/**
 * Created by xiexiaohan on 2017/10/20.
 */
$(function () {
    $('.date-picker').datetimepicker({
        language: "en",
        autoclose: true,
        todayHighlight: true,
        // locale: moment.locale('zh-cn'),
        format: "yyyy-mm-dd hh:ii"//日期格式
    });
    
    $('#addActivity').on('click',function () {
        $('#addStatus').val('1');
        doAdd();
    });
    $('#auditActivity').on('click',function () {
        $('#addStatus').val('2');
        doAdd();
    });
    function doAdd() {
        var fileIds = "";
        for(var i=0;i<files.length;i++){
            if(files[i].fileId){
                fileIds=fileIds+files[i].fileId+",";
            }
        }
        if(fileIds){
            fileIds=fileIds.substr(0,fileIds.length-1);
        }
        $('#uploadFileIds').val(fileIds);
        $('#containerInitValue').val(ue.getContent());
        $('#activityLabel').val(ms.getValue());
        $('#addActivityForm').bootstrapValidator('validate');
    }

    $('#activityAreaVoName').autocomplete({
        minlengh:1,delay:500,source : function (request,response) {
            var query = $('#activityAreaVoName').val();
            $.post(contextPath+"/b/activityarea/filter.json",{areaName:query,pageSize:8},function (result) {
                var data = [];
                var activityAreaList = result.result;
                for(var i=0;i<activityAreaList.length;i++){
                    data.push(activityAreaList[i].areaName);
                }
                response($.ui.autocomplete.filter(data,request.term));
            })
        },focus : function () {
            return false;
        },select:function (event,ui) {
            $('#activityAreaVoName').val(ui.item.value);
            var bootstrapValidator = $("#addActivityForm").data('bootstrapValidator');
            bootstrapValidator.validateField('activityAreaVo.areaName');
            return false;
        }
    });

    $('#addActivityForm').bootstrapValidator({
        feedbackIcons: {/*input状态样式图片*/
            valid: 'glyphicon glyphicon-ok',
            invalid: 'glyphicon glyphicon-remove',
            validating: 'glyphicon glyphicon-refresh'
        },
        fields: {/*验证：规则*/
            name:{
                validators: {
                    notEmpty: {
                        message: '活动标题不能为空'
                    },stringLength: {
                        max: 100,
                        message: '标题名称长度应小于100'
                    },
                    threshold :  1 , //有6字符以上才发送ajax请求，（input中输入一个字符，插件会向服务器发送一次，设置限制，6字符以上才开始）
                    remote: {//ajax验证。server result:{"valid",true or false} 向服务发送当前input name值，获得一个json数据。例表示正确：{"valid",true}
                        url: 'existactivityname.json',//验证地址
                        message: '活动标题重复',//提示消息
                        delay :  2000,//每输入一个字符，就发ajax请求，服务器压力还是太大，设置2秒发送一次ajax（默认输入一个字符，提交一次，服务器压力太大）
                        type: 'POST'//请求方式
                    }
                }
            },
            brief: {
                // message:'密码无效',
                validators: {
                    notEmpty: {
                        message: '简述不能为空'
                    },
                    stringLength: {
                        max: 300,
                        message: '简述长度应小于300'
                    }
                }
            },
            charging:{
                validators: {
                    notEmpty: {
                        message: '收费情况不能为空'
                    }
                }
            },
            recommended: {
                // message:'密码无效',
                validators: {
                    notEmpty: {
                        message: '推荐值不能为空'
                    },
                    callback: {
                        message: '推荐值大于0小于5',
                        callback: function(value, validator) {
                            if(Number(value)<0||Number(value)>5){
                                return false;
                            }
                            return true;
                        }
                    }
                }
            },
            beginDate:{
                // message:'密码无效',
                validators: {
                    notEmpty: {
                        message: '开始时间不能为空'
                    }
                }
            },
            endDate:{
                // message:'密码无效',
                validators: {
                    notEmpty: {
                        message: '结束时间不能为空'
                    }
                }
            },
            activityLabel:{
                validators: {
                    notEmpty: {
                        message: '标签不能为空'
                    }
                }
            },
            'activityAreaVo.areaName':{
                validators: {
                    notEmpty: {
                        message: '所属活动点不能为空'
                    },
                    threshold :  1 , //有6字符以上才发送ajax请求，（input中输入一个字符，插件会向服务器发送一次，设置限制，6字符以上才开始）
                    remote: {//ajax验证。server result:{"valid",true or false} 向服务发送当前input name值，获得一个json数据。例表示正确：{"valid",true}
                        url: 'existactivityareaname.json',//验证地址
                        message: '活动点不存在',//提示消息
                        delay :  2000,//每输入一个字符，就发ajax请求，服务器压力还是太大，设置2秒发送一次ajax（默认输入一个字符，提交一次，服务器压力太大）
                        type: 'POST'//请求方式
                    }
                }
            },
            telephone:{
                validators: {
                    // notEmpty:{
                    //     message: '联系电话不能为空'
                    // },
                    callback: {
                        message: '联系电话格式不对',
                        callback:function (value) {
                            if(/^\d+$/.test(value)||value==''){
                                return true;
                            }else {
                                return false;
                            }
                        }
                    }
                }
            }
        }
    }).on('success.form.bv',function () {
        document.getElementById("addActivityForm").submit();
    });

    var ms;

    $.post(contextPath+"/b/label/activitylabels.json",{},function (result) {
        var data = [];
        for(var i=0;i<result.result.length;i++){
            var obj = result.result[i];
            var newObj = {};
            newObj.id = obj.labelName;
            newObj.name = obj.labelName;
            if(obj.labelType=='fast_keywords'){
                newObj.name = newObj.name+"<span style='font-size: x-small;color: #9e9e9e'> [快速搜索]</span>";
            }else {
                newObj.name = newObj.name+"<span style='font-size: x-small;color: #9e9e9e'> [活动类型]</span>";
            }
            data.push(newObj);
        }
        ms = $('.activityLabelSuggest').magicSuggest({
            disabled:false,
            allowSelect:false,
            allowFreeEntries:false,
            data:data,
            placeholder: '输入或选择标签',
            selectionRenderer:function (data) {
                return data.id;
            }
        });
        var labels = $('#activityLabel').val();
        if(labels.trim()!=""){
            ms.setValue(labels.split(","));
        }

    });
    // $('.activityLabelSuggest').magicSuggest({
    //     disabled:false,
    //     allowSelect:false,
    //     allowFreeEntries:false,
    //     data:contextPath+"b/label/activitylabels",
    //     ajaxConfig:{
    //
    //     }
    // })

});