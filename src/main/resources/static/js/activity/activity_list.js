/**
 * Created by xiexiaohan on 2017/11/4.
 */
$(function () {
    $('.date-picker').datetimepicker({
        language: "en",
        autoclose: true,
        todayHighlight: true,
        // locale: moment.locale('zh-cn'),
        format: "yyyy-mm-dd hh:ii"//日期格式
    });

    $('.data-enable').on('click',function (event) {
        var target = $(event.target);
        var activityId = target.data("id");
        $.post(contextPath+"/b/activity/enable.json",{activityId:activityId},function (result) {
            if(result.success){
                layer.alert("操作成功",{icon:1,yes:function () {
                    $('#data-query').click();
                }});

            }else {
                layer.alert(result.message,{icon:5})
            }
        })


    });

    $('#query-reset').on('click',function (e) {
        $('#status').val("");
        $('#enableSearch').val("");
        $('#keyword').val("");
        $('#issuer').val("");
        $('#creator').val("");
    });

    $('.goIssuer').on('click',function (e) {
        var target = $(e.target);
        var id = target.data('id');
        $('#issuerActivityId').val(id);
        layer.open({
            type:1,
            title:'活动发布',
            area:'500px',
            zIndex:2,
            content:$('#goIssuerForm'),
            scrollbar:false,
            btn:['发布','取消'],
            yes:function (index) {
                if($('#issueType').val()=='false'&&!$('#gmtIssueInput').val()){
                    layer.alert("发布时间不能为空",{icon:5})
                }else {
                    layer.close(index);
                    $.post(contextPath+'/b/activity/issue.json',$('#issueForm').serialize(),function (result) {
                        if(result.success){
                            layer.alert("发布成功",{icon:1});
                            $('#data-query').click();
                        }else {
                            layer.alert(result.message,{icon:5});
                        }
                    })
                }
            }
        })
    });

    $('#issueType').on('change',function () {
        $('#isNow').val($('#issueType').val());
        if($('#issueType').val()=='false'){
            $('.gmtIssueDiv').show();
        }else {
            $('.gmtIssueDiv').hide();
        }
    });

    $('#gmtIssueInput').datetimepicker({
        language: "en",
        autoclose: true,
        todayHighlight: true,
        startDate:new Date(),
        // locale: moment.locale('zh-cn'),
        format: "yyyy-mm-dd hh:ii:ss"//日期格式
    });

    $('#areaName').autocomplete({
        minlengh:1,delay:500,source : function (request,response) {
            var query = $('#areaName').val();
            $.post(contextPath+"/b/activityarea/filter.json",{areaName:query,pageSize:8},function (result) {
                var data = [];
                var activityAreaList = result.result;
                for(var i=0;i<activityAreaList.length;i++){
                    data.push(activityAreaList[i].areaName);
                }
                response($.ui.autocomplete.filter(data,request.term));
            })
        },focus : function () {
            return false;
        },select:function (event,ui) {
            $('#areaName').val(ui.item.value);
            return false;
        }
    });

    $('#numberOfClickOrder').on('click',function (e) {
        var orderByType = $('#orderByType').val();
        var orderBy = $('#orderBy').val();
        if(orderBy!='numberOfClick'){
            $('#orderBy').val('numberOfClick');
            $('#orderByType').val('desc');
        }else{
            if(orderByType=='desc'){
                $('#orderByType').val('asc');
            }else {
                $('#orderByType').val('');
                $('#orderBy').val('');
            }
        }
        $('#data-query').click();
    });
    $('#numberOfInterestOrder').on('click',function (e) {
        var orderByType = $('#orderByType').val();
        var orderBy = $('#orderBy').val();
        if(orderBy!='numberOfInterest'){
            $('#orderBy').val('numberOfInterest');
            $('#orderByType').val('desc');
        }else{
            if(orderByType=='desc'){
                $('#orderByType').val('asc');
            }else {
                $('#orderByType').val('');
                $('#orderBy').val('');
            }
        }
        $('#data-query').click();
    })
});